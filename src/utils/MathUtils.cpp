#include "MathUtils.hpp"
#include <cmath>

namespace utils {
namespace math {

double euclidean2d(const double x1, const double y1, const double x2, const double y2)
{
    return std::sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

double haversine(const double lat1, const double lon1, const double lat2, const double lon2)
{
    const double earthRad = 6371;
    const double dLatRad = (lat2 - lat1) * M_PI / 180.0;
    const double dLonRad = (lon2 - lon1) * M_PI / 180.0;
    const double lat1Rad = lat1 * M_PI / 180.0;
    const double lat2Rad = lat2 * M_PI / 180.0;

    const double underSqrt = std::pow(std::sin(dLatRad / 2.0), 2.0)
                             + std::pow(std::sin(dLonRad / 2.0), 2.0) * std::cos(lat1Rad) * std::cos(lat2Rad);

    return earthRad * 2.0 * std::asin(std::sqrt(underSqrt));
}

}  // namespace math
} // namespace utils
