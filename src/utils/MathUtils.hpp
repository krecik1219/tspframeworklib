#pragma once

namespace utils {
namespace math {

double euclidean2d(const double x1, const double y1, const double x2, const double y2);
double haversine(const double lat1, const double lon1, const double lat2, const double lon2);

}  // namespace math
} // namespace utils
