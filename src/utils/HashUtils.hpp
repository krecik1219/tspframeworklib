#pragma once

#include <cstddef>
#include <functional>

namespace utils {
namespace hashing {

template<class T>
void hash_combine(std::size_t& seed, const T& v) noexcept
{
    seed ^= std::hash<T>{}(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

template<class It>
void hash_range(std::size_t& seed, It first, It last) noexcept
{
    for(; first != last; ++first)
        hash_combine(seed, *first);
}

}  // namespace math
} // namespace utils
