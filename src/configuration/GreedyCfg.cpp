#include "GreedyCfg.hpp"

namespace configuration {

GreedyCfg::GreedyCfg()
    : AlgoCfg(algo::AlgoType::GREEDY)
{
}

} // namespace configuration
