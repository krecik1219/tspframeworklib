#include "HybridGaTsCfg2.hpp"

namespace configuration {

HybridGaTsCfg2::HybridGaTsCfg2()
    : AlgoCfg(algo::AlgoType::HYBRID_GA_TS_2)
{
}

} // namespace configuration
