#pragma once

#include <cstdint>
#include "AlgoCfg.hpp"

namespace configuration {

class RandomCfg : public AlgoCfg
{
public:
    RandomCfg();

    uint32_t iterations;
};

} // namespace configuration
