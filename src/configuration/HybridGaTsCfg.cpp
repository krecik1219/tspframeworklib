#include "HybridGaTsCfg.hpp"

namespace configuration {

HybridGaTsCfg::HybridGaTsCfg()
    : AlgoCfg(algo::AlgoType::HYBRID_GA_TS)
{
}

} // namespace configuration
