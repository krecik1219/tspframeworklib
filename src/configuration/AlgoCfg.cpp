#include "AlgoCfg.hpp"

namespace configuration {

AlgoCfg::AlgoCfg(const algo::AlgoType algoType)
    : algoType{algoType}
{
}

AlgoCfg::~AlgoCfg() = default;
AlgoCfg::AlgoCfg(const AlgoCfg&) = default;
AlgoCfg& AlgoCfg::operator=(const AlgoCfg&) = default;
AlgoCfg::AlgoCfg(AlgoCfg&&) = default;
AlgoCfg& AlgoCfg::operator=(AlgoCfg&&) = default;

} // namespace configuration
