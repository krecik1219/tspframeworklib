#include "RunCfg.hpp"
#include <algo/AlgoType.hpp>
#include "GaCfg.hpp"
#include "HybridGaSaCfg.hpp"
#include "TsCfg.hpp"
#include "SaCfg.hpp"
#include "GreedyCfg.hpp"
#include "RandomCfg.hpp"
#include "HybridGaTsCfg.hpp"
#include "HybridGaTsCfg2.hpp"

namespace configuration {

RunCfg::RunCfg() = default;
RunCfg::~RunCfg() = default;

RunCfg::RunCfg(const RunCfg& other)
    : repetitionsNum{other.repetitionsNum}
    , tspInstanceCfgPath{other.tspInstanceCfgPath}
    , resultsCsvFile{other.resultsCsvFile}
    , maxAlgSecondsDuration{other.maxAlgSecondsDuration}
    , algoCfg{copyAlgoCfg(*other.algoCfg)}
{
}

RunCfg& RunCfg::operator=(const RunCfg& other)
{
    repetitionsNum = other.repetitionsNum;
    tspInstanceCfgPath = other.tspInstanceCfgPath;
    resultsCsvFile = other.resultsCsvFile;
    maxAlgSecondsDuration = other.maxAlgSecondsDuration;
    algoCfg = copyAlgoCfg(*other.algoCfg);
    return *this;
}

std::unique_ptr<AlgoCfg> RunCfg::copyAlgoCfg(const AlgoCfg& algoCfg)
{
    switch(algoCfg.algoType)
    {
        case algo::AlgoType::GA:
            return std::make_unique<GaCfg>(dynamic_cast<const GaCfg&>(algoCfg));
        case algo::AlgoType::TS:
            return std::make_unique<TsCfg>(dynamic_cast<const TsCfg&>(algoCfg));
        case algo::AlgoType::SA:
            return std::make_unique<SaCfg>(dynamic_cast<const SaCfg&>(algoCfg));
        case algo::AlgoType::HYBRID_GA_SA:
            return std::make_unique<HybridGaSaCfg>(dynamic_cast<const HybridGaSaCfg&>(algoCfg));
        case algo::AlgoType::HYBRID_GA_TS:
            return std::make_unique<HybridGaTsCfg>(dynamic_cast<const HybridGaTsCfg&>(algoCfg));
        case algo::AlgoType::HYBRID_GA_TS_2:
            return std::make_unique<HybridGaTsCfg2>(dynamic_cast<const HybridGaTsCfg2&>(algoCfg));
        case algo::AlgoType::GREEDY:
            return std::make_unique<GreedyCfg>(dynamic_cast<const GreedyCfg&>(algoCfg));
        case algo::AlgoType::RANDOM:
            return std::make_unique<RandomCfg>(dynamic_cast<const RandomCfg&>(algoCfg));
        default:
            throw std::runtime_error("Invalid algo type");
    }
}

RunCfg::RunCfg(RunCfg&&) = default;
RunCfg& RunCfg::operator=(RunCfg&&) = default;


} // namespace configuration
