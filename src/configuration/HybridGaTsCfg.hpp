#pragma once

#include "AlgoCfg.hpp"
#include "GaCfg.hpp"
#include <string>

namespace configuration {

class HybridGaTsCfg : public AlgoCfg
{
public:
    HybridGaTsCfg();

    GaCfg gaCfg;
    uint32_t tabuMemoryDepth;
};

} // namespace configuration
