#pragma once

#include <cstdint>
#include "AlgoCfg.hpp"

namespace configuration {

class SaCfg : public AlgoCfg
{
public:
    SaCfg();

    double initTemp;
    double tempStepFactor;
    uint32_t iterationsPerTemp;
};

} // namespace configuration
