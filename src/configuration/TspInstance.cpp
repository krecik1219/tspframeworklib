#include "TspInstance.hpp"
#include <utils/MathUtils.hpp>
#include <cmath>

namespace configuration {
namespace tsp {

void TspInstance::resizeToDimension()
{
    distanceMatrix.resize(dimension);
    for(auto& row : distanceMatrix)
        row.resize(dimension);
    citiesData.resize(dimension);
}

void TspInstance::computeDistanceMatrix()
{
    if(citiesData.size() < 2)
        return;
    for(auto i = 0u; i < citiesData.size(); i++)
    {
        for(auto j = 0u; j < citiesData.size(); j++)
        {
            if(i == j)
            {
                distanceMatrix[i][j] = 0.0;
            }
            else
            {
                distanceMatrix[i][j] = computeDistance(i, j);
            }
        }
    }
}

double TspInstance::computeDistance(const uint32_t cityIdA, const uint32_t cityIdB) const
{
    if(edgeWeightType == "EUC_2D")
    {
        return utils::math::euclidean2d(citiesData[cityIdA].xCoord, citiesData[cityIdA].yCoord,
                                        citiesData[cityIdB].xCoord, citiesData[cityIdB].yCoord);
    }
    else if(edgeWeightType == "CEIL_2D")
    {
        return std::ceil(utils::math::euclidean2d(citiesData[cityIdA].xCoord, citiesData[cityIdA].yCoord,
                                                  citiesData[cityIdB].xCoord, citiesData[cityIdB].yCoord));
    }
    else if(edgeWeightType == "GEO")
    {
        return utils::math::haversine(citiesData[cityIdA].xCoord, citiesData[cityIdA].yCoord,
                                      citiesData[cityIdB].xCoord, citiesData[cityIdB].yCoord);
    }
    else
        throw std::runtime_error("Unexpected edge weight type: " + edgeWeightType);
}

void TspInstance::fillNearestDistanceLookup()
{
    auto minDistance = std::numeric_limits<double>::infinity();
    for (auto i = 0u; i < citiesData.size(); i++)
    {
        auto nearestIndex = 0u;
        for (auto j = 0u; j < citiesData.size(); j++)
        {
            if (j == i)
                continue;
            auto distance = distanceMatrix[i][j];
            if (distance < minDistance)
            {
                minDistance = distance;
                nearestIndex = j;
            }
        }
        nearestDistanceLookup[i] = std::make_pair(nearestIndex, minDistance);
        minDistance = std::numeric_limits<double>::infinity();
    }
}

} // namespace tsp
} // namespace configuration
