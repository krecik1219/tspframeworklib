#pragma once

#include "AlgoCfg.hpp"
#include <string>

namespace configuration {

class GaCfg : public AlgoCfg
{
public:
    GaCfg();

    uint32_t populationSize;
    std::string selectionStrategy;
    uint32_t tournamentSize;
    uint32_t maxGenerationsNum;
    double crossoverProb;
    double mutationProb;
};

} // namespace configuration
