#pragma once

#include "AlgoCfg.hpp"
#include "GaCfg.hpp"
#include "TsCfg.hpp"
#include <string>

namespace configuration {

class HybridGaTsCfg2 : public AlgoCfg
{
public:
    HybridGaTsCfg2();

    GaCfg gaCfg;
    TsCfg tsCfg;
    double percentOfPopulationToOptimizeWithTs;
    uint32_t maxGenerationsWithoutImprovement;
};

} // namespace configuration
