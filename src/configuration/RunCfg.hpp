#pragma once

#include "AlgoCfg.hpp"
#include <memory>
#include <string>
#include <chrono>

namespace configuration {

struct RunCfg
{
    RunCfg();
    ~RunCfg();
    RunCfg(const RunCfg& other);
    RunCfg& operator=(const RunCfg& other);
    RunCfg(RunCfg&&);
    RunCfg& operator=(RunCfg&&);

    uint32_t repetitionsNum;
    std::string tspInstanceCfgPath;
    std::string resultsCsvFile;
    std::chrono::seconds maxAlgSecondsDuration;
    std::unique_ptr<AlgoCfg> algoCfg;

private:
    std::unique_ptr<AlgoCfg> copyAlgoCfg(const AlgoCfg& algoCfg);
};

} // namespace configuration
