#pragma once

#include <cstdint>
#include "AlgoCfg.hpp"

namespace configuration {

class GreedyCfg : public AlgoCfg
{
public:
    GreedyCfg();
};

} // namespace configuration
