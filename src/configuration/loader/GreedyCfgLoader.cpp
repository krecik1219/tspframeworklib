#include "GreedyCfgLoader.hpp"
#include "LoaderException.hpp"
#include "LoaderUtilsFunctions.hpp"

namespace configuration {
namespace loader {

GreedyCfgLoader::GreedyCfgLoader(AlgoCfg& algoCfg)
    : AlgoSpecificLoader()
    , greedyCfg{dynamic_cast<GreedyCfg&>(algoCfg)}
{
}

void GreedyCfgLoader::decideWhatToDoWithLine(const std::string& /*line*/)
{
}

} // namespace loader
} // namespace configuration
