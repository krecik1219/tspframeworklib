#include "TspInstanceLoader.hpp"
#include <fstream>
#include <utils/StringUtils.hpp>
#include <sstream>
#include <vector>
#include <iterator>
#include "LoaderException.hpp"
#include "LoaderUtilsFunctions.hpp"

namespace configuration {
namespace loader {

TspInstanceLoader::TspInstanceLoader()
    : shouldExpectCoordinates{false}
{
}

TspInstanceLoader::~TspInstanceLoader() = default;

tsp::TspInstance TspInstanceLoader::loadTspInstance(const std::string& instanceCfgPath)
{
    std::ifstream instanceCfgFile{instanceCfgPath};
    if(!instanceCfgFile)
        throw LoaderException("Can't open instance config file at path: " + instanceCfgPath);

    tspInstance = tsp::TspInstance{};
    std::string instanceCfgLine;
    while(std::getline(instanceCfgFile, instanceCfgLine))
    {
        if(isWhitespaceLine(instanceCfgLine))
            continue;
        decideWhatToDoWithLine(instanceCfgLine);
    }
    tspInstance.computeDistanceMatrix();
    tspInstance.fillNearestDistanceLookup();
    return tspInstance;
}

void TspInstanceLoader::decideWhatToDoWithLine(const std::string& instanceCfgLine)
{
    try
    {
        if(instanceCfgLine.find("NAME") != std::string::npos)
        {
            auto value = prepareValueToStore(instanceCfgLine);
            tspInstance.name = std::move(value);
        }
        else if(instanceCfgLine.find("DIMENSION") != std::string::npos)
        {
            auto value = prepareValueToStore(instanceCfgLine);
            tspInstance.dimension = static_cast<uint32_t>(std::stoul(value));
            tspInstance.resizeToDimension();
        }
        else if(instanceCfgLine.find("NODE_COORD_SECTION") != std::string::npos)
        {
            shouldExpectCoordinates = true;
        }
        else if(instanceCfgLine.find("EDGE_WEIGHT_TYPE") != std::string::npos)
        {
            auto value = prepareValueToStore(instanceCfgLine);
            tspInstance.edgeWeightType = std::move(value);
        }
        else if(instanceCfgLine.find("TYPE") != std::string::npos)
        {
            // nothing to do
        }
        else if(instanceCfgLine.find("COMMENT") != std::string::npos)
        {
            // nothing to do
        }
        else if(instanceCfgLine.find("EOF") != std::string::npos)
        {
            // nothing to do
        }
        else
        {
            if(shouldExpectCoordinates)
            {
                storeCityData(instanceCfgLine);
            }
            else
            {
                throw LoaderException("Invalid line in tsp cfg file: " + instanceCfgLine);
            }
        }
    }
    catch(std::invalid_argument& e)
    {
        throw LoaderException("Invalid conversion to number: " + std::string{e.what()});
    }
}

void TspInstanceLoader::storeCityData(const std::string& line)
{
    std::istringstream ss(line);
    std::string token;
    std::vector<std::string> tokens{std::istream_iterator<std::string>{ss},
                                    std::istream_iterator<std::string>{}};
    if(tokens.size() != 3)
        throw LoaderException("Parsing city data - bad file structure: " + line);
    auto index = static_cast<uint32_t>(std::stoi(tokens[0]) - 1);
    tspInstance.citiesData[index] = tsp::CityData{index, std::stod(tokens[1]), std::stod(tokens[2])};
}

} // namespace loader
} // namespace configuration
