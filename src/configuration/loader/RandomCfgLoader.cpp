#include "RandomCfgLoader.hpp"
#include "LoaderException.hpp"
#include "LoaderUtilsFunctions.hpp"

namespace configuration {
namespace loader {

RandomCfgLoader::RandomCfgLoader(AlgoCfg& algoCfg)
    : AlgoSpecificLoader()
    , randomCfg{dynamic_cast<RandomCfg&>(algoCfg)}
{
}

void RandomCfgLoader::decideWhatToDoWithLine(const std::string& line)
{
    if(line.find("ITERATIONS:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        randomCfg.iterations = static_cast<uint32_t>(std::stoul(value));
    }
    else
    {
        throw LoaderException("Unknown tokens in RANDOM config: " + line);
    }
}

} // namespace loader
} // namespace configuration
