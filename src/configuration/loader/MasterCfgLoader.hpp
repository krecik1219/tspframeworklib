#pragma once

#include <configuration/MasterCfg.hpp>
#include <string>

namespace configuration {
namespace loader {

class MasterCfgLoader
{
public:
    MasterCfg loadMasterCfg(const std::string& masterCfgPath);
};

} // namespace loader
} // namespace configuration
