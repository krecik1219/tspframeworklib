#pragma once

#include <configuration/HybridGaSaCfg.hpp>
#include <configuration/AlgoCfg.hpp>
#include "AlgoSpecificLoader.hpp"
#include <string>

namespace configuration {
namespace loader {

class HybridGaSaCfgLoader : public AlgoSpecificLoader
{
public:
    HybridGaSaCfgLoader(AlgoCfg& algoCfg);

    virtual void decideWhatToDoWithLine(const std::string& line) override;

protected:
    HybridGaSaCfg& hybridGaSaCfg;
};

} // namespace loader
} // namespace configuration
