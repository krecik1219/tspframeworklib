#pragma once

#include <configuration/SaCfg.hpp>
#include <configuration/AlgoCfg.hpp>
#include "AlgoSpecificLoader.hpp"
#include <string>

namespace configuration {
namespace loader {

class SaCfgLoader : public AlgoSpecificLoader
{
public:
    SaCfgLoader(AlgoCfg& algoCfg);

    virtual void decideWhatToDoWithLine(const std::string& line) override;

protected:
    SaCfg& saCfg;
};

} // namespace loader
} // namespace configuration
