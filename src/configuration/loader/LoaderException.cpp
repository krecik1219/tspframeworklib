#include "LoaderException.hpp"

namespace configuration {
namespace loader {

LoaderException::LoaderException(const std::string& msg)
    : std::runtime_error("Config loading failed. " + msg)
{
}

} // namespace loader
} // namespace configuration
