#pragma once

#include <configuration/GreedyCfg.hpp>
#include <configuration/AlgoCfg.hpp>
#include "AlgoSpecificLoader.hpp"
#include <string>

namespace configuration {
namespace loader {

class GreedyCfgLoader : public AlgoSpecificLoader
{
public:
    GreedyCfgLoader(AlgoCfg& algoCfg);

    virtual void decideWhatToDoWithLine(const std::string& line) override;

protected:
    GreedyCfg& greedyCfg;
};

} // namespace loader
} // namespace configuration
