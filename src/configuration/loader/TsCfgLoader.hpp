#pragma once

#include <configuration/TsCfg.hpp>
#include <configuration/AlgoCfg.hpp>
#include "AlgoSpecificLoader.hpp"
#include <string>

namespace configuration {
namespace loader {

class TsCfgLoader : public AlgoSpecificLoader
{
public:
    TsCfgLoader(AlgoCfg& algoCfg);

    virtual void decideWhatToDoWithLine(const std::string& line) override;

protected:
    TsCfg& tsCfg;
};

} // namespace loader
} // namespace configuration
