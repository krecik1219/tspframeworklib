#include "HybridGaTsCfgLoader.hpp"
#include "LoaderException.hpp"
#include "LoaderUtilsFunctions.hpp"

namespace configuration {
namespace loader {

HybridGaTsCfgLoader::HybridGaTsCfgLoader(AlgoCfg& algoCfg)
    : AlgoSpecificLoader()
    , hybridGaTsCfg{dynamic_cast<HybridGaTsCfg&>(algoCfg)}
{
}

void HybridGaTsCfgLoader::decideWhatToDoWithLine(const std::string &line)
{
    if (line.find("POPULATION SIZE:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaTsCfg.gaCfg.populationSize = static_cast<uint32_t>(std::stoul(value));
    }
    else if (line.find("SELECTION STRATEGY:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaTsCfg.gaCfg.selectionStrategy = std::move(value);
    }
    else if (line.find("TOURNAMENT SIZE:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaTsCfg.gaCfg.tournamentSize = static_cast<uint32_t>(std::stoul(value));
    }
    else if (line.find("MAX GENERATIONS NUM:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaTsCfg.gaCfg.maxGenerationsNum = static_cast<uint32_t>(std::stoul(value));
    }
    else if (line.find("CROSSOVER PROBABILITY:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaTsCfg.gaCfg.crossoverProb = std::stod(value);
    }
    else if (line.find("MUTATION PROBABILITY:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaTsCfg.gaCfg.mutationProb = std::stod(value);
    }
    else if(line.find("TABU MEMORY DEPTH:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaTsCfg.tabuMemoryDepth = static_cast<uint32_t>(std::stoul(value));
    }
    else
    {
        throw LoaderException("Unknown tokens in HYBRID GA+TS config: " + line);
    }
}

} // namespace loader
} // namespace configuration
