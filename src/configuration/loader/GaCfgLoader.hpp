#pragma once

#include <configuration/GaCfg.hpp>
#include <configuration/AlgoCfg.hpp>
#include "AlgoSpecificLoader.hpp"
#include <string>

namespace configuration {
namespace loader {

class GaCfgLoader : public AlgoSpecificLoader
{
public:
    GaCfgLoader(AlgoCfg& algoCfg);

    virtual void decideWhatToDoWithLine(const std::string& line) override;

protected:
    GaCfg& gaCfg;
};

} // namespace loader
} // namespace configuration
