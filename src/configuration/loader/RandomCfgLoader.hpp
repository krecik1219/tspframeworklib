#pragma once

#include <configuration/RandomCfg.hpp>
#include <configuration/AlgoCfg.hpp>
#include "AlgoSpecificLoader.hpp"
#include <string>

namespace configuration {
namespace loader {

class RandomCfgLoader : public AlgoSpecificLoader
{
public:
    RandomCfgLoader(AlgoCfg& algoCfg);

    virtual void decideWhatToDoWithLine(const std::string& line) override;

protected:
    RandomCfg& randomCfg;
};

} // namespace loader
} // namespace configuration
