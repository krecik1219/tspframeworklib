#pragma once

#include <configuration/RunCfg.hpp>
#include "AlgoSpecificLoader.hpp"

namespace configuration {
namespace loader {

class RunCfgLoader
{
public:
    RunCfgLoader();
    ~RunCfgLoader();

    RunCfgLoader(const RunCfgLoader&) = delete;
    RunCfgLoader& operator=(const RunCfgLoader&) = delete;
    RunCfgLoader(RunCfgLoader&&) = delete;
    RunCfgLoader& operator=(RunCfgLoader&&) = delete;

    RunCfg loadRunCfg(const std::string& runCfgPath);

private:
    void decideWhatToDoWithLine(const std::string& line);
    void makeAlgoCfg(const std::string& line);
    void makeAlgoSpecificLoader();

    RunCfg runCfg;
    std::unique_ptr<AlgoSpecificLoader> algoSpecificLoader;
};

} // namespace loader
} // namespace configuration
