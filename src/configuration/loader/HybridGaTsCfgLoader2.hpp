#pragma once

#include <configuration/HybridGaTsCfg2.hpp>
#include <configuration/AlgoCfg.hpp>
#include "AlgoSpecificLoader.hpp"
#include <string>

namespace configuration {
namespace loader {

class HybridGaTsCfgLoader2 : public AlgoSpecificLoader
{
public:
    HybridGaTsCfgLoader2(AlgoCfg& algoCfg);

    virtual void decideWhatToDoWithLine(const std::string& line) override;

protected:
    HybridGaTsCfg2& hybridGaTsCfg;
};

} // namespace loader
} // namespace configuration
