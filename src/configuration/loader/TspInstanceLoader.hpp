#pragma once

#include <string>
#include <configuration/TspInstance.hpp>

namespace configuration {
namespace loader {

class TspInstanceLoader
{
public:
    TspInstanceLoader();
    ~TspInstanceLoader();

    TspInstanceLoader(const TspInstanceLoader&) = delete;
    TspInstanceLoader& operator=(const TspInstanceLoader&) = delete;
    TspInstanceLoader(TspInstanceLoader&&) = delete;
    TspInstanceLoader& operator=(TspInstanceLoader&&) = delete;

    tsp::TspInstance loadTspInstance(const std::string& instanceCfgPath);

private:
    void decideWhatToDoWithLine(const std::string& instanceCfgLine);
    void storeCityData(const std::string& line);

    tsp::TspInstance tspInstance;
    bool shouldExpectCoordinates;
};

} // namespace loader
} // namespace configuration
