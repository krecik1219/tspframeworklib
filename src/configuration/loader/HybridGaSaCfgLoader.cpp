#include "HybridGaSaCfgLoader.hpp"
#include "LoaderException.hpp"
#include "LoaderUtilsFunctions.hpp"

namespace configuration {
namespace loader {

HybridGaSaCfgLoader::HybridGaSaCfgLoader(AlgoCfg& algoCfg)
    : AlgoSpecificLoader()
    , hybridGaSaCfg{dynamic_cast<HybridGaSaCfg&>(algoCfg)}
{
}

void HybridGaSaCfgLoader::decideWhatToDoWithLine(const std::string &line)
{
    if (line.find("POPULATION SIZE:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaSaCfg.gaCfg.populationSize = static_cast<uint32_t>(std::stoul(value));
    }
    else if (line.find("SELECTION STRATEGY:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaSaCfg.gaCfg.selectionStrategy = std::move(value);
    }
    else if (line.find("TOURNAMENT SIZE:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaSaCfg.gaCfg.tournamentSize = static_cast<uint32_t>(std::stoul(value));
    }
    else if (line.find("MAX GENERATIONS NUM:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaSaCfg.gaCfg.maxGenerationsNum = static_cast<uint32_t>(std::stoul(value));
    }
    else if (line.find("CROSSOVER PROBABILITY:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaSaCfg.gaCfg.crossoverProb = std::stod(value);
    }
    else if (line.find("MUTATION PROBABILITY:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaSaCfg.gaCfg.mutationProb = std::stod(value);
    }
    else if(line.find("INIT TEMP:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaSaCfg.saCfg.initTemp = std::stod(value);
    }
    else if (line.find("TEMP STEP FACTOR:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaSaCfg.saCfg.tempStepFactor = std::stod(value);
    }
    else if (line.find("ITERATIONS PER TEMP:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaSaCfg.saCfg.iterationsPerTemp = static_cast<uint32_t>(std::stoul(value));
    }
    else if(line.find("PERCENT OF POPULATION TO OPTIMIZE WITH SA:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaSaCfg.percentOfPopulationToOptimizeWithSa = std::stod(value);
    }
    else if(line.find("SA OPTIMIZATION GENERATIONS INTERVAL:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        hybridGaSaCfg.saOptimizationGenerationsInterval = static_cast<uint32_t>(std::stoul(value));
    }
    else
    {
        throw LoaderException("Unknown tokens in HYBRID GA+SA config: " + line);
    }
}

} // namespace loader
} // namespace configuration
