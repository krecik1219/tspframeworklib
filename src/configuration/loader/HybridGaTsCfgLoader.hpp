#pragma once

#include <configuration/HybridGaTsCfg.hpp>
#include <configuration/AlgoCfg.hpp>
#include "AlgoSpecificLoader.hpp"
#include <string>

namespace configuration {
namespace loader {

class HybridGaTsCfgLoader : public AlgoSpecificLoader
{
public:
    HybridGaTsCfgLoader(AlgoCfg& algoCfg);

    virtual void decideWhatToDoWithLine(const std::string& line) override;

protected:
    HybridGaTsCfg& hybridGaTsCfg;
};

} // namespace loader
} // namespace configuration
