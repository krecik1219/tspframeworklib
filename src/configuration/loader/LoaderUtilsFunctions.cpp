#include "LoaderUtilsFunctions.hpp"
#include <utils/StringUtils.hpp>
#include "LoaderException.hpp"

namespace configuration {
namespace loader {

std::string prepareValueToStore(const std::string& s)
{
    auto pos = s.find(':');
    if (pos == std::string::npos)
        throw LoaderException("Bad file structure at line: \"" + s + "\"");
    auto value = s.substr(pos + 1, s.length() - pos - 1);
    utils::str::trim(value);
    return value;
}

bool isWhitespaceLine(const std::string& line)
{
    if(line.empty() || (line.length() == 1 && std::isspace(line[0])))
        return true;
    else
        return false;
}

} // namespace loader
} // namespace configuration
