#pragma once

#include <stdexcept>

namespace configuration {
namespace loader {

class LoaderException : public std::runtime_error
{
public:
    LoaderException(const std::string& msg);
};

} // namespace loader
} // namespace configuration
