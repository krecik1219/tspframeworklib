#include "MasterCfgLoader.hpp"
#include "LoaderException.hpp"
#include "LoaderUtilsFunctions.hpp"
#include <fstream>

namespace configuration {
namespace loader {

MasterCfg MasterCfgLoader::loadMasterCfg(const std::string& masterCfgPath)
{
    std::ifstream masterCfgFile{masterCfgPath};
    if(!masterCfgFile)
        throw LoaderException("Can't opem master config fle at path: " + masterCfgPath);

    MasterCfg masterCfg{};
    std::string masterCfgLine;
    while(std::getline(masterCfgFile, masterCfgLine))
    {
        if(isWhitespaceLine(masterCfgLine))
            continue;
        masterCfg.runConfigsPaths.push_back(masterCfgLine);
    }
    return masterCfg;
}

} // namespace loader
} // namespace configuration
