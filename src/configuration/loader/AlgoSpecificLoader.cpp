#include "AlgoSpecificLoader.hpp"

namespace configuration {
namespace loader {

AlgoSpecificLoader::AlgoSpecificLoader() = default;
AlgoSpecificLoader::~AlgoSpecificLoader() = default;

} // namespace loader
} // namespace configuration
