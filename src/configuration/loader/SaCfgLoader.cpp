#include "SaCfgLoader.hpp"
#include "LoaderException.hpp"
#include "LoaderUtilsFunctions.hpp"

namespace configuration {
namespace loader {

SaCfgLoader::SaCfgLoader(AlgoCfg& algoCfg)
    : AlgoSpecificLoader()
    , saCfg{dynamic_cast<SaCfg&>(algoCfg)}
{
}

void SaCfgLoader::decideWhatToDoWithLine(const std::string &line)
{
    if(line.find("INIT TEMP:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        saCfg.initTemp = std::stod(value);
    }
    else if (line.find("TEMP STEP FACTOR:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        saCfg.tempStepFactor = std::stod(value);
    }
    else if (line.find("ITERATIONS PER TEMP:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        saCfg.iterationsPerTemp = static_cast<uint32_t>(std::stoul(value));
    }
    else
    {
        throw LoaderException("Unknown tokens in SA config: " + line);
    }
}

} // namespace loader
} // namespace configuration
