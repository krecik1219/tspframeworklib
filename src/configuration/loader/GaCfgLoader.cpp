#include "GaCfgLoader.hpp"
#include "LoaderException.hpp"
#include "LoaderUtilsFunctions.hpp"

namespace configuration {
namespace loader {

GaCfgLoader::GaCfgLoader(AlgoCfg& algoCfg)
    : AlgoSpecificLoader()
    , gaCfg{dynamic_cast<GaCfg&>(algoCfg)}
{
}

void GaCfgLoader::decideWhatToDoWithLine(const std::string &line)
{
    if (line.find("POPULATION SIZE:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        gaCfg.populationSize = static_cast<uint32_t>(std::stoul(value));
    }
    else if (line.find("SELECTION STRATEGY:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        gaCfg.selectionStrategy = std::move(value);
    }
    else if (line.find("TOURNAMENT SIZE:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        gaCfg.tournamentSize = static_cast<uint32_t>(std::stoul(value));
    }
    else if (line.find("MAX GENERATIONS NUM:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        gaCfg.maxGenerationsNum = static_cast<uint32_t>(std::stoul(value));
    }
    else if (line.find("CROSSOVER PROBABILITY:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        gaCfg.crossoverProb = std::stod(value);
    }
    else if (line.find("MUTATION PROBABILITY:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        gaCfg.mutationProb = std::stod(value);
    }
    else
    {
        throw LoaderException("Unknown tokens in GA config: " + line);
    }
}

} // namespace loader
} // namespace configuration
