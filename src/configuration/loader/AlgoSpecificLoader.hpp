#pragma once

#include <configuration/AlgoCfg.hpp>
#include <string>

namespace configuration {
namespace loader {

class AlgoSpecificLoader
{
public:
    AlgoSpecificLoader();
    virtual ~AlgoSpecificLoader();

    AlgoSpecificLoader(const AlgoSpecificLoader&) = delete;
    AlgoSpecificLoader& operator=(AlgoSpecificLoader&) = delete;
    AlgoSpecificLoader(AlgoSpecificLoader&&) = delete;
    AlgoSpecificLoader& operator=(AlgoSpecificLoader&&) = delete;

    virtual void decideWhatToDoWithLine(const std::string& line) = 0;
};

} // namespace loader
} // namespace configuration
