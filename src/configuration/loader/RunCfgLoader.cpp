#include "RunCfgLoader.hpp"
#include "LoaderException.hpp"
#include "LoaderUtilsFunctions.hpp"
#include <fstream>
#include <memory>
#include <configuration/GaCfg.hpp>
#include <configuration/HybridGaSaCfg.hpp>
#include <configuration/TsCfg.hpp>
#include <configuration/SaCfg.hpp>
#include <configuration/GreedyCfg.hpp>
#include <configuration/RandomCfg.hpp>
#include <configuration/HybridGaTsCfg.hpp>
#include <configuration/HybridGaTsCfg2.hpp>
#include "GaCfgLoader.hpp"
#include "HybridGaSaCfgLoader.hpp"
#include "TsCfgLoader.hpp"
#include "SaCfgLoader.hpp"
#include "GreedyCfgLoader.hpp"
#include "RandomCfgLoader.hpp"
#include "HybridGaTsCfgLoader.hpp"
#include "HybridGaTsCfgLoader2.hpp"

namespace configuration {
namespace loader {

RunCfgLoader::RunCfgLoader() = default;
RunCfgLoader::~RunCfgLoader() = default;

RunCfg RunCfgLoader::loadRunCfg(const std::string& runCfgPath)
{
    std::ifstream runCfgFile{runCfgPath};
    if(!runCfgFile)
        throw LoaderException("Can't open run config file at path: " + runCfgPath);

    runCfg = RunCfg{};
    algoSpecificLoader = nullptr;
    std::string runCfgLine;
    while(std::getline(runCfgFile, runCfgLine))
    {
        if(isWhitespaceLine(runCfgLine))
            continue;
        decideWhatToDoWithLine(runCfgLine);
    }
    return runCfg;
}

void RunCfgLoader::decideWhatToDoWithLine(const std::string &line)
{
    if(algoSpecificLoader != nullptr)
    {
        algoSpecificLoader->decideWhatToDoWithLine(line);
        return;
    }

    try
    {
        if (line.find("REPETITIONS:") != std::string::npos)
        {
            auto value = prepareValueToStore(line);
            runCfg.repetitionsNum = static_cast<uint32_t>(std::stoul(value));
        }
        else if (line.find("INSTANCE CONFIG PATH:") != std::string::npos)
        {
            auto value = prepareValueToStore(line);
            runCfg.tspInstanceCfgPath = std::move(value);
        }
        else if (line.find("RESULTS CSV FILE:") != std::string::npos)
        {
            auto value = prepareValueToStore(line);
            runCfg.resultsCsvFile = std::move(value);
        }
        else if (line.find("MAX ALG SECONDS DURATION:") != std::string::npos)
        {
            auto value = prepareValueToStore(line);
            runCfg.maxAlgSecondsDuration = std::chrono::seconds(std::stoi(value));
        }
        else if (line.find("ALGO TYPE:") != std::string::npos)
        {
            makeAlgoCfg(line);
            makeAlgoSpecificLoader();
        }
        else
        {
            throw LoaderException("Unexpected tokens in run config file: " + line);
        }
    }
    catch(std::invalid_argument& e)
    {
        throw LoaderException("Invalid conversion to number: " + std::string{e.what()});
    }
}

void RunCfgLoader::makeAlgoCfg(const std::string& line)
{
    auto value = prepareValueToStore(line);
    if(value == "GA")
    {
        runCfg.algoCfg = std::make_unique<GaCfg>();
    }
    else if(value == "TS")
    {
        runCfg.algoCfg = std::make_unique<TsCfg>();
    }
    else if(value == "SA")
    {
        runCfg.algoCfg = std::make_unique<SaCfg>();
    }
    else if(value == "GA+SA")
    {
        runCfg.algoCfg = std::make_unique<HybridGaSaCfg>();
    }
    else if(value == "GA+TS")
    {
        runCfg.algoCfg = std::make_unique<HybridGaTsCfg>();
    }
    else if(value == "GA+TS_2")
    {
        runCfg.algoCfg = std::make_unique<HybridGaTsCfg2>();
    }
    else if(value == "GREEDY")
    {
        runCfg.algoCfg = std::make_unique<GreedyCfg>();
    }
    else if(value == "RANDOM")
    {
        runCfg.algoCfg = std::make_unique<RandomCfg>();
    }
    else
    {
        throw LoaderException("Invalid algorithm type provided: " + value);
    }
}

void RunCfgLoader::makeAlgoSpecificLoader()
{
    try
    {
        switch(runCfg.algoCfg->algoType)
        {
            case algo::AlgoType::GA:
                algoSpecificLoader = std::make_unique<GaCfgLoader>(*runCfg.algoCfg);
                break;
            case algo::AlgoType::TS:
                algoSpecificLoader = std::make_unique<TsCfgLoader>(*runCfg.algoCfg);
                break;
            case algo::AlgoType::SA:
                algoSpecificLoader = std::make_unique<SaCfgLoader>(*runCfg.algoCfg);
                break;
            case algo::AlgoType::HYBRID_GA_SA:
                algoSpecificLoader = std::make_unique<HybridGaSaCfgLoader>(*runCfg.algoCfg);
                break;
            case algo::AlgoType::HYBRID_GA_TS:
                algoSpecificLoader = std::make_unique<HybridGaTsCfgLoader>(*runCfg.algoCfg);
                break;
            case algo::AlgoType::HYBRID_GA_TS_2:
                algoSpecificLoader = std::make_unique<HybridGaTsCfgLoader2>(*runCfg.algoCfg);
                break;
            case algo::AlgoType::GREEDY:
                algoSpecificLoader = std::make_unique<GreedyCfgLoader>(*runCfg.algoCfg);
                break;
            case algo::AlgoType::RANDOM:
                algoSpecificLoader = std::make_unique<RandomCfgLoader>(*runCfg.algoCfg);
                break;
            default:
                throw LoaderException("Algorithm type not specified or unknown");
        }
    }
    catch(std::bad_cast&)
    {
        throw LoaderException("Internal error, inconsistency in algorithm type");
    }
}

} // namespace loader
} // namespace configuration
