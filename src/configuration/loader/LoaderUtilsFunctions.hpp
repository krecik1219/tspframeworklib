#pragma once

#include <string>

namespace configuration {
namespace loader {

std::string prepareValueToStore(const std::string& s);
bool isWhitespaceLine(const std::string& line);

} // namespace loader
} // namespace configuration
