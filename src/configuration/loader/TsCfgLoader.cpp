#include "TsCfgLoader.hpp"
#include "LoaderException.hpp"
#include "LoaderUtilsFunctions.hpp"

namespace configuration {
namespace loader {

TsCfgLoader::TsCfgLoader(AlgoCfg& algoCfg)
    : AlgoSpecificLoader()
    , tsCfg{dynamic_cast<TsCfg&>(algoCfg)}
{
}

void TsCfgLoader::decideWhatToDoWithLine(const std::string &line)
{
    if (line.find("ITERATIONS NUM:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        tsCfg.iterationsNum = static_cast<uint32_t>(std::stoul(value));
    }
    else if (line.find("TABU MEMORY DEPTH:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        tsCfg.tabuMemoryDepth = static_cast<uint32_t>(std::stoul(value));
    }
    else if (line.find("MAX NEIGHBOURHOOD SIZE:") != std::string::npos)
    {
        auto value = prepareValueToStore(line);
        tsCfg.maxNeighbourhoodSize = static_cast<uint32_t>(std::stoul(value));
    }
    else
    {
        throw LoaderException("Unknown tokens in TS config: " + line);
    }
}

} // namespace loader
} // namespace configuration
