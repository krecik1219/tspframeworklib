#pragma once

#include <string>
#include <vector>

namespace configuration {

struct MasterCfg
{
    std::vector<std::string> runConfigsPaths;
};

} // namespace configuration
