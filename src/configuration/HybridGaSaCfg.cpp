#include "HybridGaSaCfg.hpp"

namespace configuration {

HybridGaSaCfg::HybridGaSaCfg()
    : AlgoCfg(algo::AlgoType::HYBRID_GA_SA)
{
}

} // namespace configuration
