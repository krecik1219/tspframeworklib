#pragma once

#include <cstdint>
#include "AlgoCfg.hpp"

namespace configuration {

class TsCfg : public AlgoCfg
{
public:
    TsCfg();

    uint32_t iterationsNum;
    uint32_t tabuMemoryDepth;
    uint32_t maxNeighbourhoodSize;
};

} // namespace configuration
