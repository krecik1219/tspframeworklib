#pragma once

#include <algo/AlgoType.hpp>

namespace configuration {

class AlgoCfg
{
public:
    AlgoCfg() = delete;

    AlgoCfg(const algo::AlgoType algoType);
    virtual ~AlgoCfg();
    AlgoCfg(const AlgoCfg&);
    AlgoCfg& operator=(const AlgoCfg&);
    AlgoCfg(AlgoCfg&&);
    AlgoCfg& operator=(AlgoCfg&&);

    algo::AlgoType algoType;
};

} // namespace configuration
