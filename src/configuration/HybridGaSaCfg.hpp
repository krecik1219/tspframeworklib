#pragma once

#include "AlgoCfg.hpp"
#include "GaCfg.hpp"
#include "SaCfg.hpp"
#include <string>

namespace configuration {

class HybridGaSaCfg : public AlgoCfg
{
public:
    HybridGaSaCfg();

    GaCfg gaCfg;
    SaCfg saCfg;
    double percentOfPopulationToOptimizeWithSa;
    uint32_t saOptimizationGenerationsInterval;
};

} // namespace configuration
