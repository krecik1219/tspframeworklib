#pragma once

#include <cstdint>
#include <vector>
#include <string>
#include <unordered_map>

namespace configuration {
namespace tsp {

using DistanceMatrix = std::vector<std::vector<double>>;

struct CityData
{
    uint32_t id;
    double xCoord;
    double yCoord;
};

struct TspInstance
{
    std::string name;
    uint32_t dimension;
    std::string edgeWeightType;
    DistanceMatrix distanceMatrix;
    std::vector<CityData> citiesData;
    std::unordered_map<uint32_t, std::pair<uint32_t, double>> nearestDistanceLookup;

    void resizeToDimension();
    void computeDistanceMatrix();
    void fillNearestDistanceLookup();

private:
    double computeDistance(const uint32_t cityIdA, const uint32_t cityIdB) const;
};

} // namespace tsp
} // namespace configuration
