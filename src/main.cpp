#include <iostream>
#include <runner/ProblemSolverRunner.hpp>

int main(int argc, char** argv)
{
    if(argc != 2)
    {
        std::cout << "Path to master cfg required" << std::endl;
        return -1;
    }

    runner::ProblemSolverRunner problemSolverRunner{argv[1]};
    problemSolverRunner.solveProblems();

    return 0;
}
