#include "TspSolution.hpp"

namespace tsp {

double computeTotalDistance(const CitySequence& citySequence, const cfgtsp::TspInstance& tspCfg)
{
    double totalDistance = 0.0;
    if(citySequence.size() < 2)
        return totalDistance;

    for(auto i = 0u; i < citySequence.size() - 1; i++)
    {
        totalDistance += tspCfg.distanceMatrix[citySequence[i]][citySequence[i + 1]];
    }
    // from last to first
    totalDistance += tspCfg.distanceMatrix[citySequence[citySequence.size() - 1]][citySequence[0]];
    return totalDistance;
}

TspSolution::TspSolution(const cfgtsp::TspInstance& tspCfg, const uint32_t problemSize)
    : tspCfg{tspCfg}
    , citySequence(problemSize)
{
}

TspSolution::TspSolution(const cfgtsp::TspInstance& tspCfg, CitySequence &&citySequence)
    : tspCfg{tspCfg}
    , citySequence(std::move(citySequence))
{
}

TspSolution::~TspSolution() = default;
TspSolution::TspSolution(const TspSolution&) = default;

double TspSolution::computeTotalDistance() const
{
    return tsp::computeTotalDistance(citySequence, tspCfg);
}

void TspSolution::setCityIdAtIndex(const uint32_t cityId, const uint32_t indexInSequence)
{
    citySequence[indexInSequence] = cityId;
}

uint32_t TspSolution::getCityIdAtIndex(const uint32_t indexInSequence) const
{
    return citySequence[indexInSequence];
}

const CitySequence& TspSolution::getCitySequence() const
{
    return citySequence;
}

const configuration::tsp::TspInstance &TspSolution::getTspCfg() const
{
    return tspCfg;
}

std::string TspSolution::toString() const
{
    std::string repr = "cities: [";
    for(auto i = 0u; i < citySequence.size(); i++)
    {
        repr += std::to_string(citySequence[i]);
        if(i < citySequence.size() - 1)
            repr += ", ";
    }
    repr += "]";
    return repr;
}

} // namespace tsp
