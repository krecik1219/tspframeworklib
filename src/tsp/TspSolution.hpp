#pragma once

#include <configuration/TspInstance.hpp>
#include <cstdint>
#include <vector>

namespace tsp {

namespace cfgtsp = configuration::tsp;

using CitySequence = std::vector<uint32_t>;

double computeTotalDistance(const CitySequence& citySequence, const cfgtsp::TspInstance& tspCfg);

class TspSolution
{
public:
    TspSolution(const cfgtsp::TspInstance& tspCfg, const uint32_t problemSize);
    TspSolution(const cfgtsp::TspInstance& tspCfg, CitySequence&& citySequence);

    TspSolution() = delete;

    virtual ~TspSolution();
    TspSolution(const TspSolution&);
    TspSolution& operator=(const TspSolution&) = delete;
    TspSolution(TspSolution&&) = delete;
    TspSolution& operator=(TspSolution&&) = delete;

    double computeTotalDistance() const;
    void setCityIdAtIndex(const uint32_t cityId, const uint32_t indexInSequence);
    uint32_t getCityIdAtIndex(const uint32_t indexInSequence) const;
    const CitySequence& getCitySequence() const;
    const cfgtsp::TspInstance& getTspCfg() const;
    virtual std::string toString() const;

protected:
    const cfgtsp::TspInstance& tspCfg;
    CitySequence citySequence;
};

} // namespace tsp
