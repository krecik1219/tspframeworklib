#include "LoggerExtension.hpp"

namespace tools {

LoggerExtension::LoggerExtension() = default;
LoggerExtension::~LoggerExtension() = default;

} // namespace tools
