#include "Logger.hpp"

namespace tools {

Logger::Logger(LoggingPolicyPtr&& policy, bool mtSafe, bool skipLogging)
    : mtSafe{mtSafe}
    , skipLogging{skipLogging}
{
    loggingPolicies.push_back(std::move(policy));
}

Logger::~Logger() = default;

Logger& Logger::addLoggingPolicy(LoggingPolicyPtr&& policy)
{
    loggingPolicies.push_back(std::move(policy));
    return *this;
}

Logger& Logger::addLoggerExtension(LoggerExtensionPtr&& extension)
{
    loggerExtensions.push_back(std::move(extension));
    return *this;
}

uint32_t Logger::extendLogLine(char* buffer)
{
    uint32_t totalExtensionsLength = 0u;
    for(auto& extension : loggerExtensions)
    {
        uint32_t extensionSize = extension->extendLogLine(buffer);
        buffer[extensionSize + 1] = ' ';
        buffer = buffer + extensionSize + 1;
        totalExtensionsLength += extensionSize + 1;
    }
    return totalExtensionsLength;
}

} // namespace tools
