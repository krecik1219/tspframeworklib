#pragma once

#include <cstring>
#include <memory>
#include <fstream>
#include <mutex>
#include <vector>

#include "LoggingPolicy.hpp"
#include "LoggerExtension.hpp"

namespace tools {

using LoggingPolicyPtr = std::unique_ptr<LoggingPolicy>;
using LoggerExtensionPtr = std::unique_ptr<LoggerExtension>;

class Logger
{
public:
    Logger(LoggingPolicyPtr&& policy, const bool mtSafe = false, const bool skipLogging = false);
    ~Logger();

    Logger() = delete;
    Logger(const Logger&) = delete;
    Logger& operator=(const Logger&) = delete;
    Logger(Logger&&) = delete;
    Logger& operator=(Logger&&) = delete;

    Logger& addLoggingPolicy(LoggingPolicyPtr&& policy);
    Logger& addLoggerExtension(LoggerExtensionPtr&& extension);

    template<class... Args>
    void log(const char* format, Args... args);

private:
    uint32_t extendLogLine(char* buffer);

    const bool mtSafe;
    const bool skipLogging;
    std::mutex logMutex;
    std::vector<LoggingPolicyPtr> loggingPolicies;
    std::vector<LoggerExtensionPtr> loggerExtensions;
};

template<class... Args>
void Logger::log(const char* format, Args... args)
{
    if(skipLogging)
        return;
    std::unique_ptr<std::lock_guard<std::mutex>> lock;
    if(mtSafe)
        lock = std::make_unique<std::lock_guard<std::mutex>>(logMutex);
    const uint32_t bufferLength = 15000;
    auto buffer = std::make_unique<char[]>(bufferLength);
    uint32_t extensionsLength = extendLogLine(buffer.get());
    std::snprintf(buffer.get() + extensionsLength, bufferLength - extensionsLength, format, args...);
    for(auto& policy : loggingPolicies)
        policy->log(buffer.get());
}

} // namespace tools
