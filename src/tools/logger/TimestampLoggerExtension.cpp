#include "TimestampLoggerExtension.hpp"
#include <ctime>
#include <cstring>
#include <chrono>

namespace tools {

uint32_t TimestampLoggerExtension::extendLogLine(char* bufferPtr)
{
    std::time_t currTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    struct tm* time = std::localtime(&currTime);
    constexpr uint32_t bufferTimeSize = 8 + 1;
    char bufferTime[bufferTimeSize]{};
    std::strftime(bufferTime, sizeof(bufferTime), "%H:%M:%S", time);
    std::strcpy(bufferPtr, bufferTime);
    return bufferTimeSize - 1;
}

} // namespace tools
