#pragma once

#include <cstdint>

namespace tools {

class LoggerExtension
{
public:
    LoggerExtension();
    virtual ~LoggerExtension();

    LoggerExtension(const LoggerExtension&) = delete;
    LoggerExtension& operator=(const LoggerExtension&) = delete;
    LoggerExtension(LoggerExtension&&) = delete;
    LoggerExtension& operator=(LoggerExtension&&) = delete;

    virtual uint32_t extendLogLine(char* bufferPtr) = 0;
};

} // namespace tools
