#pragma once

#include "LoggerExtension.hpp"
#include <cstdint>

namespace tools {

class TimestampLoggerExtension : public LoggerExtension
{
public:
    virtual uint32_t extendLogLine(char* bufferPtr);
};

} // namespace tools
