#pragma once

#include <string>
#include <fstream>
#include <experimental/filesystem>

#include "LoggingPolicy.hpp"

namespace tools {

namespace fs = std::experimental::filesystem;

class FileLoggingPolicy : public LoggingPolicy
{
public:
    FileLoggingPolicy(const std::string& dirName, const std::string& baseFileName, bool shouldUseNewFile);

    virtual ~FileLoggingPolicy() override;

    FileLoggingPolicy() = delete;
    FileLoggingPolicy(const FileLoggingPolicy&) = delete;
    FileLoggingPolicy& operator=(const FileLoggingPolicy&) = delete;
    FileLoggingPolicy(FileLoggingPolicy&&) = delete;
    FileLoggingPolicy& operator=(FileLoggingPolicy&&) = delete;

    virtual void log(const char* buffer) override;

private:
    void flushInternalBuffer();
    std::string internalBuffer;
    void createLogsDirIfNotExists(fs::path& logsDirPath) const;
    std::string getNextLogFileFullPath(const fs::path& logsDir, const std::string& baseFileName) const;

    std::ofstream outputStream;

    static constexpr uint32_t INTERNAL_BUFFER_MAX_SIZE = 1000;
};

} // namespace tools
