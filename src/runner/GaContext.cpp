#include "GaContext.hpp"
#include <memory>
#include <algo/ga/IndividualInterface.hpp>
#include <algo/ga/TspIndividual.hpp>
#include <tools/logger/Logger.hpp>
#include <tools/logger/FileLoggingPolicy.hpp>

namespace runner {

using namespace algo::ga;

void GaContext::runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath)
{
    tspInstanceCfg = tspInstanceLoader.loadTspInstance(runCfg.tspInstanceCfgPath);
    auto& tspCfg = tspInstanceCfg;
    auto& generator = g;
    initFunction = [&tspCfg, &generator](){
        return TspIndividual::createRandom(tspCfg, generator, CrossoverType::OX);
    };
    gaCfg = dynamic_cast<const configuration::GaCfg&>(*runCfg.algoCfg);
    for(auto i = 0u; i < runCfg.repetitionsNum; i++)
    {
        makeLoggers(runCfg, runCfgPath);
        runSingleProblemRepetition();
    }
}

void GaContext::runSingleProblemRepetition()
{
    auto geneticAlgo = makeAlgo();
    auto start = std::chrono::steady_clock::now();
    geneticAlgo->run();
    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    IndividualPtr bestEver = geneticAlgo->getBestEverIndividual();
    additionalLogger->log("Elapsed=%.3f seconds, best ever: %s", elapsed.count() / 1000.0, bestEver->toString().c_str());
}

std::unique_ptr<GeneticAlgorithm> GaContext::makeAlgo()
{
    std::unique_ptr<SelectionStrategy> selectionStrategy = selectionStrategyFactory.make(gaCfg);
    return std::make_unique<GeneticAlgorithm>(gaCfg, initFunction, std::move(selectionStrategy), *mainLogger);
}

} // namespace runner
