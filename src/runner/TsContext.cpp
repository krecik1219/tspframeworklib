#include "TsContext.hpp"
#include <algo/ts/TspSolutionTabuSearch.hpp>
#include <algo/ts/TsSolutionAbstractFactory.hpp>
#include <algo/ts/SwapTabuFactory.hpp>
#include <algo/ts/SequenceTabuFactory.hpp>
#include <algo/ts/InversionTabuFactory.hpp>
#include <tools/logger/FileLoggingPolicy.hpp>

namespace runner {

using namespace algo::ts;

void TsContext::runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath)
{
    tspInstanceCfg = tspInstanceLoader.loadTspInstance(runCfg.tspInstanceCfgPath);
    auto& tspCfg = tspInstanceCfg;
    auto& generator = g;
    initFunction = [&tspCfg, &generator](){
        return TspSolutionTabuSearch::createRandom(tspCfg, generator);
    };
    tsCfg = dynamic_cast<const configuration::TsCfg&>(*runCfg.algoCfg);
    for(auto i = 0u; i < runCfg.repetitionsNum; i++)
    {
        makeLoggers(runCfg, runCfgPath);
        runSingleProblemRepetition();
    }
}

void TsContext::runSingleProblemRepetition()
{
    auto tabuSearchAlgo = makeAlgo();
    auto start = std::chrono::steady_clock::now();
    SolutionPtr bestEver = tabuSearchAlgo->run();
    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    Statistics statistics = tabuSearchAlgo->getStatistics();
    additionalLogger->log("Elapsed=%.3f seconds, best ever: %s, tabu guarded=%u, tabu passed=%u, aspiracy passed=%u",
                          elapsed.count() / 1000.0, bestEver->toString().c_str(),
                          statistics.tabuGuardedNums, statistics.tabuNotGuardedNums, statistics.aspirationNum);
}

std::unique_ptr<TabuSearch> TsContext::makeAlgo()
{
    // std::unique_ptr<TabuAbstractFactory> tabuFactory = std::make_unique<InversionTabuFactory>();
    tabuFactory = std::make_unique<SwapTabuFactory>();
    // std::unique_ptr<TabuAbstractFactory> tabuFactory = std::make_unique<SequenceTabuFactory>();
    return std::make_unique<TabuSearch>(tsCfg, initFunction, *tabuFactory, *mainLogger);
}

} // namesacpe runner
