#include "HybridGaTsContext2.hpp"
#include <memory>
#include <algo/ga/IndividualInterface.hpp>
#include <algo/hybridGa/HybridGaTsIndividual2.hpp>
#include <tools/logger/Logger.hpp>
#include <tools/logger/FileLoggingPolicy.hpp>
#include <algo/ts/SwapTabuFactory.hpp>

namespace runner {

using namespace algo::hybrid;

void HybridGaTsContext2::runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath)
{
    tspInstanceCfg = tspInstanceLoader.loadTspInstance(runCfg.tspInstanceCfgPath);
    auto& tspCfg = tspInstanceCfg;
    auto& generator = g;
    tabuFactory = std::make_unique<algo::ts::SwapTabuFactory>();
    initFunction = [&tspCfg, &generator](){
        return HybridGaTsIndividual2::createRandom(tspCfg, generator, algo::ga::CrossoverType::OX);
    };
    hybridGaTsCfg = dynamic_cast<const configuration::HybridGaTsCfg2&>(*runCfg.algoCfg);
    for(auto i = 0u; i < runCfg.repetitionsNum; i++)
    {
        makeLoggers(runCfg, runCfgPath);
        runSingleProblemRepetition();
    }
}

void HybridGaTsContext2::makeLoggers(const configuration::RunCfg &runCfg, const std::string &runCfgPath,
                                    const bool /*mtSafe*/, const bool /*skipLogging*/)
{
    AlgoContext::makeLoggers(runCfg, runCfgPath);

    std::unique_ptr<tools::LoggingPolicy> fileLoggingPolicy =
        std::make_unique<tools::FileLoggingPolicy>("results/" + getFileName(runCfgPath), tspInstanceCfg.name + "_ts",
                                                   (runCfg.repetitionsNum > 1));
    tsLogger = std::make_unique<tools::Logger>(std::move(fileLoggingPolicy));
}

void HybridGaTsContext2::runSingleProblemRepetition()
{
    auto hybridGaTsAlgo = makeAlgo();
    auto start = std::chrono::steady_clock::now();
    hybridGaTsAlgo->run();
    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    algo::ga::IndividualPtr bestEver = hybridGaTsAlgo->getBestEverIndividual();
    additionalLogger->log("Elapsed=%.3f seconds, best ever: %s", elapsed.count() / 1000.0, bestEver->toString().c_str());
}

std::unique_ptr<HybridGaTs2> HybridGaTsContext2::makeAlgo()
{
    std::unique_ptr<algo::ga::SelectionStrategy> selectionStrategy = selectionStrategyFactory.make(hybridGaTsCfg.gaCfg);
    TsSolutionToIndividualConverter tsSolutionIndividualConverter =
        [](algo::ts::SolutionPtr&& optimizedSolution) {
            return std::make_unique<HybridGaTsIndividual2>(dynamic_cast<HybridGaTsIndividual2&>(*(optimizedSolution.release())));
        };
    return std::make_unique<HybridGaTs2>(hybridGaTsCfg, initFunction, std::move(selectionStrategy),
                                         *tabuFactory, tsSolutionIndividualConverter, *mainLogger, *tsLogger);
}

} // namespace runner
