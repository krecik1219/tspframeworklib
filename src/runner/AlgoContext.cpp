#include "AlgoContext.hpp"
#include <tools/logger/FileLoggingPolicy.hpp>

namespace runner {

AlgoContext::AlgoContext()
    : tspInstanceLoader{}
    , rd{}
    , g{rd()}
{
}

AlgoContext::~AlgoContext() = default;

std::string AlgoContext::getFileName(const std::string& fullPath) const
{
    auto slashIndex = fullPath.rfind("/");
    auto dotIndex = fullPath.rfind(".");
    return fullPath.substr(slashIndex + 1, dotIndex - slashIndex - 1);
}

void AlgoContext::makeLoggers(const configuration::RunCfg &runCfg, const std::string &runCfgPath,
                              const bool mtSafe, const bool skipLogging)
{
    std::unique_ptr<tools::LoggingPolicy> fileLoggingPolicy =
        std::make_unique<tools::FileLoggingPolicy>("results/" + getFileName(runCfgPath), tspInstanceCfg.name,
                                                   (runCfg.repetitionsNum > 1));
    mainLogger = std::make_unique<tools::Logger>(std::move(fileLoggingPolicy), mtSafe, skipLogging);
    std::unique_ptr<tools::LoggingPolicy> fileLoggingPolicyAdditional =
        std::make_unique<tools::FileLoggingPolicy>("results/" + getFileName(runCfgPath), tspInstanceCfg.name + "_extra",
                                                   (runCfg.repetitionsNum > 1));
    additionalLogger = std::make_unique<tools::Logger>(std::move(fileLoggingPolicyAdditional));
}

} // namespace runner
