#pragma once

#include "AlgoContext.hpp"
#include <algo/ga/selection/SelectionStrategyFactory.hpp>
#include <algo/hybridGa/HybridGaTs.hpp>
#include <configuration/HybridGaTsCfg.hpp>
#include <algo/ts/TabuAbstractFactory.hpp>

namespace runner {

class HybridGaTsContext : public AlgoContext
{
public:
    void runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath) override;

private:
    void runSingleProblemRepetition();
    std::unique_ptr<algo::hybrid::HybridGaTs> makeAlgo();

    algo::ga::SelectionStrategyFactory selectionStrategyFactory;
    std::unique_ptr<algo::ts::TabuAbstractFactory> tabuFactory;
    algo::ga::InitFunction initFunction;
    configuration::HybridGaTsCfg hybridGaTsCfg;
};

} // namesapce runner
