#include "GreedyContext.hpp"
#include <tools/logger/FileLoggingPolicy.hpp>
#include <algo/sa/TspSolutionSa.hpp>

namespace runner {

using namespace algo::naive;

void GreedyContext::runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath)
{
    tspInstanceCfg = tspInstanceLoader.loadTspInstance(runCfg.tspInstanceCfgPath);
    greedyCfg = dynamic_cast<const configuration::GreedyCfg&>(*runCfg.algoCfg);
    for(auto i = 0u; i < runCfg.repetitionsNum; i++)
    {
        makeLoggers(runCfg, runCfgPath);
        runSingleProblemRepetition();
    }
}

void GreedyContext::runSingleProblemRepetition()
{
    auto greedyAlgo = makeAlgo();
    auto start = std::chrono::steady_clock::now();
    auto bestEver = greedyAlgo->getGreedyBest();
    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    auto stats = greedyAlgo->getLastRunStatistics();
    mainLogger->log("iters num=%u, best fitness=%.3f, avg fitness=%.3f, worst fitness=%.3f, fitness stdev=%.3f",
                    stats.iterationsNum, stats.bestFitness, stats.avgFitness, stats.worstFitness, stats.stdFitness);
    additionalLogger->log("Elapsed=%.3f seconds, best ever: %s", elapsed.count() / 1000.0, bestEver->toString().c_str());
}

std::unique_ptr<GreedyAlgo> GreedyContext::makeAlgo()
{
    return std::make_unique<GreedyAlgo>(tspInstanceCfg);
}

} // namesacpe runner
