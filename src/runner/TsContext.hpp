#pragma once

#include <memory>
#include "AlgoContext.hpp"
#include <algo/ts/TabuSearch.hpp>
#include <algo/ts/TabuAbstractFactory.hpp>
#include <configuration/TsCfg.hpp>

namespace runner {

class TsContext : public AlgoContext
{
public:
    virtual void runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath) override;

private:
    void runSingleProblemRepetition();
    std::unique_ptr<algo::ts::TabuSearch> makeAlgo();

    algo::ts::InitFunction initFunction;
    configuration::TsCfg tsCfg;
    std::unique_ptr<algo::ts::TabuAbstractFactory> tabuFactory;
};

} // namepsace runner
