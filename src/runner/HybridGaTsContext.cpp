#include "HybridGaTsContext.hpp"
#include <memory>
#include <algo/ga/IndividualInterface.hpp>
#include <algo/hybridGa/HybridGaTsIndividual.hpp>
#include <tools/logger/Logger.hpp>
#include <tools/logger/FileLoggingPolicy.hpp>
#include <algo/ts/SwapTabuFactory.hpp>

namespace runner {

using namespace algo::hybrid;

void HybridGaTsContext::runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath)
{
    tspInstanceCfg = tspInstanceLoader.loadTspInstance(runCfg.tspInstanceCfgPath);
    hybridGaTsCfg = dynamic_cast<const configuration::HybridGaTsCfg&>(*runCfg.algoCfg);
    tabuFactory = std::make_unique<algo::ts::SwapTabuFactory>();
    auto& tspCfg = tspInstanceCfg;
    auto& generator = g;
    auto& memoryDepth = hybridGaTsCfg.tabuMemoryDepth;
    auto& tabuFactoryRef = *tabuFactory;
    initFunction = [&tspCfg, &memoryDepth, &tabuFactoryRef, &generator](){
        return HybridGaTsIndividual::createRandom(tspCfg, memoryDepth, tabuFactoryRef, generator,
                                                  algo::ga::CrossoverType::OX);
    };
    for(auto i = 0u; i < runCfg.repetitionsNum; i++)
    {
        makeLoggers(runCfg, runCfgPath);
        runSingleProblemRepetition();
    }
}

void HybridGaTsContext::runSingleProblemRepetition()
{
    auto hybridGaTsAlgo = makeAlgo();
    auto start = std::chrono::steady_clock::now();
    hybridGaTsAlgo->run();
    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    algo::ga::IndividualPtr bestEver = hybridGaTsAlgo->getBestEverIndividual();
    auto tabuStatistics = hybridGaTsAlgo->getTabuStatistics();
    auto uniqueTabus = hybridGaTsAlgo->getNumberOfUniqueTabuMemories();
    additionalLogger->log("Elapsed=%.3f seconds, tabu violations=%u, tabu passed=%u, "
                          "aspiracy num=%u, unique tabus=%u, best ever: %s",
                          elapsed.count() / 1000.0,
                          tabuStatistics.tabuViolationNum,
                          tabuStatistics.tabuPassNum,
                          tabuStatistics.aspiracyPassNum,
                          uniqueTabus,
                          bestEver->toString().c_str());
}

std::unique_ptr<HybridGaTs> HybridGaTsContext::makeAlgo()
{
    std::unique_ptr<algo::ga::SelectionStrategy> selectionStrategy = selectionStrategyFactory.make(hybridGaTsCfg.gaCfg);
    return std::make_unique<HybridGaTs>(hybridGaTsCfg.gaCfg, initFunction, std::move(selectionStrategy), *mainLogger);
}

} // namespace runner
