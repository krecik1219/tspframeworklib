#pragma once

#include <memory>
#include "AlgoContext.hpp"
#include <algo/sa/SimulatedAnnealing.hpp>
#include <configuration/SaCfg.hpp>

namespace runner {

class SaContext : public AlgoContext
{
public:
    virtual void runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath) override;

private:
    void runSingleProblemRepetition();
    std::unique_ptr<algo::sa::SimulatedAnnealing> makeAlgo();

    algo::sa::InitFunction initFunction;
    configuration::SaCfg saCfg;
};

} // namepsace runner
