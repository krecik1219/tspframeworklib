#include "ProblemSolverRunner.hpp"
#include <algo/AlgoType.hpp>
#include "GaContext.hpp"
#include "HybridGaSaContext.hpp"
#include "TsContext.hpp"
#include "SaContext.hpp"
#include "GreedyContext.hpp"
#include "RandomContext.hpp"
#include "HybridGaTsContext.hpp"
#include "HybridGaTsContext2.hpp"

namespace runner {

ProblemSolverRunner::ProblemSolverRunner(const std::string& masterCfgPath)
    : masterCfgLoader{}
    , masterCfg{masterCfgLoader.loadMasterCfg(masterCfgPath)}
    , runCfgLoader{}
{
}

ProblemSolverRunner::~ProblemSolverRunner() = default;

void ProblemSolverRunner::solveProblems()
{
    for(const auto& runCfgPath : masterCfg.runConfigsPaths)
    {
        configuration::RunCfg runCfg = runCfgLoader.loadRunCfg(runCfgPath);
        auto algoContext = makeAlgoContext(runCfg);
        algoContext->runSingleRunCfg(runCfg, runCfgPath);
    }
}

std::unique_ptr<AlgoContext> ProblemSolverRunner::makeAlgoContext(const configuration::RunCfg& runCfg)
{
    switch(runCfg.algoCfg->algoType)
    {
        case algo::AlgoType::GA:
            return std::make_unique<GaContext>();
        case algo::AlgoType::TS:
            return std::make_unique<TsContext>();
        case algo::AlgoType::SA:
            return std::make_unique<SaContext>();
        case algo::AlgoType::HYBRID_GA_SA:
            return std::make_unique<HybridGaSaContext>();
        case algo::AlgoType::HYBRID_GA_TS:
            return std::make_unique<HybridGaTsContext>();
        case algo::AlgoType::HYBRID_GA_TS_2:
            return std::make_unique<HybridGaTsContext2>();
        case algo::AlgoType::GREEDY:
            return std::make_unique<GreedyContext>();
        case algo::AlgoType::RANDOM:
            return std::make_unique<RandomContext>();
        default:
            throw std::runtime_error("Unknown algo type: "
                                     + std::to_string(static_cast<uint32_t>(runCfg.algoCfg->algoType)));
    }
}

} // namespace runner
