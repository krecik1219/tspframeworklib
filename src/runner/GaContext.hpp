#pragma once

#include "AlgoContext.hpp"
#include <algo/ga/selection/SelectionStrategyFactory.hpp>
#include <algo/ga/GeneticAlgorithm.hpp>

namespace runner {

class GaContext : public AlgoContext
{
public:
    void runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath) override;

private:
    void runSingleProblemRepetition();
    std::unique_ptr<algo::ga::GeneticAlgorithm> makeAlgo();

    algo::ga::SelectionStrategyFactory selectionStrategyFactory;
    algo::ga::InitFunction initFunction;
    configuration::GaCfg gaCfg;
};

} // namesapce runner
