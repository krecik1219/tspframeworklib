#include "SaContext.hpp"
#include <tools/logger/FileLoggingPolicy.hpp>
#include <algo/sa/TspSolutionSa.hpp>

namespace runner {

using namespace algo::sa;

void SaContext::runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath)
{
    tspInstanceCfg = tspInstanceLoader.loadTspInstance(runCfg.tspInstanceCfgPath);
    auto& tspCfg = tspInstanceCfg;
    auto& generator = g;
    initFunction = [&tspCfg, &generator](){
        return TspSolutionSa::createRandom(tspCfg, generator);
    };
    saCfg = dynamic_cast<const configuration::SaCfg&>(*runCfg.algoCfg);
    for(auto i = 0u; i < runCfg.repetitionsNum; i++)
    {
        makeLoggers(runCfg, runCfgPath, false, true);
        runSingleProblemRepetition();
    }
}

void SaContext::runSingleProblemRepetition()
{
    auto simulatedAnnealing = makeAlgo();
    const auto start = std::chrono::steady_clock::now();
    SolutionPtr bestEver = simulatedAnnealing->run();
    const auto end = std::chrono::steady_clock::now();
    const uint32_t iterationsNum = simulatedAnnealing->getIterationsNum();
    const auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    additionalLogger->log("Elapsed=%.3f seconds, iterations num=%u, best ever: %s",
                          elapsed.count() / 1000.0, iterationsNum, bestEver->toString().c_str());
}

std::unique_ptr<SimulatedAnnealing> SaContext::makeAlgo()
{
    return std::make_unique<SimulatedAnnealing>(saCfg, initFunction, *mainLogger);
}

} // namesacpe runner
