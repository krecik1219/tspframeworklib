#include "RandomContext.hpp"
#include <tools/logger/FileLoggingPolicy.hpp>
#include <algo/sa/TspSolutionSa.hpp>

namespace runner {

using namespace algo::naive;

void RandomContext::runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath)
{
    tspInstanceCfg = tspInstanceLoader.loadTspInstance(runCfg.tspInstanceCfgPath);
    randomCfg = dynamic_cast<const configuration::RandomCfg&>(*runCfg.algoCfg);
    for(auto i = 0u; i < runCfg.repetitionsNum; i++)
    {
        makeLoggers(runCfg, runCfgPath);
        runSingleProblemRepetition();
    }
}

void RandomContext::runSingleProblemRepetition()
{
    auto randomSearch = makeAlgo();
    auto start = std::chrono::steady_clock::now();
    auto bestEver = randomSearch->getRandomSolution(g);
    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    auto stats = randomSearch->getLastRunStatistics();
    additionalLogger->log("Elapsed=%.3f seconds, iters num=%u, best fitness=%.3f, avg fitness=%.3f, "
                          "worst fitness=%.3f, fitness stdev=%.3f, best ever: %s",
                          elapsed.count() / 1000.0, stats.iterationsNum, stats.bestFitness,
                          stats.avgFitness, stats.worstFitness, stats.stdFitness,
                          bestEver->toString().c_str());
}

std::unique_ptr<RandomSearch> RandomContext::makeAlgo()
{
    return std::make_unique<RandomSearch>(randomCfg, tspInstanceCfg, *mainLogger);
}

} // namesacpe runner
