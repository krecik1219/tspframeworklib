#pragma once

#include <memory>
#include "AlgoContext.hpp"
#include <algo/naive/RandomSearch.hpp>
#include <configuration/RandomCfg.hpp>

namespace runner {

class RandomContext : public AlgoContext
{
public:
    virtual void runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath) override;

private:
    void runSingleProblemRepetition();
    std::unique_ptr<algo::naive::RandomSearch> makeAlgo();

    configuration::RandomCfg randomCfg;
};

} // namepsace runner
