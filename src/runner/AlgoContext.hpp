#pragma once

#include <configuration/loader/MasterCfgLoader.hpp>
#include <configuration/loader/RunCfgLoader.hpp>
#include <configuration/loader/TspInstanceLoader.hpp>
#include <random>
#include <string>
#include <tools/logger/Logger.hpp>
#include <configuration/RunCfg.hpp>

namespace runner {

class AlgoContext
{
public:
    AlgoContext();
    virtual ~AlgoContext();

    AlgoContext(const AlgoContext&) = delete;
    AlgoContext& operator=(const AlgoContext&) = delete;
    AlgoContext(AlgoContext&&) = delete;
    AlgoContext& operator=(AlgoContext&&) = delete;

    virtual void runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath) = 0;

protected:
    std::string getFileName(const std::string& fullPath) const;
    virtual void makeLoggers(const configuration::RunCfg& runCfg, const std::string& runCfgPath,
                             const bool mtSafe = false, const bool skipLogging = false);

    configuration::loader::TspInstanceLoader tspInstanceLoader;
    configuration::tsp::TspInstance tspInstanceCfg;

    std::unique_ptr<tools::Logger> mainLogger;
    std::unique_ptr<tools::Logger> additionalLogger;

    std::random_device rd;
    std::mt19937 g;
};

} // namesapce runner
