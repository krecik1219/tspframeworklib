#pragma once

#include "AlgoContext.hpp"
#include <algo/ga/selection/SelectionStrategyFactory.hpp>
#include <algo/hybridGa/HybridGaSa.hpp>

namespace runner {

class HybridGaSaContext : public AlgoContext
{
public:
    void runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath) override;

protected:
    virtual void makeLoggers(const configuration::RunCfg& runCfg, const std::string& runCfgPath,
                             const bool mtSafe = false, const bool skipLogging = false) override;

private:
    void runSingleProblemRepetition();
    std::unique_ptr<algo::hybrid::HybridGaSa> makeAlgo();

    algo::ga::SelectionStrategyFactory selectionStrategyFactory;
    algo::ga::InitFunction initFunction;
    configuration::HybridGaSaCfg hybridGaSaCfg;

    std::unique_ptr<tools::Logger> saLogger;
};

} // namesapce runner
