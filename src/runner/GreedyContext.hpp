#pragma once

#include <memory>
#include "AlgoContext.hpp"
#include <algo/naive/GreedyAlgo.hpp>
#include <configuration/GreedyCfg.hpp>

namespace runner {

class GreedyContext : public AlgoContext
{
public:
    virtual void runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath) override;

private:
    void runSingleProblemRepetition();
    std::unique_ptr<algo::naive::GreedyAlgo> makeAlgo();

    configuration::GreedyCfg greedyCfg;
};

} // namepsace runner
