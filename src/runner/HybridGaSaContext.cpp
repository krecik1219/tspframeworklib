#include "HybridGaSaContext.hpp"
#include <memory>
#include <algo/ga/IndividualInterface.hpp>
#include <algo/hybridGa/HybridGaSaIndividual.hpp>
#include <tools/logger/Logger.hpp>
#include <tools/logger/FileLoggingPolicy.hpp>
#include <algo/TwoOpt.hpp>
#include <algo/ThreeOpt.hpp>

namespace runner {

using namespace algo::hybrid;

void HybridGaSaContext::runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath)
{
    tspInstanceCfg = tspInstanceLoader.loadTspInstance(runCfg.tspInstanceCfgPath);
    auto& tspCfg = tspInstanceCfg;
    auto& generator = g;
    initFunction = [&tspCfg, &generator](){
        return HybridGaSaIndividual::createRandom(tspCfg, generator, algo::ga::CrossoverType::NRX);
    };
    hybridGaSaCfg = dynamic_cast<const configuration::HybridGaSaCfg&>(*runCfg.algoCfg);
    for(auto i = 0u; i < runCfg.repetitionsNum; i++)
    {
        makeLoggers(runCfg, runCfgPath);
        runSingleProblemRepetition();
    }
}

void HybridGaSaContext::makeLoggers(const configuration::RunCfg &runCfg, const std::string &runCfgPath,
                                    const bool /*mtSafe*/, const bool /*skipLogging*/)
{
    AlgoContext::makeLoggers(runCfg, runCfgPath);

    std::unique_ptr<tools::LoggingPolicy> fileLoggingPolicy =
        std::make_unique<tools::FileLoggingPolicy>("results/" + getFileName(runCfgPath), tspInstanceCfg.name + "_sa",
                                                   (runCfg.repetitionsNum > 1));
    saLogger = std::make_unique<tools::Logger>(std::move(fileLoggingPolicy));
}

void HybridGaSaContext::runSingleProblemRepetition()
{
    auto hybridGaSaAlgo = makeAlgo();
    auto start = std::chrono::steady_clock::now();
    hybridGaSaAlgo->run();
    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    algo::ga::IndividualPtr bestEver = hybridGaSaAlgo->getBestEverIndividual();
    additionalLogger->log("Elapsed=%.3f seconds, best ever: %s", elapsed.count() / 1000.0, bestEver->toString().c_str());
}

std::unique_ptr<HybridGaSa> HybridGaSaContext::makeAlgo()
{
    std::unique_ptr<algo::ga::SelectionStrategy> selectionStrategy = selectionStrategyFactory.make(hybridGaSaCfg.gaCfg);
    SaSolutionToIndividualConverter saSolutionIndividualConverter =
        [](algo::sa::SolutionPtr&& optimizedSolution) {
            return std::make_unique<HybridGaSaIndividual>(dynamic_cast<HybridGaSaIndividual&>(*(optimizedSolution.release())));
        };
    auto& tspCfg = tspInstanceCfg;
    TwoOptOptimizer twoOptOptimizer = [&tspCfg](const algo::ga::IndividualInterface& individual){
        const auto& tspSolution = dynamic_cast<const tsp::TspSolution&>(individual);
        algo::TwoOpt twoOpt{};
        auto optimizedSolution = twoOpt.run2Opt(tspSolution);
        auto citySeq = optimizedSolution.getCitySequence();
        return std::make_unique<HybridGaSaIndividual>(tspCfg, std::move(citySeq), algo::ga::CrossoverType::NRX);
    };
    ThreeOptOptimizer threeOptOptimizer = [&tspCfg](const algo::ga::IndividualInterface& individual){
        const auto& tspSolution = dynamic_cast<const tsp::TspSolution&>(individual);
        algo::ThreeOpt threeOpt{};
        auto optimizedSolution = threeOpt.run3Opt(tspSolution);
        auto citySeq = optimizedSolution.getCitySequence();
        return std::make_unique<HybridGaSaIndividual>(tspCfg, std::move(citySeq), algo::ga::CrossoverType::NRX);
    };

    return std::make_unique<HybridGaSa>(hybridGaSaCfg, initFunction,
                                        saSolutionIndividualConverter, twoOptOptimizer, threeOptOptimizer,
                                        std::move(selectionStrategy), *mainLogger, *saLogger);
}

} // namespace runner
