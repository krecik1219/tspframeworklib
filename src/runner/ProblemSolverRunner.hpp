#pragma once

#include <string>
#include <configuration/loader/MasterCfgLoader.hpp>
#include <configuration/loader/RunCfgLoader.hpp>
#include <configuration/loader/TspInstanceLoader.hpp>
#include <configuration/MasterCfg.hpp>
#include <configuration/RunCfg.hpp>
#include "AlgoContext.hpp"

namespace runner {

class ProblemSolverRunner
{
public:
    explicit ProblemSolverRunner(const std::string& masterCfgPath);
    ~ProblemSolverRunner();

    ProblemSolverRunner(const ProblemSolverRunner&) = delete;
    ProblemSolverRunner& operator=(const ProblemSolverRunner&) = delete;
    ProblemSolverRunner(ProblemSolverRunner&&) = delete;
    ProblemSolverRunner& operator=(ProblemSolverRunner&&) = delete;

    void solveProblems();

private:
    std::unique_ptr<AlgoContext> makeAlgoContext(const configuration::RunCfg& runCfg);

    configuration::loader::MasterCfgLoader masterCfgLoader;
    configuration::MasterCfg masterCfg;
    configuration::loader::RunCfgLoader runCfgLoader;
};

} // namespace runner
