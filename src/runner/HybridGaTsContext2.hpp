#pragma once

#include "AlgoContext.hpp"
#include <algo/ga/selection/SelectionStrategyFactory.hpp>
#include <algo/hybridGa/HybridGaTs2.hpp>
#include <algo/ts/TabuAbstractFactory.hpp>

namespace runner {

class HybridGaTsContext2 : public AlgoContext
{
public:
    void runSingleRunCfg(const configuration::RunCfg& runCfg, const std::string& runCfgPath) override;

protected:
    virtual void makeLoggers(const configuration::RunCfg& runCfg, const std::string& runCfgPath,
                             const bool mtSafe = false, const bool skipLogging = false) override;

private:
    void runSingleProblemRepetition();
    std::unique_ptr<algo::hybrid::HybridGaTs2> makeAlgo();

    algo::ga::SelectionStrategyFactory selectionStrategyFactory;
    algo::ga::InitFunction initFunction;
    configuration::HybridGaTsCfg2 hybridGaTsCfg;
    std::unique_ptr<algo::ts::TabuAbstractFactory> tabuFactory;

    std::unique_ptr<tools::Logger> tsLogger;
};

} // namesapce runner
