set(SRC_CODE
    src/algo/AlgoType.hpp
    src/algo/TwoOpt.cpp
    src/algo/TwoOpt.hpp
    src/algo/ThreeOpt.cpp
    src/algo/ThreeOpt.hpp
    src/algo/ga/GeneticAlgorithm.cpp
    src/algo/ga/GeneticAlgorithm.hpp
    src/algo/ga/IndividualInterface.cpp
    src/algo/ga/IndividualInterface.hpp
    src/algo/ga/TspIndividual.cpp
    src/algo/ga/TspIndividual.hpp
    src/algo/ga/selection/SelectionStrategy.cpp
    src/algo/ga/selection/SelectionStrategy.hpp
    src/algo/ga/selection/TournamentSelectionStrategy.cpp
    src/algo/ga/selection/TournamentSelectionStrategy.hpp
    src/algo/ga/selection/RouletteSelectionStrategy.cpp
    src/algo/ga/selection/RouletteSelectionStrategy.hpp
    src/algo/ga/selection/SelectionStrategyFactory.cpp
    src/algo/ga/selection/SelectionStrategyFactory.hpp

    src/algo/hybridGa/HybridGaSa.cpp
    src/algo/hybridGa/HybridGaSa.hpp
    src/algo/hybridGa/HybridGaTs.cpp
    src/algo/hybridGa/HybridGaTs.hpp
    src/algo/hybridGa/HybridGaTs2.cpp
    src/algo/hybridGa/HybridGaTs2.hpp
    src/algo/hybridGa/TabuStatistics.hpp
    src/algo/hybridGa/HybridGaSaIndividual.cpp
    src/algo/hybridGa/HybridGaSaIndividual.hpp
    src/algo/hybridGa/HybridGaTsIndividual.cpp
    src/algo/hybridGa/HybridGaTsIndividual.hpp
    src/algo/hybridGa/HybridGaTsIndividual2.cpp
    src/algo/hybridGa/HybridGaTsIndividual2.hpp

    src/algo/sa/SaSolutionInterface.cpp
    src/algo/sa/SaSolutionInterface.hpp
    src/algo/sa/SimulatedAnnealing.cpp
    src/algo/sa/SimulatedAnnealing.hpp
    src/algo/sa/TspSolutionSa.cpp
    src/algo/sa/TspSolutionSa.hpp

    src/algo/ts/InversionTabu.cpp
    src/algo/ts/InversionTabu.hpp
    src/algo/ts/SequenceTabu.cpp
    src/algo/ts/SequenceTabu.hpp
    src/algo/ts/SwapTabu.cpp
    src/algo/ts/SwapTabu.hpp
    src/algo/ts/InversionTabuFactory.cpp
    src/algo/ts/InversionTabuFactory.hpp
    src/algo/ts/SequenceTabuFactory.cpp
    src/algo/ts/SequenceTabuFactory.hpp
    src/algo/ts/SwapTabuFactory.cpp
    src/algo/ts/SwapTabuFactory.hpp
    src/algo/ts/Tabu.cpp
    src/algo/ts/Tabu.hpp
    src/algo/ts/TabuAbstractFactory.cpp
    src/algo/ts/TabuAbstractFactory.hpp
    src/algo/ts/TabuMemory.hpp
    src/algo/ts/TabuMemory.cpp
    src/algo/ts/TabuSearch.cpp
    src/algo/ts/TabuSearch.hpp
    src/algo/ts/TsSolutionAbstractFactory.cpp
    src/algo/ts/TsSolutionAbstractFactory.hpp
    src/algo/ts/TsSolutionInterface.cpp
    src/algo/ts/TsSolutionInterface.hpp
    src/algo/ts/TspSolutionFactory.cpp
    src/algo/ts/TspSolutionFactory.hpp
    src/algo/ts/TspSolutionTabuSearch.cpp
    src/algo/ts/TspSolutionTabuSearch.hpp

    src/algo/naive/GreedyAlgo.cpp
    src/algo/naive/GreedyAlgo.hpp
    src/algo/naive/RandomSearch.cpp
    src/algo/naive/RandomSearch.hpp
    src/algo/naive/Statistics.cpp
    src/algo/naive/Statistics.hpp

    src/configuration/AlgoCfg.cpp
    src/configuration/AlgoCfg.hpp
    src/configuration/GaCfg.cpp
    src/configuration/GaCfg.hpp
    src/configuration/GreedyCfg.cpp
    src/configuration/GreedyCfg.hpp
    src/configuration/HybridGaSaCfg.cpp
    src/configuration/HybridGaSaCfg.hpp
    src/configuration/HybridGaTsCfg.cpp
    src/configuration/HybridGaTsCfg.hpp
    src/configuration/HybridGaTsCfg2.cpp
    src/configuration/HybridGaTsCfg2.hpp
    src/configuration/MasterCfg.hpp
    src/configuration/RandomCfg.cpp
    src/configuration/RandomCfg.hpp
    src/configuration/RunCfg.cpp
    src/configuration/RunCfg.hpp
    src/configuration/SaCfg.cpp
    src/configuration/SaCfg.hpp
    src/configuration/TsCfg.cpp
    src/configuration/TsCfg.hpp
    src/configuration/TspInstance.cpp
    src/configuration/TspInstance.hpp

    src/configuration/loader/AlgoSpecificLoader.cpp
    src/configuration/loader/AlgoSpecificLoader.hpp
    src/configuration/loader/GaCfgLoader.cpp
    src/configuration/loader/GaCfgLoader.hpp
    src/configuration/loader/GreedyCfgLoader.cpp
    src/configuration/loader/GreedyCfgLoader.hpp
    src/configuration/loader/HybridGaSaCfgLoader.cpp
    src/configuration/loader/HybridGaSaCfgLoader.hpp
    src/configuration/loader/HybridGaTsCfgLoader.cpp
    src/configuration/loader/HybridGaTsCfgLoader.hpp
    src/configuration/loader/HybridGaTsCfgLoader2.cpp
    src/configuration/loader/HybridGaTsCfgLoader2.hpp
    src/configuration/loader/LoaderException.cpp
    src/configuration/loader/LoaderException.hpp
    src/configuration/loader/LoaderUtilsFunctions.cpp
    src/configuration/loader/LoaderUtilsFunctions.hpp
    src/configuration/loader/MasterCfgLoader.cpp
    src/configuration/loader/MasterCfgLoader.hpp
    src/configuration/loader/RandomCfgLoader.cpp
    src/configuration/loader/RandomCfgLoader.hpp
    src/configuration/loader/RunCfgLoader.cpp
    src/configuration/loader/RunCfgLoader.hpp
    src/configuration/loader/SaCfgLoader.cpp
    src/configuration/loader/SaCfgLoader.hpp
    src/configuration/loader/TsCfgLoader.cpp
    src/configuration/loader/TsCfgLoader.hpp
    src/configuration/loader/TspInstanceLoader.cpp
    src/configuration/loader/TspInstanceLoader.hpp

    src/runner/AlgoContext.cpp
    src/runner/AlgoContext.hpp
    src/runner/GaContext.cpp
    src/runner/GaContext.hpp
    src/runner/GreedyContext.cpp
    src/runner/GreedyContext.hpp
    src/runner/HybridGaSaContext.cpp
    src/runner/HybridGaSaContext.hpp
    src/runner/HybridGaTsContext.cpp
    src/runner/HybridGaTsContext.hpp
    src/runner/HybridGaTsContext2.cpp
    src/runner/HybridGaTsContext2.hpp
    src/runner/ProblemSolverRunner.cpp
    src/runner/ProblemSolverRunner.hpp
    src/runner/RandomContext.cpp
    src/runner/RandomContext.hpp
    src/runner/SaContext.cpp
    src/runner/SaContext.hpp
    src/runner/TsContext.cpp
    src/runner/TsContext.hpp

    src/tools/logger/ConsoleLoggingPolicy.cpp
    src/tools/logger/ConsoleLoggingPolicy.hpp
    src/tools/logger/DumbLoggingPolicy.cpp
    src/tools/logger/DumbLoggingPolicy.hpp
    src/tools/logger/FileLoggingPolicy.cpp
    src/tools/logger/FileLoggingPolicy.hpp
    src/tools/logger/Logger.cpp
    src/tools/logger/Logger.hpp
    src/tools/logger/LoggerExtension.cpp
    src/tools/logger/LoggerExtension.hpp
    src/tools/logger/LoggingPolicy.cpp
    src/tools/logger/LoggingPolicy.hpp
    src/tools/logger/TimestampLoggerExtension.cpp
    src/tools/logger/TimestampLoggerExtension.hpp

    src/tsp/TspSolution.cpp
    src/tsp/TspSolution.hpp

    src/utils/HashUtils.hpp
    src/utils/MathUtils.cpp
    src/utils/MathUtils.hpp
    src/utils/RandomUtils.cpp
    src/utils/RandomUtils.hpp
    src/utils/StringUtils.cpp
    src/utils/StringUtils.hpp
)
