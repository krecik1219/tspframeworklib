#pragma once

#include <memory>
#include <vector>

namespace algo {
namespace sa {

class SaSolutionInterface;
using SolutionPtr = std::unique_ptr<SaSolutionInterface>;

class SaSolutionInterface
{
public:
    SaSolutionInterface();
    virtual ~SaSolutionInterface();

    SaSolutionInterface(const SaSolutionInterface&);
    SaSolutionInterface& operator=(const SaSolutionInterface&);
    SaSolutionInterface(SaSolutionInterface&&);
    SaSolutionInterface& operator=(SaSolutionInterface&&);

    virtual double cost() = 0;
    virtual SolutionPtr getNeighbour() = 0;
    virtual std::string toString() const = 0;
    virtual SolutionPtr cloneSaSolution() const = 0;
};

} // namespace sa
} // namespace algo
