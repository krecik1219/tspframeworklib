#include "SaSolutionInterface.hpp"

#include <limits>

namespace algo {
namespace sa {

SaSolutionInterface::SaSolutionInterface() = default;
SaSolutionInterface::~SaSolutionInterface() = default;
SaSolutionInterface::SaSolutionInterface(const SaSolutionInterface &) = default;
SaSolutionInterface &SaSolutionInterface::operator=(const SaSolutionInterface &) = default;
SaSolutionInterface::SaSolutionInterface(SaSolutionInterface &&) = default;
SaSolutionInterface &SaSolutionInterface::operator=(SaSolutionInterface &&) = default;

} // namespace sa
} // namespace algo
