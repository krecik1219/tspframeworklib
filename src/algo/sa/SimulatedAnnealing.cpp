#include "SimulatedAnnealing.hpp"
#include <numeric>
#include <utils/RandomUtils.hpp>
#include <cmath>

namespace algo {
namespace sa {

SimulatedAnnealing::SimulatedAnnealing(const configuration::SaCfg& saCfg, const InitFunction& initFunction, tools::Logger& logger)
    : saCfg{saCfg}
    , initFunction{initFunction}
    , currentTemp{saCfg.initTemp}
    , iterationNum{0u}
    , logger{logger}
{
}

SimulatedAnnealing::~SimulatedAnnealing() = default;

SolutionPtr SimulatedAnnealing::run()
{
    currentSolution = initFunction();
    bestSolution = currentSolution->cloneSaSolution();
    logState();
    auto& random = utils::rnd::Random::getInstance();
    while(!freezed())
    {
        for(auto i = 0u; i < saCfg.iterationsPerTemp; i++)
        {
            SolutionPtr neighbour = currentSolution->getNeighbour();
            if(neighbour->cost() < currentSolution->cost())
            {
                currentSolution = std::move(neighbour);
                if(currentSolution->cost() < bestSolution->cost())
                {
                    bestSolution = currentSolution->cloneSaSolution();
                }
            }
            else
            {
                const double rndPick = random.getRandomDouble(0.0, 1.0);
                if(rndPick < metropolisFunction(*neighbour))
                    currentSolution = std::move(neighbour);
            }
            iterationNum++;
            logState();
        }
        cooling();
    }
    return bestSolution->cloneSaSolution();
}

uint32_t SimulatedAnnealing::getIterationsNum() const
{
    return iterationNum;
}

bool SimulatedAnnealing::freezed() const
{
    return currentTemp <= minTemp;
}

double SimulatedAnnealing::metropolisFunction(SaSolutionInterface& neighbour)
{
    const double metropolisFunctionValue = std::exp((currentSolution->cost() - neighbour.cost()) / currentTemp);
    return metropolisFunctionValue;
}

void SimulatedAnnealing::cooling()
{
    currentTemp = currentTemp * saCfg.tempStepFactor;
}

void SimulatedAnnealing::logState()
{
    logger.log("%u, %.4f, %.4f", iterationNum, bestSolution->cost(), currentSolution->cost());
}

} // namespace ts
} // namesapce algo
