#pragma once

#include <algorithm>
#include <tsp/TspSolution.hpp>
#include <configuration/TspInstance.hpp>
#include "SaSolutionInterface.hpp"
#include <numeric>
#include <memory>

namespace algo {
namespace sa {

namespace cfgtsp = configuration::tsp;

class TspSolutionSa : public SaSolutionInterface, public tsp::TspSolution
{
public:
    TspSolutionSa(const cfgtsp::TspInstance& tspCfg, const uint32_t problemSize);
    TspSolutionSa(const cfgtsp::TspInstance& tspCfg, tsp::CitySequence&& citySequence);

    TspSolutionSa() = delete;

    virtual ~TspSolutionSa() override;
    TspSolutionSa(const TspSolutionSa&);
    TspSolutionSa& operator=(const TspSolutionSa&) = delete;
    TspSolutionSa(TspSolutionSa&&) = delete;
    TspSolutionSa& operator=(TspSolutionSa&&) = delete;

    virtual double cost() override;
    virtual SolutionPtr getNeighbour() override;
    virtual std::string toString() const override;
    virtual SolutionPtr cloneSaSolution() const override;

    template <class RandomGenerator>
    static std::unique_ptr<TspSolutionSa> createRandom(const cfgtsp::TspInstance& tspCfg,
                                                               RandomGenerator&& g);
private:
    void evaluate();
    SolutionPtr createNeighbourBySwap();
    SolutionPtr createNeighbourByInversion();

    double currentCost;
    bool isCurrentCostValid;
};

template<class RandomGenerator>
std::unique_ptr<TspSolutionSa> TspSolutionSa::createRandom(
    const configuration::tsp::TspInstance& tspCfg, RandomGenerator&& g)
{
    tsp::CitySequence citySequence(tspCfg.citiesData.size());
    std::iota(citySequence.begin(), citySequence.end(), 0u);
    std::shuffle(citySequence.begin(), citySequence.end(), g);
    return std::make_unique<TspSolutionSa>(tspCfg, std::move(citySequence));
}

} // namespace sa
} // namespace algo
