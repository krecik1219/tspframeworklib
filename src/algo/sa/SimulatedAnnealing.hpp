#pragma once

#include "SaSolutionInterface.hpp"
#include <configuration/SaCfg.hpp>
#include <memory>
#include <functional>
#include <tools/logger/Logger.hpp>

namespace algo {
namespace sa {

using InitFunction = std::function<SolutionPtr()>;

class SimulatedAnnealing
{
public:
    SimulatedAnnealing(const configuration::SaCfg& saCfg, const InitFunction& initFunction, tools::Logger& logger);

    ~SimulatedAnnealing();

    SimulatedAnnealing(const SimulatedAnnealing&) = delete;
    SimulatedAnnealing& operator=(const SimulatedAnnealing&) = delete;
    SimulatedAnnealing(SimulatedAnnealing&&) = delete;
    SimulatedAnnealing& operator=(SimulatedAnnealing&&) = delete;

    SolutionPtr run();
    uint32_t getIterationsNum() const;

private:
    bool freezed() const;
    double metropolisFunction(SaSolutionInterface& neighbour);
    void cooling();
    void logState();

    const configuration::SaCfg saCfg;
    const InitFunction initFunction;

    double currentTemp;
    uint32_t iterationNum;
    tools::Logger& logger;

    SolutionPtr currentSolution;
    SolutionPtr bestSolution;

    static constexpr double minTemp = 0.1;
};

} // namespace sa
} // namesapce algo
