#include "TspSolutionSa.hpp"
#include <utils/RandomUtils.hpp>
#include <cstdint>
#include <set>

namespace algo {
namespace sa {

TspSolutionSa::TspSolutionSa(const cfgtsp::TspInstance& tspCfg, const uint32_t problemSize)
    : SaSolutionInterface()
    , tsp::TspSolution(tspCfg, problemSize)
    , currentCost{std::numeric_limits<double>::infinity()}
    , isCurrentCostValid{false}
{
}

TspSolutionSa::TspSolutionSa(const cfgtsp::TspInstance& tspCfg, tsp::CitySequence&& citySequence)
    : SaSolutionInterface()
    , tsp::TspSolution(tspCfg, std::move(citySequence))
    , currentCost{std::numeric_limits<double>::infinity()}
    , isCurrentCostValid{false}
{
}

TspSolutionSa::~TspSolutionSa() = default;
TspSolutionSa::TspSolutionSa(const TspSolutionSa&) = default;

SolutionPtr TspSolutionSa::getNeighbour()
{
    // return createNeighbourBySwap();
    return createNeighbourByInversion();
}

SolutionPtr TspSolutionSa::createNeighbourBySwap()
{
    auto clone = std::make_unique<TspSolutionSa>(*this);
    clone->isCurrentCostValid = false;
    auto& random = utils::rnd::Random::getInstance();
    const uint32_t indexA = random.getRandomUint(0, clone->citySequence.size() - 1);
    const uint32_t indexB = random.getRandomUint(0, clone->citySequence.size() - 1);
    std::swap(clone->citySequence[indexA], clone->citySequence[indexB]);
    return clone;
}

SolutionPtr TspSolutionSa::createNeighbourByInversion()
{
    auto clone = std::make_unique<TspSolutionSa>(*this);
    clone->isCurrentCostValid = false;
    auto& random = utils::rnd::Random::getInstance();
    const int32_t indexA = random.getRandomInt(0, static_cast<int32_t>(clone->citySequence.size() - 1));
    const int32_t indexB = random.getRandomInt(0, static_cast<int32_t>(clone->citySequence.size() - 1));
    auto firstIt = std::next(clone->citySequence.begin(), std::min(indexA, indexB));
    auto secondIt = std::next(clone->citySequence.begin(), std::max(indexA, indexB) + 1);
    std::reverse(firstIt, secondIt);
    return clone;
}

double TspSolutionSa::cost()
{
    if(!isCurrentCostValid)
    {
        evaluate();
        isCurrentCostValid = true;
    }
    return currentCost;
}

void TspSolutionSa::evaluate()
{
    currentCost = computeTotalDistance();
}

std::string TspSolutionSa::toString() const
{
    std::string repr = "fitness=" + std::to_string(currentCost) + ", " + tsp::TspSolution::toString();
    return repr;
}

SolutionPtr TspSolutionSa::cloneSaSolution() const
{
    return std::make_unique<TspSolutionSa>(*this);
}

} // namespace sa
} // namespace algo
