#pragma once

#include "TsSolutionInterface.hpp"
#include "TsSolutionAbstractFactory.hpp"

namespace algo {
namespace ts {

class TspSolutionFactory : public TsSolutionAbstractFactory
{
public:
    virtual SolutionPtr make(const TsSolutionInterface& other) override;
};

} // namespace ga
} // namespace algo
