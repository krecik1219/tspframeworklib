#include "TabuAbstractFactory.hpp"

namespace algo {
namespace ts {

TabuAbstractFactory::TabuAbstractFactory() = default;
TabuAbstractFactory::~TabuAbstractFactory() = default;
TabuAbstractFactory::TabuAbstractFactory(const TabuAbstractFactory &) = default;
TabuAbstractFactory &TabuAbstractFactory::operator=(const TabuAbstractFactory &) = default;
TabuAbstractFactory::TabuAbstractFactory(TabuAbstractFactory &&) = default;
TabuAbstractFactory &TabuAbstractFactory::operator=(TabuAbstractFactory &&) = default;

} // namespace ts
} // namesapce algo
