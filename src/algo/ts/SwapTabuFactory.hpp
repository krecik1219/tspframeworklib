#pragma once

#include <memory>
#include "TabuAbstractFactory.hpp"
#include "Tabu.hpp"

namespace algo {
namespace ts {

class SwapTabuFactory : public TabuAbstractFactory
{
public:
    virtual TabuPtr make(CreationInfoPtr&& creationInfo) const override;
    virtual TabuPtr make(const CreationInfo& creationInfo) const override;
};

} // namespace ts
} // namesapce algo
