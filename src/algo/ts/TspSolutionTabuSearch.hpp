#pragma once

#include <algorithm>
#include <tsp/TspSolution.hpp>
#include <configuration/TspInstance.hpp>
#include "TsSolutionInterface.hpp"
#include <numeric>
#include <memory>

namespace algo {
namespace ts {

namespace cfgtsp = configuration::tsp;

class SwapCreationInfo : public CreationInfo
{
public:
    SwapCreationInfo(const uint32_t cityIdA, const uint32_t cityIdB);

    uint32_t cityIdA;
    uint32_t cityIdB;
};

class SequenceCreationInfo : public CreationInfo
{
public:
    SequenceCreationInfo(tsp::CitySequence&& citySequence);

    tsp::CitySequence citySequence;
};

class InversionCreationInfo : public CreationInfo
{
public:
    InversionCreationInfo(std::vector<uint32_t>&& inversionSequence);

    std::vector<uint32_t> inversionSequence;
};

class TspSolutionTabuSearch : public TsSolutionInterface, public tsp::TspSolution
{
public:
    TspSolutionTabuSearch(const cfgtsp::TspInstance& tspCfg, const uint32_t problemSize);
    TspSolutionTabuSearch(const cfgtsp::TspInstance& tspCfg, tsp::CitySequence&& citySequence);

    TspSolutionTabuSearch() = delete;

    virtual ~TspSolutionTabuSearch() override;
    TspSolutionTabuSearch(const TspSolutionTabuSearch&);
    TspSolutionTabuSearch& operator=(const TspSolutionTabuSearch&) = delete;
    TspSolutionTabuSearch(TspSolutionTabuSearch&&) = delete;
    TspSolutionTabuSearch& operator=(TspSolutionTabuSearch&&) = delete;

    virtual Neighbourhood getNeighbourhood(const uint32_t maxNeighbourhoodSize) override;
    virtual double cost() override;
    virtual std::string toString() const override;
    virtual SolutionPtr cloneTsSolution() const override;

    template <class RandomGenerator>
    static std::unique_ptr<TspSolutionTabuSearch> createRandom(const cfgtsp::TspInstance& tspCfg,
                                                               RandomGenerator&& g);

private:
    void evaluate();

    double currentCost;
    bool isCurrentCostValid;
};

template<class RandomGenerator>
std::unique_ptr<TspSolutionTabuSearch> TspSolutionTabuSearch::createRandom(
    const configuration::tsp::TspInstance& tspCfg, RandomGenerator&& g)
{
    tsp::CitySequence citySequence(tspCfg.citiesData.size());
    std::iota(citySequence.begin(), citySequence.end(), 0u);
    std::shuffle(citySequence.begin(), citySequence.end(), g);
    return std::make_unique<TspSolutionTabuSearch>(tspCfg, std::move(citySequence));
}

} // namespace ts
} // namespace algo
