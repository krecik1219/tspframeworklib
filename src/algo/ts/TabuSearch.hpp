#pragma once

#include "TsSolutionInterface.hpp"
#include <configuration/TsCfg.hpp>
#include "Tabu.hpp"
#include <memory>
#include "TabuAbstractFactory.hpp"
#include <functional>
#include "TabuMemory.hpp"
#include "TsSolutionAbstractFactory.hpp"
#include <tools/logger/Logger.hpp>

namespace algo {
namespace ts {

using InitFunction = std::function<SolutionPtr()>;

struct Statistics
{
    double avgNeighbour;
    double worstNeighbour;
    uint32_t tabuGuardedNums;
    uint32_t tabuNotGuardedNums;
    uint32_t aspirationNum;
};

class TabuSearch
{
public:
    TabuSearch(const configuration::TsCfg& tsCfg, const InitFunction& initFunction,
               const TabuAbstractFactory& tabuFactory,
               tools::Logger& logger);

    ~TabuSearch();

    TabuSearch(const TabuSearch&) = delete;
    TabuSearch& operator=(const TabuSearch&) = delete;
    TabuSearch(TabuSearch&&) = delete;
    TabuSearch& operator=(TabuSearch&&) = delete;

    SolutionPtr run();
    Statistics getStatistics() const;

private:
    bool stopCondition() const;
    bool isNeighbourAccessible(const Neighbour& neighbour);
    void calculateNeighbourhoodStatistics(const Neighbourhood& neighbourhood);
    void logState();

    const configuration::TsCfg tsCfg;
    const InitFunction initFunction;
    const TabuAbstractFactory& tabuFactory;

    TabuMemory tabuMem;
    uint32_t iterationNum;
    tools::Logger& logger;

    SolutionPtr currentSolution;
    SolutionPtr bestSolution;

    Statistics statistics;
};

} // namespace ts
} // namesapce algo
