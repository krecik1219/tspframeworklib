#include "SequenceTabu.hpp"
#include "TspSolutionTabuSearch.hpp"
#include <utils/HashUtils.hpp>

namespace algo {
namespace ts {

SequenceTabu::SequenceTabu(CreationInfoPtr&& creationInfo)
{
    auto&& sequenceCreationInfo = std::move(dynamic_cast<SequenceCreationInfo&>(*creationInfo));
    this->citySequence = std::move(sequenceCreationInfo.citySequence);
}

SequenceTabu::SequenceTabu(const CreationInfo& creationInfo)
{
    const auto& sequenceCreationInfo = dynamic_cast<const SequenceCreationInfo&>(creationInfo);
    this->citySequence = sequenceCreationInfo.citySequence;
}

size_t SequenceTabu::hash() const noexcept
{
    std::size_t seed = 0;
    utils::hashing::hash_range(seed, citySequence.begin(), citySequence.end());
    return seed;
}

bool SequenceTabu::operator==(const Tabu& other) const
{
    auto const& otherSequenceTabu = dynamic_cast<const SequenceTabu&>(other);
    return citySequence == otherSequenceTabu.citySequence;
}

TabuPtr SequenceTabu::clone() const
{
    return std::make_unique<SequenceTabu>(*this);
}

} // namespace ts
} // namesapce algo
