#include "SwapTabu.hpp"
#include "TspSolutionTabuSearch.hpp"
#include <utils/HashUtils.hpp>

namespace algo {
namespace ts {

SwapTabu::SwapTabu(CreationInfoPtr&& creationInfo)
{
    auto&& swapCreationInfo = std::move(dynamic_cast<SwapCreationInfo&>(*creationInfo));
    cityIdA = swapCreationInfo.cityIdA;
    cityIdB = swapCreationInfo.cityIdB;
}

SwapTabu::SwapTabu(const CreationInfo& creationInfo)
{
    const auto& swapCreationInfo = dynamic_cast<const SwapCreationInfo&>(creationInfo);
    cityIdA = swapCreationInfo.cityIdA;
    cityIdB = swapCreationInfo.cityIdB;
}

size_t SwapTabu::hash() const noexcept
{
    uint32_t first = std::min(cityIdA, cityIdB);
    uint32_t second = std::max(cityIdA, cityIdB);
    std::size_t seed = 0;
    utils::hashing::hash_combine(seed, first);
    utils::hashing::hash_combine(seed, second);
    return seed;
}

bool SwapTabu::operator==(const Tabu& other) const
{
    auto const& otherSwapTabu = dynamic_cast<const SwapTabu&>(other);
    if(cityIdA == otherSwapTabu.cityIdA && cityIdB == otherSwapTabu.cityIdB)
        return true;
    if(cityIdA == otherSwapTabu.cityIdB && cityIdB == otherSwapTabu.cityIdA)
        return true;
    return false;
}

TabuPtr SwapTabu::clone() const
{
    return std::make_unique<SwapTabu>(*this);
}

} // namespace ts
} // namesapce algo
