#include "TspSolutionTabuSearch.hpp"
#include <utils/RandomUtils.hpp>
#include <utils/HashUtils.hpp>
#include <cstdint>
#include <unordered_set>

namespace algo {
namespace ts {

SwapCreationInfo::SwapCreationInfo(const uint32_t cityIdA, const uint32_t cityIdB)
    : cityIdA{cityIdA}
    , cityIdB{cityIdB}
{
}

SequenceCreationInfo::SequenceCreationInfo(tsp::CitySequence&& citySequence)
    : citySequence(std::move(citySequence))
{
}

InversionCreationInfo::InversionCreationInfo(std::vector<uint32_t>&& inversionSequence)
    : inversionSequence(std::move(inversionSequence))
{
}

TspSolutionTabuSearch::TspSolutionTabuSearch(const cfgtsp::TspInstance& tspCfg, const uint32_t problemSize)
    : TsSolutionInterface()
    , tsp::TspSolution(tspCfg, problemSize)
    , currentCost{std::numeric_limits<double>::infinity()}
    , isCurrentCostValid{false}
{
}

TspSolutionTabuSearch::TspSolutionTabuSearch(const cfgtsp::TspInstance& tspCfg, tsp::CitySequence&& citySequence)
    : TsSolutionInterface()
    , tsp::TspSolution(tspCfg, std::move(citySequence))
    , currentCost{std::numeric_limits<double>::infinity()}
    , isCurrentCostValid{false}
{
}

TspSolutionTabuSearch::~TspSolutionTabuSearch() = default;
TspSolutionTabuSearch::TspSolutionTabuSearch(const TspSolutionTabuSearch&) = default;

Neighbourhood TspSolutionTabuSearch::getNeighbourhood(const uint32_t maxNeighbourhoodSize)
{
    const uint32_t problemNeighboirhoodSizeLimit = tspCfg.dimension * (tspCfg.dimension - 1) / 2;
    const uint32_t neighbourHoodSizeLimit = std::min(maxNeighbourhoodSize, problemNeighboirhoodSizeLimit);
    Neighbourhood neighbourhood;
    neighbourhood.reserve(neighbourHoodSizeLimit);

    struct SwapPair
    {
        uint32_t cityAIndex;
        uint32_t cityBIndex;

        bool operator<(const SwapPair& other) const
        {
            if(cityAIndex < other.cityAIndex)
                return true;
            if(cityAIndex == other.cityAIndex)
                return cityBIndex < other.cityBIndex;
            else
                return false;
        }

        bool operator==(const SwapPair& other) const
        {
            return cityAIndex == other.cityAIndex && cityBIndex == other.cityBIndex;
        }

        class Hasher
        {
        public:
            size_t operator()(const SwapPair& swapPair) const
            {
                size_t seed = 0;
                utils::hashing::hash_combine(seed, swapPair.cityAIndex);
                utils::hashing::hash_combine(seed, swapPair.cityBIndex);
                return seed;
            }
        };
    };

    std::unordered_set<SwapPair, SwapPair::Hasher> possiblePairs;
    possiblePairs.reserve(citySequence.size() * (citySequence.size() - 1));
    for(auto i = 0u; i < citySequence.size(); i++)
    {
        for(auto j = i + 1u; j < citySequence.size(); j++)
            possiblePairs.insert({i, j});
    }

    auto& random = utils::rnd::Random::getInstance();
//    for(auto i = 0u; i < neighbourHoodSizeLimit; i++)
//    {
//        auto clonedSolution = std::make_unique<TspSolutionTabuSearch>(*this);
//        clonedSolution->isCurrentCostValid = false;
//        int32_t rndIndex = random.getRandomInt(0, static_cast<int32_t>(possiblePairs.size() - 1));
//        auto& clonedSolutionCitySeq = clonedSolution->citySequence;
//        auto rndIt = std::next(possiblePairs.begin(), rndIndex);
//        const uint32_t cityAIndex = (*rndIt).cityAIndex;
//        const uint32_t cityBIndex = (*rndIt).cityBIndex;
//        const uint32_t cityAId = getCityIdAtIndex(cityAIndex);
//        const uint32_t cityBId = getCityIdAtIndex(cityBIndex);
//        std::swap(clonedSolutionCitySeq[cityAIndex], clonedSolutionCitySeq[cityBIndex]);
//        possiblePairs.erase(rndIt);
//        neighbourhood.push_back(Neighbour{std::move(clonedSolution),
//                                          std::make_unique<SwapCreationInfo>(cityAId, cityBId)});
////        auto citySequenceForCreationInfo = clonedSolution->citySequence;
////        neighbourhood.push_back(Neighbour{std::move(clonedSolution),
////                                          std::make_unique<SequenceCreationInfo>(std::move(citySequenceForCreationInfo))});
//    }

    for(auto i = 0u; i < neighbourHoodSizeLimit; i++)
    {
        auto clonedSolution = std::make_unique<TspSolutionTabuSearch>(*this);
        clonedSolution->isCurrentCostValid = false;
        int32_t rndIndex = random.getRandomInt(0, static_cast<int32_t>(possiblePairs.size() - 1));
        auto& clonedSolutionCitySeq = clonedSolution->citySequence;
        auto rndIt = std::next(possiblePairs.begin(), rndIndex);
        const uint32_t cityAIndex = (*rndIt).cityAIndex;
        const uint32_t cityBIndex = (*rndIt).cityBIndex;

        auto firstIt = std::next(clonedSolutionCitySeq.begin(), cityAIndex);
        auto secondIt = std::next(clonedSolutionCitySeq.begin(), cityBIndex + 1);

        std::reverse(firstIt, secondIt);
        std::vector<uint32_t> invertedCities(firstIt, secondIt);

        possiblePairs.erase(rndIt);
//        neighbourhood.push_back(Neighbour{std::move(clonedSolution),
//                                          std::make_unique<InversionCreationInfo>(std::move(invertedCities))});
        neighbourhood.push_back(Neighbour{std::move(clonedSolution),
                                          std::make_unique<SwapCreationInfo>(getCityIdAtIndex(cityAIndex),
                                                                             getCityIdAtIndex(cityBIndex))});
    }

    return neighbourhood;
}

double TspSolutionTabuSearch::cost()
{
    if(!isCurrentCostValid)
    {
        evaluate();
        isCurrentCostValid = true;
    }
    return currentCost;
}

void TspSolutionTabuSearch::evaluate()
{
    currentCost = computeTotalDistance();
}

std::string TspSolutionTabuSearch::toString() const
{
    std::string repr = "fitness=" + std::to_string(currentCost) + ", " + tsp::TspSolution::toString();
    return repr;
}

SolutionPtr TspSolutionTabuSearch::cloneTsSolution() const
{
    return std::make_unique<TspSolutionTabuSearch>(*this);
}

} // namespace ts
} // namespace algo
