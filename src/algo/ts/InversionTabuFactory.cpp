#include "InversionTabuFactory.hpp"
#include "InversionTabu.hpp"

namespace algo {
namespace ts {

TabuPtr InversionTabuFactory::make(CreationInfoPtr&& creationInfo) const
{
    return std::make_unique<InversionTabu>(std::move(creationInfo));
}

TabuPtr InversionTabuFactory::make(const CreationInfo& creationInfo) const
{
    return std::make_unique<InversionTabu>(std::move(creationInfo));
}

} // namespace ts
} // namesapce algo
