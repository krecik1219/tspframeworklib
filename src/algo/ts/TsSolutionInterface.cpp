#include "TsSolutionInterface.hpp"

#include <limits>

namespace algo {
namespace ts {

CreationInfo::CreationInfo() = default;
CreationInfo::~CreationInfo() = default;
CreationInfo::CreationInfo(const CreationInfo &) = default;
CreationInfo &CreationInfo::operator=(const CreationInfo &) = default;
CreationInfo::CreationInfo(CreationInfo &&) = default;
CreationInfo &CreationInfo::operator=(CreationInfo &&) = default;

TsSolutionInterface::TsSolutionInterface() = default;
TsSolutionInterface::~TsSolutionInterface() = default;
TsSolutionInterface::TsSolutionInterface(const TsSolutionInterface &) = default;
TsSolutionInterface &TsSolutionInterface::operator=(const TsSolutionInterface &) = default;
TsSolutionInterface::TsSolutionInterface(TsSolutionInterface &&) = default;
TsSolutionInterface &TsSolutionInterface::operator=(TsSolutionInterface &&) = default;

} // namespace ts
} // namespace algo
