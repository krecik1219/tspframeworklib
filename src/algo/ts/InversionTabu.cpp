#include "InversionTabu.hpp"
#include "TspSolutionTabuSearch.hpp"
#include <utils/HashUtils.hpp>

namespace algo {
namespace ts {

InversionTabu::InversionTabu(CreationInfoPtr&& creationInfo)
{
    auto&& inversionCreationInfo = std::move(dynamic_cast<InversionCreationInfo&>(*creationInfo));

    inversionSequence = std::move(inversionCreationInfo.inversionSequence);
}

InversionTabu::InversionTabu(const CreationInfo& creationInfo)
{
    const auto& inversionCreationInfo = dynamic_cast<const InversionCreationInfo&>(creationInfo);
    inversionSequence = inversionCreationInfo.inversionSequence;
}

size_t InversionTabu::hash() const noexcept
{
    std::size_t seed = 0;
    utils::hashing::hash_range(seed, inversionSequence.begin(), inversionSequence.end());
    return seed;
}

bool InversionTabu::operator==(const Tabu& other) const
{
    auto const& otherInversionTabu = dynamic_cast<const InversionTabu&>(other);
    return inversionSequence == otherInversionTabu.inversionSequence;
}

TabuPtr InversionTabu::clone() const
{
    return std::make_unique<InversionTabu>(*this);
}

} // namespace ts
} // namesapce algo
