#pragma once

#include "TsSolutionInterface.hpp"

namespace algo {
namespace ts {

class TsSolutionAbstractFactory
{
public:
    TsSolutionAbstractFactory();
    virtual ~TsSolutionAbstractFactory();

    TsSolutionAbstractFactory(const TsSolutionAbstractFactory&);
    TsSolutionAbstractFactory& operator=(const TsSolutionAbstractFactory&);
    TsSolutionAbstractFactory(TsSolutionAbstractFactory&&);
    TsSolutionAbstractFactory& operator=(TsSolutionAbstractFactory&&);

    virtual SolutionPtr make(const TsSolutionInterface& other) = 0;
};

} // namespace sa
} // namespace algo
