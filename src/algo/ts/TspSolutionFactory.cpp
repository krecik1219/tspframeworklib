#include "TspSolutionFactory.hpp"
#include "TspSolutionTabuSearch.hpp"

namespace algo {
namespace ts {

SolutionPtr TspSolutionFactory::make(const TsSolutionInterface& other)
{
    return std::make_unique<TspSolutionTabuSearch>(dynamic_cast<const TspSolutionTabuSearch&>(other));
}

} // namespace ga
} // namespace algo
