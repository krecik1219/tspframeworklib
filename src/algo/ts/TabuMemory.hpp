#pragma once

#include <cstdint>
#include <deque>
#include "TsSolutionInterface.hpp"
#include "TabuAbstractFactory.hpp"
#include "Tabu.hpp"
#include <unordered_set>

namespace algo {
namespace ts {

class TabuMemory
{
public:
    explicit TabuMemory(const uint32_t memoryDepth, const TabuAbstractFactory& tabuFactory);
    ~TabuMemory();

    TabuMemory(const TabuMemory&) = delete;
    TabuMemory& operator=(const TabuMemory&) = delete;
    TabuMemory(TabuMemory&&) = delete;
    TabuMemory& operator=(TabuMemory&&) = delete;

    void add(TabuPtr&& tabu);
    bool isTabuViolated(const CreationInfo& creationInfo) const;

private:
    const uint32_t memoryDepth;
    const TabuAbstractFactory& tabuFactory;

    std::deque<TabuPtr> baseContainer;
    std::unordered_set<TabuPtr, TabuHasher, TabuEquality> quickAccessContainer;
};

} // namespace ts
} // namesapce algo
