#include "TabuMemory.hpp"
#include <algorithm>

namespace algo {
namespace ts {

TabuMemory::TabuMemory(const uint32_t memoryDepth, const TabuAbstractFactory& tabuFactory)
    : memoryDepth{memoryDepth}
    , tabuFactory{tabuFactory}
    , quickAccessContainer{}
{
}

TabuMemory::~TabuMemory() = default;

void TabuMemory::add(TabuPtr&& tabu)
{
    if(quickAccessContainer.find(tabu) == quickAccessContainer.end())
    {
        if(baseContainer.size() == memoryDepth)
        {
            const auto& frontTabu = baseContainer.front();
            auto tabuInQuickAccessIt = quickAccessContainer.find(frontTabu);
            if(tabuInQuickAccessIt != quickAccessContainer.end())
            {
                quickAccessContainer.erase(tabuInQuickAccessIt);
            }
            baseContainer.pop_front();
        }
        baseContainer.push_back(tabu->clone());
        quickAccessContainer.insert(std::move(tabu));
    }
    else
    {
        auto findResultIt = std::find_if(baseContainer.begin(), baseContainer.end(),
                                         [&tabu](const auto& t)
                                         {
                                             return (*t) == (*tabu);
                                         });
        baseContainer.erase(findResultIt);
        baseContainer.push_back(std::move(tabu));
    }
}

bool TabuMemory::isTabuViolated(const CreationInfo& creationInfo) const
{
    TabuPtr tempTabu = tabuFactory.make(creationInfo);
    bool violated = quickAccessContainer.find(tempTabu) != quickAccessContainer.end();
    return violated;
}

} // namespace ts
} // namesapce algo
