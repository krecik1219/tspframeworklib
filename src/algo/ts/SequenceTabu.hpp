#pragma once

#include "Tabu.hpp"
#include <memory>
#include <vector>
#include "TsSolutionInterface.hpp"

namespace algo {
namespace ts {

class SequenceTabu : public Tabu
{
public:
    SequenceTabu(CreationInfoPtr&& creationInfo);
    SequenceTabu(const CreationInfo& creationInfo);

    virtual size_t hash() const noexcept override;
    virtual bool operator==(const Tabu& other) const override;
    virtual TabuPtr clone() const override;

    std::vector<uint32_t> citySequence;
};

} // namespace ts
} // namesapce algo
