#pragma once

#include <memory>
#include "Tabu.hpp"
#include "TsSolutionInterface.hpp"

namespace algo {
namespace ts {

class TabuAbstractFactory
{
public:
    TabuAbstractFactory();
    virtual ~TabuAbstractFactory();

    TabuAbstractFactory(const TabuAbstractFactory&);
    TabuAbstractFactory& operator=(const TabuAbstractFactory&);
    TabuAbstractFactory(TabuAbstractFactory&&);
    TabuAbstractFactory& operator=(TabuAbstractFactory&&);

    virtual TabuPtr make(CreationInfoPtr&& creationInfo) const = 0;
    virtual TabuPtr make(const CreationInfo& creationInfo) const = 0;
};

} // namespace ts
} // namesapce algo
