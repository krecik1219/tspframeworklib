#include "TabuSearch.hpp"
#include <numeric>

namespace algo {
namespace ts {

TabuSearch::TabuSearch(const configuration::TsCfg &tsCfg, const InitFunction &initFunction,
                       const TabuAbstractFactory& tabuFactory,
                       tools::Logger& logger)
    : tsCfg{tsCfg}
    , initFunction{initFunction}
    , tabuFactory{tabuFactory}
    , tabuMem{tsCfg.tabuMemoryDepth, tabuFactory}
    , iterationNum{0u}
    , logger{logger}
    , statistics{}
{
}

TabuSearch::~TabuSearch() = default;

SolutionPtr TabuSearch::run()
{
    currentSolution = initFunction();
    bestSolution = currentSolution->cloneTsSolution();
    logState();
    while(!stopCondition())
    {
        CreationInfoPtr creationInfo = nullptr;
        Neighbourhood neighbourhood = currentSolution->getNeighbourhood(tsCfg.maxNeighbourhoodSize);
        calculateNeighbourhoodStatistics(neighbourhood);
        SolutionPtr bestNeighbour = nullptr;
        for(auto& neighbour : neighbourhood)
        {
            if(isNeighbourAccessible(neighbour))
            {
                if(bestNeighbour == nullptr || neighbour.neighbourSolution->cost() < bestNeighbour->cost())
                {
                    bestNeighbour = std::move(neighbour.neighbourSolution);
                    creationInfo = std::move(neighbour.creationInfo);
                }
            }
        }
        if(bestNeighbour != nullptr)
        {
            currentSolution = std::move(bestNeighbour);
            if(currentSolution->cost() < bestSolution->cost())
                bestSolution = currentSolution->cloneTsSolution();
            tabuMem.add(tabuFactory.make(std::move(creationInfo)));
        }
        iterationNum++;
        logState();
    }
    return bestSolution->cloneTsSolution();
}

bool TabuSearch::stopCondition() const
{
    return iterationNum >= tsCfg.iterationsNum;
}

bool TabuSearch::isNeighbourAccessible(const Neighbour& neighbour)
{
    if(neighbour.neighbourSolution->cost() < bestSolution->cost())  // aspiracy
    {
        statistics.aspirationNum++;
        return true;
    }
    bool accessible = !tabuMem.isTabuViolated(*neighbour.creationInfo);
    if(!accessible)
        statistics.tabuGuardedNums++;
    else
        statistics.tabuNotGuardedNums++;
    return accessible;
}

void TabuSearch::calculateNeighbourhoodStatistics(const Neighbourhood& neighbourhood)
{
    auto maxSolutionIt = std::max_element(neighbourhood.cbegin(), neighbourhood.cend(),
                                          [](const auto& lhs, const auto& rhs)
                                          {
                                              return lhs.neighbourSolution->cost() < rhs.neighbourSolution->cost();
                                          });
    statistics.worstNeighbour = (*maxSolutionIt).neighbourSolution->cost();
    double fitnessSum = std::accumulate(neighbourhood.cbegin(), neighbourhood.cend(), 0.0,
                                        [](auto acc, const auto& solution)
                                        {
                                            return acc + solution.neighbourSolution->cost();
                                        });
    statistics.avgNeighbour = fitnessSum / neighbourhood.size();
}

void TabuSearch::logState()
{
    logger.log("%u, %.4f, %.4f, %.4f, %.4f", iterationNum, bestSolution->cost(), currentSolution->cost(),
               statistics.avgNeighbour, statistics.worstNeighbour);
}

Statistics TabuSearch::getStatistics() const
{
    return statistics;
}

} // namespace ts
} // namesapce algo
