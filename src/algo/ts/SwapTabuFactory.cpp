#include "SwapTabuFactory.hpp"
#include "SwapTabu.hpp"

namespace algo {
namespace ts {

TabuPtr SwapTabuFactory::make(CreationInfoPtr&& creationInfo) const
{
    return std::make_unique<SwapTabu>(std::move(creationInfo));
}

TabuPtr SwapTabuFactory::make(const CreationInfo& creationInfo) const
{
   return std::make_unique<SwapTabu>(std::move(creationInfo));
}

} // namespace ts
} // namesapce algo
