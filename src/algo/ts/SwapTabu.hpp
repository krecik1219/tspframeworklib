#pragma once

#include "Tabu.hpp"
#include <memory>
#include "TsSolutionInterface.hpp"

namespace algo {
namespace ts {

class SwapTabu : public Tabu
{
public:
    SwapTabu(CreationInfoPtr&& creationInfo);
    SwapTabu(const CreationInfo& creationInfo);

    virtual size_t hash() const noexcept override;
    virtual bool operator==(const Tabu& other) const override;
    virtual TabuPtr clone() const override;

    uint32_t cityIdA;
    uint32_t cityIdB;
};

} // namespace ts
} // namesapce algo
