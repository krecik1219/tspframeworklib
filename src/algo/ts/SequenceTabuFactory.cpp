#include "SequenceTabuFactory.hpp"
#include "SequenceTabu.hpp"

namespace algo {
namespace ts {

TabuPtr SequenceTabuFactory::make(CreationInfoPtr&& creationInfo) const
{
    return std::make_unique<SequenceTabu>(std::move(creationInfo));
}

TabuPtr SequenceTabuFactory::make(const CreationInfo& creationInfo) const
{
    return std::make_unique<SequenceTabu>(creationInfo);
}

} // namespace ts
} // namesapce algo
