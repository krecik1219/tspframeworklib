#include "Tabu.hpp"

namespace algo {
namespace ts {

size_t TabuHasher::operator()(const TabuPtr& tabu) const noexcept
{
    return tabu->hash();
}

bool TabuEquality::operator()(const TabuPtr &lhs, const TabuPtr &rhs) const
{
    return (*lhs) == (*rhs);
}

Tabu::Tabu() = default;
Tabu::~Tabu() = default;
Tabu::Tabu(const Tabu &) = default;
Tabu &Tabu::operator=(const Tabu &) = default;
Tabu::Tabu(Tabu &&) = default;
Tabu &Tabu::operator=(Tabu &&) = default;

} // namespace ts
} // namesapce algo
