#pragma once

#include <memory>
#include <vector>

namespace algo {
namespace ts {

class TsSolutionInterface;
using SolutionPtr = std::unique_ptr<TsSolutionInterface>;

class CreationInfo
{
public:
    CreationInfo();
    virtual ~CreationInfo();

    CreationInfo(const CreationInfo&);
    CreationInfo& operator=(const CreationInfo&);
    CreationInfo(CreationInfo&&);
    CreationInfo& operator=(CreationInfo&&);
};

using CreationInfoPtr = std::unique_ptr<CreationInfo>;

struct Neighbour
{
    SolutionPtr neighbourSolution;
    CreationInfoPtr creationInfo;
};

using Neighbourhood = std::vector<Neighbour>;

class TsSolutionInterface
{
public:
    TsSolutionInterface();
    virtual ~TsSolutionInterface();

    TsSolutionInterface(const TsSolutionInterface&);
    TsSolutionInterface& operator=(const TsSolutionInterface&);
    TsSolutionInterface(TsSolutionInterface&&);
    TsSolutionInterface& operator=(TsSolutionInterface&&);

    virtual double cost() = 0;

    virtual Neighbourhood getNeighbourhood(const uint32_t maxNeighbourhoodSize) = 0;
    virtual std::string toString() const = 0;
    virtual SolutionPtr cloneTsSolution() const = 0;
};

} // namespace ts
} // namespace algo
