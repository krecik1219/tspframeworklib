#include "TsSolutionAbstractFactory.hpp"

namespace algo {
namespace ts {

TsSolutionAbstractFactory::TsSolutionAbstractFactory() = default;
TsSolutionAbstractFactory::~TsSolutionAbstractFactory() = default;
TsSolutionAbstractFactory::TsSolutionAbstractFactory(const TsSolutionAbstractFactory &) = default;
TsSolutionAbstractFactory &TsSolutionAbstractFactory::operator=(const TsSolutionAbstractFactory &) = default;
TsSolutionAbstractFactory::TsSolutionAbstractFactory(TsSolutionAbstractFactory &&) = default;
TsSolutionAbstractFactory &TsSolutionAbstractFactory::operator=(TsSolutionAbstractFactory &&) = default;

} // namespace sa
} // namespace algo
