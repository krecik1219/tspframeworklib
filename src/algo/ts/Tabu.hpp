#pragma once

#include <memory>

namespace algo {
namespace ts {

class Tabu;
using TabuPtr = std::unique_ptr<Tabu>;

struct TabuHasher
{
    size_t operator()(const TabuPtr& tabu) const noexcept;
};

struct TabuEquality
{
    bool operator()(const TabuPtr& lhs, const TabuPtr& rhs) const;
};

class Tabu
{
public:
    Tabu();
    virtual ~Tabu();

    Tabu(const Tabu&);
    Tabu& operator=(const Tabu&);
    Tabu(Tabu&&);
    Tabu& operator=(Tabu&&);

    virtual size_t hash() const noexcept = 0;
    virtual bool operator==(const Tabu& other) const = 0;
    virtual TabuPtr clone() const = 0;
};

} // namespace ts
} // namesapce algo
