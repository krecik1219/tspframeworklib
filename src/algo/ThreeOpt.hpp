#pragma once
#include <cstdint>
#include <vector>
#include <tsp/TspSolution.hpp>

namespace algo {

class ThreeOpt
{
public:
    tsp::TspSolution run3Opt(const tsp::TspSolution& inputSolution);

private:
    double reverseTourIfBetter(const int32_t i, const int32_t j, const int32_t k,
                               tsp::CitySequence& solution) const;

    double distance(const uint32_t city1, const uint32_t city2) const;

    const configuration::tsp::TspInstance* tspCfg;
};

} // namespace algo
