#include "HybridGaTs2.hpp"
#include <algo/sa/SimulatedAnnealing.hpp>
#include <utils/RandomUtils.hpp>
#include <tools/logger/DumbLoggingPolicy.hpp>
#include <algorithm>
#include <algo/ts/TabuSearch.hpp>

namespace algo {
namespace hybrid {

HybridGaTs2::HybridGaTs2(const configuration::HybridGaTsCfg2& hybridGaTsCfg,
                         const ga::InitFunction& initFunction,
                         std::unique_ptr<ga::SelectionStrategy>&& selectionStrategy,
                         const ts::TabuAbstractFactory& tabuFactory,
                         const TsSolutionToIndividualConverter& solutionConverter,
                         tools::Logger& gaLogger, tools::Logger& tsLogger)
    : ga::GeneticAlgorithm{hybridGaTsCfg.gaCfg, initFunction, std::move(selectionStrategy), gaLogger}
    , hybridGaTsCfg{hybridGaTsCfg}
    , tabuFactory{tabuFactory}
    , solutionConverter{solutionConverter}
    , tsLogger{tsLogger}
    , dumbLoggerForTs{std::make_unique<tools::DumbLoggingPolicy>(), false, true}
    , numOfGenerationsWithoutImprovement{0u}
    , antiBounceTsOptimization{0u}
{
}

void HybridGaTs2::run()
{
    initialize();
    evaluate();
    updateStatisticValues();
    updateBestIndividualSoFar();
    logState();
    gaLoop();
    generationNum++;
    optimizePartOfPopulationWithTs();
    updateStatisticValues();
    updateBestIndividualSoFar();
    logState();
}

void HybridGaTs2::gaLoop()
{
    while(!stopConditionReached())
    {
        selection();
        evaluate();
        generationNum++;
        updateStatisticValues();
        updateBestIndividualSoFar();
        if(shouldRunTsOptimization())
        {
            optimizePartOfPopulationWithTs();
            updateStatisticValues();
            updateBestIndividualSoFar();
        }
        logState();
    }
}

bool HybridGaTs2::shouldRunTsOptimization()
{
    return numOfGenerationsWithoutImprovement > hybridGaTsCfg.maxGenerationsWithoutImprovement;
}

void HybridGaTs2::optimizePartOfPopulationWithTs()
{
    numOfGenerationsWithoutImprovement = 0u;
    const uint32_t numOfIndividualsToOptimzeWithTs =
        static_cast<uint32_t>(hybridGaTsCfg.percentOfPopulationToOptimizeWithTs * population.size());
    auto& random = utils::rnd::Random::getInstance();
    const auto start = std::chrono::steady_clock::now();
    for(auto i = 0u; i < numOfIndividualsToOptimzeWithTs; i++)
    {
        const uint32_t individualToOptimize = random.getRandomUint(0, population.size() - 1);
        auto& selectedIndividual =
            dynamic_cast<ts::TsSolutionInterface&>(*population[individualToOptimize]);
        auto tsInitFn = [&selectedIndividual](){
            return selectedIndividual.cloneTsSolution();
        };
        ts::TabuSearch tabuSearchAlgo{hybridGaTsCfg.tsCfg, tsInitFn, tabuFactory, dumbLoggerForTs};
        auto solution = tabuSearchAlgo.run();
        tsLogger.log("%u, before opt=%.3f, after opt=%.3f", i, selectedIndividual.cost(), solution->cost());
        if(solution->cost() < selectedIndividual.cost())
        {
            population[individualToOptimize] = solutionConverter(std::move(solution));
        }
    }
    const auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    tsLogger.log("TS optimization done, elapsed=%.3f seconds", elapsed.count() / 1000.0);
}

void HybridGaTs2::updateBestIndividualSoFar()
{
    auto compareFn = [](const auto& lhs, const auto& rhs)
    {
        return lhs->getCurrentFitness() < rhs->getCurrentFitness();
    };
    auto bestIndividualIt = std::min_element(population.cbegin(), population.cend(), compareFn);
    if(bestSoFar == nullptr)
    {
        bestSoFar = (*bestIndividualIt)->cloneIndividual();
        numOfGenerationsWithoutImprovement = 0;
    }
    else
    {
        const auto best = std::min(bestSoFar.get(), (*bestIndividualIt).get(), compareFn);
        if(best != bestSoFar.get())
        {
            bestSoFar = best->cloneIndividual();
            numOfGenerationsWithoutImprovement = 0;
        }
        else
        {
            numOfGenerationsWithoutImprovement++;
        }
    }
}

} // namespace hybrid
} // namespace algo
