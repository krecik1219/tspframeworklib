#pragma once

#include <memory>
#include <algo/ga/TspIndividual.hpp>
#include <algo/ga/IndividualInterface.hpp>
#include <algo/ts/TabuMemory.hpp>
#include "TabuStatistics.hpp"

namespace algo {
namespace hybrid {

namespace cfgtsp = configuration::tsp;

class HybridGaTsIndividual : public ga::TspIndividual
{
public:
    HybridGaTsIndividual(const cfgtsp::TspInstance& tspCfg, const uint32_t problemSize,
                         const ga::CrossoverType crossoverType, const uint32_t memoryDepth,
                         const ts::TabuAbstractFactory& tabuFactory);
    HybridGaTsIndividual(const cfgtsp::TspInstance& tspCfg, tsp::CitySequence&& citySequence,
                         const ga::CrossoverType crossoverType, const uint32_t memoryDepth,
                         const ts::TabuAbstractFactory& tabuFactory);

    virtual ga::IndividualPtr cloneIndividual() const override;
    virtual void mutation() override;

    virtual ga::IndividualPtr makeIndividual(tsp::CitySequence&& citySequence) const override;

    void setStatisticsPointer(TabuStatistics* const tabuStatistics);
    const std::shared_ptr<ts::TabuMemory>& getTabuMemRef() const;

    template <class RandomGenerator>
    static std::unique_ptr<HybridGaTsIndividual> createRandom(const cfgtsp::TspInstance& tspCfg,
                                                              const uint32_t memoryDepth,
                                                              const ts::TabuAbstractFactory& tabuFactory,
                                                              RandomGenerator&& rndGen,
                                                              const ga::CrossoverType crossoverType);

private:
    std::pair<uint32_t, uint32_t> mutationIrgibnnm();

    const uint32_t memoryDepth;
    const ts::TabuAbstractFactory& tabuFactory;
    std::shared_ptr<ts::TabuMemory> tabuMem;
    double bestFitnessInEvolutionHistory;
    TabuStatistics* tabuStatistics;
};

template<class RandomGenerator>
std::unique_ptr<HybridGaTsIndividual> HybridGaTsIndividual::createRandom(
    const cfgtsp::TspInstance& tspCfg,
    const uint32_t memoryDepth,
    const ts::TabuAbstractFactory& tabuFactory,
    RandomGenerator&& rndGen,
    const ga::CrossoverType crossoverType)
{
    tsp::CitySequence citySequence(tspCfg.citiesData.size());
    std::iota(citySequence.begin(), citySequence.end(), 0u);
    std::shuffle(citySequence.begin(), citySequence.end(), std::forward<RandomGenerator>(rndGen));
    return std::make_unique<HybridGaTsIndividual>(tspCfg, std::move(citySequence), crossoverType, memoryDepth, tabuFactory);
}

} // namespace hybrid
} // namespace algo
