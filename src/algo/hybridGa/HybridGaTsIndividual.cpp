#include "HybridGaTsIndividual.hpp"
#include <utils/RandomUtils.hpp>
#include <algo/ts/TspSolutionTabuSearch.hpp>

namespace algo {
namespace hybrid {

HybridGaTsIndividual::HybridGaTsIndividual(const configuration::tsp::TspInstance& tspCfg,
                                           const uint32_t problemSize,
                                           const ga::CrossoverType crossoverType,
                                           const uint32_t memoryDepth,
                                           const ts::TabuAbstractFactory& tabuFactory)
    : ga::TspIndividual{tspCfg, problemSize, crossoverType}
    , memoryDepth{memoryDepth}
    , tabuFactory{tabuFactory}
    , tabuMem{std::make_shared<ts::TabuMemory>(this->memoryDepth, this->tabuFactory)}
    , bestFitnessInEvolutionHistory{std::numeric_limits<double>::infinity()}
    , tabuStatistics{nullptr}
{
}

HybridGaTsIndividual::HybridGaTsIndividual(const configuration::tsp::TspInstance& tspCfg,
                                           tsp::CitySequence&& citySequence,
                                           const ga::CrossoverType crossoverType,
                                           const uint32_t memoryDepth,
                                           const ts::TabuAbstractFactory& tabuFactory)
    : ga::TspIndividual{tspCfg, std::move(citySequence), crossoverType}
    , memoryDepth{memoryDepth}
    , tabuFactory{tabuFactory}
    , tabuMem{std::make_shared<ts::TabuMemory>(this->memoryDepth, this->tabuFactory)}
    , bestFitnessInEvolutionHistory{std::numeric_limits<double>::infinity()}
    , tabuStatistics{nullptr}
{
}

ga::IndividualPtr HybridGaTsIndividual::cloneIndividual() const
{
    return std::make_unique<HybridGaTsIndividual>(*this);
}

void HybridGaTsIndividual::mutation()
{
    bool successfulMutation = false;
    ts::CreationInfoPtr creationInfo;
    uint32_t unsuccessfulMutationsCounter = 0;
    while(!successfulMutation)
    {
        const auto [firstCity, secondCity] = mutationIrgibnnm();
        isCurrentFitnessValid = false;
        const double fitnessAfterMutation = getCurrentFitness();
        creationInfo = std::make_unique<ts::SwapCreationInfo>(firstCity, secondCity);
        if(fitnessAfterMutation < bestFitnessInEvolutionHistory)
        {
            if(tabuStatistics != nullptr)
                tabuStatistics->aspiracyPassNum++;
            successfulMutation = true;
        }
        else if(!tabuMem->isTabuViolated(*creationInfo))
        {
            if(tabuStatistics != nullptr)
                tabuStatistics->tabuPassNum++;
            successfulMutation = true;
        }
        else
        {
            unsuccessfulMutationsCounter++;
            if(tabuStatistics != nullptr)
                tabuStatistics->tabuViolationNum++;
            if(unsuccessfulMutationsCounter >= memoryDepth)
                successfulMutation = true;  // don't loop over and over if its hard to pass tabu
        }
    }
    if(getCurrentFitness() < bestFitnessInEvolutionHistory)
        bestFitnessInEvolutionHistory = getCurrentFitness();

    tabuMem->add(tabuFactory.make(std::move(creationInfo)));
}

std::pair<uint32_t, uint32_t> HybridGaTsIndividual::mutationIrgibnnm()
{
    const int32_t neighbourhoodThreshold = 5;
    auto& random = utils::rnd::Random::getInstance();
    auto lastIndexInChain = static_cast<int32_t>(citySequence.size() - 1);
    auto first = random.getRandomInt(0, lastIndexInChain);
    auto second = random.getRandomInt(0, lastIndexInChain);
    auto firstIt = std::next(citySequence.begin(), std::min(first, second));
    auto firstCityId = *firstIt;
    auto secondIt = std::next(citySequence.begin(), std::max(first, second));
    auto secondCityId = *secondIt;
    std::reverse(firstIt, secondIt);
    auto randomGeneIndex = random.getRandomUint(0, static_cast<uint32_t>(lastIndexInChain));
    auto randomGene = citySequence[randomGeneIndex];
    auto nearestId = tspCfg.nearestDistanceLookup.at(randomGene).first;
    auto nearestCityIndex = static_cast<int32_t>(getIndexOfCityInSequence(nearestId));
    auto leftBound = nearestCityIndex - neighbourhoodThreshold >= 0 ? nearestCityIndex - neighbourhoodThreshold : 0;
    auto rightBound =
        nearestCityIndex + neighbourhoodThreshold <= lastIndexInChain ?
                                                                      nearestCityIndex + neighbourhoodThreshold
                                                                      : lastIndexInChain;
    auto randomNeighbourIndex = random.getRandomUint(static_cast<uint32_t>(leftBound),
                                                     static_cast<uint32_t>(rightBound));
    std::swap(citySequence[randomGeneIndex], citySequence[randomNeighbourIndex]);
    return std::make_pair(firstCityId, secondCityId);
}

ga::IndividualPtr HybridGaTsIndividual::makeIndividual(tsp::CitySequence&& citySequence) const
{
    auto individual = std::make_unique<HybridGaTsIndividual>(tspCfg, std::move(citySequence), crossoverType,
                                                             memoryDepth, tabuFactory);
    individual->setStatisticsPointer(this->tabuStatistics);
    return individual;
}

void HybridGaTsIndividual::setStatisticsPointer(TabuStatistics* const tabuStatistics)
{
    this->tabuStatistics = tabuStatistics;
}

const std::shared_ptr<ts::TabuMemory> &HybridGaTsIndividual::getTabuMemRef() const
{
    return tabuMem;
}

} // namespace hybrid
} // namespace algo
