#pragma once

#include <cstdint>

namespace algo {
namespace hybrid {

struct TabuStatistics
{
    uint32_t tabuViolationNum;
    uint32_t tabuPassNum;
    uint32_t aspiracyPassNum;
};

} // namespace algo
} //namespace hybrid
