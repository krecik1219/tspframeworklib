#pragma once

#include <algo/ts/TsSolutionInterface.hpp>
#include <configuration/TspInstance.hpp>
#include <algo/ga/TspIndividual.hpp>

namespace algo {
namespace hybrid {

namespace cfgtsp = configuration::tsp;

class HybridGaTsIndividual2 : public ga::TspIndividual, public ts::TsSolutionInterface
{
public:
    HybridGaTsIndividual2(const cfgtsp::TspInstance& tspCfg, const uint32_t problemSize, const ga::CrossoverType crossoverType);
    HybridGaTsIndividual2(const cfgtsp::TspInstance& tspCfg, tsp::CitySequence&& citySequence, const ga::CrossoverType crossoverType);

    HybridGaTsIndividual2() = delete;

    virtual ~HybridGaTsIndividual2() override;
    HybridGaTsIndividual2(const HybridGaTsIndividual2&);
    HybridGaTsIndividual2& operator=(const HybridGaTsIndividual2&) = delete;
    HybridGaTsIndividual2(HybridGaTsIndividual2&&) = delete;
    HybridGaTsIndividual2& operator=(HybridGaTsIndividual2&&) = delete;

    virtual ga::IndividualPtr cloneIndividual() const override;
    virtual ga::IndividualPtr makeIndividual(tsp::CitySequence&& citySequence) const override;

    virtual ts::Neighbourhood getNeighbourhood(const uint32_t maxNeighbourhoodSize) override;
    virtual std::string toString() const override;
    virtual ts::SolutionPtr cloneTsSolution() const override;
    virtual double cost() override;

    template <class RandomGenerator>
    static std::unique_ptr<HybridGaTsIndividual2> createRandom(const cfgtsp::TspInstance& tspCfg,
                                                               RandomGenerator&& g,
                                                               const ga::CrossoverType crossoverType);
};


template<class RandomGenerator>
std::unique_ptr<HybridGaTsIndividual2> HybridGaTsIndividual2::createRandom(
    const configuration::tsp::TspInstance& tspCfg, RandomGenerator&& g, const ga::CrossoverType crossoverType)
{
    tsp::CitySequence citySequence(tspCfg.citiesData.size());
    std::iota(citySequence.begin(), citySequence.end(), 0u);
    std::shuffle(citySequence.begin(), citySequence.end(), g);
    return std::make_unique<HybridGaTsIndividual2>(tspCfg, std::move(citySequence), crossoverType);
}

} // namespace hybrid
} // namespace algo
