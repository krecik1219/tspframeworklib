#pragma once

#include <configuration/HybridGaTsCfg2.hpp>
#include <memory>
#include <algo/ga/GeneticAlgorithm.hpp>
#include <tools/logger/Logger.hpp>
#include <algo/ga/selection/SelectionStrategy.hpp>
#include <functional>
#include <algo/ts/TabuAbstractFactory.hpp>

namespace algo {
namespace hybrid {

using TsSolutionToIndividualConverter = std::function<ga::IndividualPtr(ts::SolutionPtr&&)>;

class HybridGaTs2 : public ga::GeneticAlgorithm
{
public:
    HybridGaTs2(const configuration::HybridGaTsCfg2& hybridGaTsCfg,
                const ga::InitFunction& initFunction,
                std::unique_ptr<ga::SelectionStrategy>&& selectionStrategy,
                const ts::TabuAbstractFactory& tabuFactory,
                const TsSolutionToIndividualConverter& solutionConverter,
                tools::Logger& gaLogger, tools::Logger& tsLogger);

    virtual void run() override;

protected:
    virtual void gaLoop() override;

private:
    bool shouldRunTsOptimization();
    void optimizePartOfPopulationWithTs();
    void updateBestIndividualSoFar();

    configuration::HybridGaTsCfg2 hybridGaTsCfg;
    const ts::TabuAbstractFactory& tabuFactory;
    const TsSolutionToIndividualConverter solutionConverter;
    tools::Logger& tsLogger;
    tools::Logger dumbLoggerForTs;
    uint32_t numOfGenerationsWithoutImprovement;
    uint32_t antiBounceTsOptimization;
};

} // namespace hybrid
} // namespace algo
