#pragma once

#include <algorithm>
#include <algo/ga/TspIndividual.hpp>
#include <numeric>
#include <memory>
#include <algo/sa/SaSolutionInterface.hpp>
#include <iostream>

namespace algo {
namespace hybrid {

namespace cfgtsp = configuration::tsp;

class HybridGaSaIndividual : public ga::TspIndividual, public sa::SaSolutionInterface
{
public:
    HybridGaSaIndividual(const cfgtsp::TspInstance& tspCfg, const uint32_t problemSize, const ga::CrossoverType crossoverType);
    HybridGaSaIndividual(const cfgtsp::TspInstance& tspCfg, tsp::CitySequence&& citySequence, const ga::CrossoverType crossoverType);

    HybridGaSaIndividual() = delete;

    virtual ~HybridGaSaIndividual() override;
    HybridGaSaIndividual(const HybridGaSaIndividual&);
    HybridGaSaIndividual& operator=(const HybridGaSaIndividual&) = delete;
    HybridGaSaIndividual(HybridGaSaIndividual&&) = delete;
    HybridGaSaIndividual& operator=(HybridGaSaIndividual&&) = delete;

    virtual double cost() override;
    virtual sa::SolutionPtr getNeighbour() override;
    virtual std::string toString() const override;
    virtual sa::SolutionPtr cloneSaSolution() const override;
    virtual ga::IndividualPtr cloneIndividual() const override;
    virtual ga::IndividualPtr makeIndividual(tsp::CitySequence&& citySequence) const override;

    template <class RandomGenerator>
    static std::unique_ptr<HybridGaSaIndividual> createRandom(const cfgtsp::TspInstance& tspCfg,
                                                              RandomGenerator&& g,
                                                              const ga::CrossoverType crossoverType);
};

template<class RandomGenerator>
std::unique_ptr<HybridGaSaIndividual> HybridGaSaIndividual::createRandom(
    const configuration::tsp::TspInstance& tspCfg, RandomGenerator&& g, const ga::CrossoverType crossoverType)
{
    tsp::CitySequence citySequence(tspCfg.citiesData.size());
    std::iota(citySequence.begin(), citySequence.end(), 0u);
    std::shuffle(citySequence.begin(), citySequence.end(), g);
    return std::make_unique<HybridGaSaIndividual>(tspCfg, std::move(citySequence), crossoverType);
}

} // namespace hybrid
} // namespace algo
