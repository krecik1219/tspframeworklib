#include "HybridGaTsIndividual2.hpp"
#include <utils/HashUtils.hpp>
#include <utils/RandomUtils.hpp>
#include <unordered_set>
#include <algo/ts/TspSolutionTabuSearch.hpp>

namespace algo {
namespace hybrid {

HybridGaTsIndividual2::HybridGaTsIndividual2(const configuration::tsp::TspInstance& tspCfg,
                                             const uint32_t problemSize, const ga::CrossoverType crossoverType)
    : ga::TspIndividual{tspCfg, problemSize, crossoverType}
    , ts::TsSolutionInterface{}
{
}

HybridGaTsIndividual2::HybridGaTsIndividual2(const configuration::tsp::TspInstance& tspCfg,
                                             tsp::CitySequence&& citySequence,
                                             const ga::CrossoverType crossoverType)
    : ga::TspIndividual{tspCfg, std::move(citySequence), crossoverType}
    , ts::TsSolutionInterface{}
{
}

HybridGaTsIndividual2::~HybridGaTsIndividual2() = default;
HybridGaTsIndividual2::HybridGaTsIndividual2(const HybridGaTsIndividual2&) = default;

ga::IndividualPtr HybridGaTsIndividual2::cloneIndividual() const
{
    return std::make_unique<HybridGaTsIndividual2>(*this);
}

ga::IndividualPtr HybridGaTsIndividual2::makeIndividual(tsp::CitySequence&& citySequence) const
{
    return std::make_unique<HybridGaTsIndividual2>(tspCfg, std::move(citySequence), crossoverType);
}

ts::Neighbourhood HybridGaTsIndividual2::getNeighbourhood(const uint32_t maxNeighbourhoodSize)
{
    const uint32_t problemNeighboirhoodSizeLimit = tspCfg.dimension * (tspCfg.dimension - 1) / 2;
    const uint32_t neighbourHoodSizeLimit = std::min(maxNeighbourhoodSize, problemNeighboirhoodSizeLimit);
    ts::Neighbourhood neighbourhood;
    neighbourhood.reserve(neighbourHoodSizeLimit);

    struct SwapPair
    {
        uint32_t cityAIndex;
        uint32_t cityBIndex;

        bool operator<(const SwapPair& other) const
        {
            if(cityAIndex < other.cityAIndex)
                return true;
            if(cityAIndex == other.cityAIndex)
                return cityBIndex < other.cityBIndex;
            else
                return false;
        }

        bool operator==(const SwapPair& other) const
        {
            return cityAIndex == other.cityAIndex && cityBIndex == other.cityBIndex;
        }

        class Hasher
        {
        public:
            size_t operator()(const SwapPair& swapPair) const
            {
                size_t seed = 0;
                utils::hashing::hash_combine(seed, swapPair.cityAIndex);
                utils::hashing::hash_combine(seed, swapPair.cityBIndex);
                return seed;
            }
        };
    };

    std::unordered_set<SwapPair, SwapPair::Hasher> possiblePairs;
    possiblePairs.reserve(citySequence.size() * (citySequence.size() - 1));
    for(auto i = 0u; i < citySequence.size(); i++)
    {
        for(auto j = i + 1u; j < citySequence.size(); j++)
            possiblePairs.insert({i, j});
    }

    auto& random = utils::rnd::Random::getInstance();
    for(auto i = 0u; i < neighbourHoodSizeLimit; i++)
    {
        auto clonedSolution = std::make_unique<HybridGaTsIndividual2>(*this);
        clonedSolution->isCurrentFitnessValid = false;
        int32_t rndIndex = random.getRandomInt(0, static_cast<int32_t>(possiblePairs.size() - 1));
        auto& clonedSolutionCitySeq = clonedSolution->citySequence;
        auto rndIt = std::next(possiblePairs.begin(), rndIndex);
        const uint32_t cityAIndex = (*rndIt).cityAIndex;
        const uint32_t cityBIndex = (*rndIt).cityBIndex;

        auto firstIt = std::next(clonedSolutionCitySeq.begin(), cityAIndex);
        auto secondIt = std::next(clonedSolutionCitySeq.begin(), cityBIndex + 1);

        std::reverse(firstIt, secondIt);
        std::vector<uint32_t> invertedCities(firstIt, secondIt);

        possiblePairs.erase(rndIt);
        neighbourhood.push_back(ts::Neighbour{std::move(clonedSolution),
                                              std::make_unique<ts::SwapCreationInfo>(getCityIdAtIndex(cityAIndex),
                                                                                     getCityIdAtIndex(cityBIndex))});
    }
    return neighbourhood;
}

std::string HybridGaTsIndividual2::toString() const
{
    return ga::TspIndividual::toString();
}

ts::SolutionPtr HybridGaTsIndividual2::cloneTsSolution() const
{
    return std::make_unique<HybridGaTsIndividual2>(*this);
}

double HybridGaTsIndividual2::cost()
{
    return ga::TspIndividual::getCurrentFitness();
}

} // namespace hybrid
} // namespace algo
