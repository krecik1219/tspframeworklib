#pragma once

#include <algo/ga/GeneticAlgorithm.hpp>
#include "TabuStatistics.hpp"

namespace algo {
namespace hybrid {

class HybridGaTs : public ga::GeneticAlgorithm
{
public:
    using ga::GeneticAlgorithm::GeneticAlgorithm;

    TabuStatistics getTabuStatistics() const;
    uint32_t getNumberOfUniqueTabuMemories() const;

protected:
    virtual void initialize() override;

private:
    TabuStatistics tabuStatistics;
};

} // namespace hybrid
} // namespace algo
