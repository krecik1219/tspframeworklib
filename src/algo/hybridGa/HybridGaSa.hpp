#pragma once

#include <configuration/HybridGaSaCfg.hpp>
#include <memory>
#include <algo/ga/GeneticAlgorithm.hpp>
#include <algo/sa/SaSolutionInterface.hpp>
#include <tools/logger/Logger.hpp>
#include <algo/ga/selection/SelectionStrategy.hpp>
#include <functional>

namespace algo {
namespace hybrid {

using SaSolutionToIndividualConverter = std::function<ga::IndividualPtr(sa::SolutionPtr&&)>;
using TwoOptOptimizer = std::function<ga::IndividualPtr(const ga::IndividualInterface&)>;
using ThreeOptOptimizer = std::function<ga::IndividualPtr(const ga::IndividualInterface&)>;

class HybridGaSa : public ga::GeneticAlgorithm
{
public:
    HybridGaSa(const configuration::HybridGaSaCfg& hybridGaSaCfg, const ga::InitFunction& initFunction,
               const SaSolutionToIndividualConverter& saSolutionToIndividualConverter,
               const TwoOptOptimizer& twoOptOptimizer,
               const ThreeOptOptimizer& threeOptOptimizer,
               std::unique_ptr<ga::SelectionStrategy>&& selectionStrategy,
               tools::Logger& gaLogger, tools::Logger& saLogger);

    virtual void run() override;

protected:
    virtual void gaLoop() override;

private:
    bool shouldRunSaOptimization();
    bool shouldInsertRandomsToPopulation();
    void insertRandomsToPopulation();
    void optimizePartOfPopulationWithSa();
    void additionalLocalOptimizations();
    void optimizeBestWith2Opt();
    void optimizeBestWith3Opt();
    void optimizeRandomWith2OptAnd3Opt();

    configuration::HybridGaSaCfg hybridGaSaCfg;
    uint32_t numOfIndividualsToOptimizeWithSa;
    const SaSolutionToIndividualConverter saSolutionToIndividualConverter;
    const TwoOptOptimizer twoOptOptimizer;
    const ThreeOptOptimizer threeOptOptimizer;
    tools::Logger& saLogger;
    tools::Logger dumbLoggerForSa;
    uint32_t howManyTimesInsertedRandoms = 0u;
};

} // namespace hybrid
} // namespace algo
