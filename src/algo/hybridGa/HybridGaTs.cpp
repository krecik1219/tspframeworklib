#include "HybridGaTs.hpp"
#include "HybridGaTsIndividual.hpp"

namespace algo {
namespace hybrid {

TabuStatistics HybridGaTs::getTabuStatistics() const
{
    return tabuStatistics;
}

uint32_t HybridGaTs::getNumberOfUniqueTabuMemories() const
{
    uint32_t numOfUnique = 0u;
    for(auto& individual : population)
    {
        const auto usersOfTabuMem = dynamic_cast<HybridGaTsIndividual&>(*individual).getTabuMemRef().use_count();
        if(usersOfTabuMem == 1)
            numOfUnique++;
    }
    return numOfUnique;
}

void HybridGaTs::initialize()
{
    ga::GeneticAlgorithm::initialize();
    this->tabuStatistics = TabuStatistics{};
    for(auto& individual : population)
        dynamic_cast<HybridGaTsIndividual&>(*individual).setStatisticsPointer(&this->tabuStatistics);
}

} // namespace hybrid
} // namespace algo
