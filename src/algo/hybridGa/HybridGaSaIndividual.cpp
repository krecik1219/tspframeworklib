#include "HybridGaSaIndividual.hpp"
#include <utils/RandomUtils.hpp>

namespace algo {
namespace hybrid {

HybridGaSaIndividual::HybridGaSaIndividual(const cfgtsp::TspInstance& tspCfg,
                                           const uint32_t problemSize,
                                           const ga::CrossoverType crossoverType)
    : ga::TspIndividual{tspCfg, problemSize, crossoverType}
    , sa::SaSolutionInterface{}
{
}

HybridGaSaIndividual::HybridGaSaIndividual(const cfgtsp::TspInstance& tspCfg,
                                           tsp::CitySequence&& citySequence,
                                           const ga::CrossoverType crossoverType)
    : ga::TspIndividual{tspCfg, std::move(citySequence), crossoverType}
    , sa::SaSolutionInterface{}
{
}

HybridGaSaIndividual::~HybridGaSaIndividual() = default;
HybridGaSaIndividual::HybridGaSaIndividual(const HybridGaSaIndividual &) = default;

double HybridGaSaIndividual::cost()
{
    if(!isCurrentFitnessValid)
    {
        evaluate();
        isCurrentFitnessValid = true;
    }
    return currentFitness;
}

sa::SolutionPtr HybridGaSaIndividual::getNeighbour()
{
    auto clone = std::make_unique<HybridGaSaIndividual>(*this);
    clone->isCurrentFitnessValid = false;
    auto& random = utils::rnd::Random::getInstance();
    const int32_t indexA = random.getRandomInt(0, static_cast<int32_t>(clone->citySequence.size() - 1));
    const int32_t indexB = random.getRandomInt(0, static_cast<int32_t>(clone->citySequence.size() - 1));
    auto firstIt = std::next(clone->citySequence.begin(), std::min(indexA, indexB));
    auto secondIt = std::next(clone->citySequence.begin(), std::max(indexA, indexB) + 1);
    std::reverse(firstIt, secondIt);
    return clone;
}

std::string HybridGaSaIndividual::toString() const
{
    return ga::TspIndividual::toString();
}

sa::SolutionPtr HybridGaSaIndividual::cloneSaSolution() const
{
    return std::make_unique<HybridGaSaIndividual>(*this);
}

ga::IndividualPtr HybridGaSaIndividual::cloneIndividual() const
{
    return std::make_unique<HybridGaSaIndividual>(*this);
}

ga::IndividualPtr HybridGaSaIndividual::makeIndividual(tsp::CitySequence&& citySequence) const
{
    return std::make_unique<HybridGaSaIndividual>(tspCfg, std::move(citySequence), crossoverType);
}

} // namespace hybrid
} // namespace algo
