#include "HybridGaSa.hpp"
#include <algo/sa/SimulatedAnnealing.hpp>
#include <utils/RandomUtils.hpp>
#include <tools/logger/DumbLoggingPolicy.hpp>
#include <algorithm>

namespace algo {
namespace hybrid {

HybridGaSa::HybridGaSa(const configuration::HybridGaSaCfg& hybridGaSaCfg, const ga::InitFunction& initFunction,
                       const SaSolutionToIndividualConverter& saSolutionToIndividualConverter,
                       const TwoOptOptimizer& twoOptOptimizer,
                       const ThreeOptOptimizer& threeOptOptimizer,
                       std::unique_ptr<ga::SelectionStrategy>&& selectionStrategy,
                       tools::Logger& gaLogger, tools::Logger& saLogger)
    : ga::GeneticAlgorithm{hybridGaSaCfg.gaCfg, initFunction, std::move(selectionStrategy), gaLogger}
    , hybridGaSaCfg{hybridGaSaCfg}
    , numOfIndividualsToOptimizeWithSa{
          static_cast<uint32_t>(hybridGaSaCfg.gaCfg.populationSize * hybridGaSaCfg.percentOfPopulationToOptimizeWithSa)}
    , saSolutionToIndividualConverter{saSolutionToIndividualConverter}
    , twoOptOptimizer{twoOptOptimizer}
    , threeOptOptimizer{threeOptOptimizer}
    , saLogger {saLogger}
    , dumbLoggerForSa{std::make_unique<tools::DumbLoggingPolicy>(), false, true}
{
}

void HybridGaSa::run()
{
    initialize();
    evaluate();
    updateStatisticValues();
    updateBestIndividualSoFar();
    logState();
    gaLoop();
    generationNum++;
    optimizePartOfPopulationWithSa();
    updateStatisticValues();
    updateBestIndividualSoFar();
    // additionalLocalOptimizations();
    logState();
}

void HybridGaSa::gaLoop()
{
    while(!stopConditionReached())
    {
        selection();
        evaluate();
        generationNum++;
        if(shouldRunSaOptimization())
        {
            optimizePartOfPopulationWithSa();
        }
        updateStatisticValues();
        updateBestIndividualSoFar();
        logState();
//        if(shouldInsertRandomsToPopulation())
//        {
//            insertRandomsToPopulation();
//            evaluate();
//        }
    }
}

bool HybridGaSa::shouldRunSaOptimization()
{
    return generationNum % hybridGaSaCfg.saOptimizationGenerationsInterval == 0;
}

bool HybridGaSa::shouldInsertRandomsToPopulation()
{
    return std::abs(avgFitnessStatistic - bestFitnessStatistic) / bestFitnessStatistic <= 0.01;
}

void HybridGaSa::insertRandomsToPopulation()
{
    const double percentToReplace = 0.7;
    uint32_t numToReplace = static_cast<uint32_t>(percentToReplace * population.size());
    auto& random = utils::rnd::Random::getInstance();
    for(auto i = 0u; i < numToReplace; i++)
    {
        uint32_t indexToReplace = random.getRandomUint(0u, static_cast<uint32_t>(population.size() - 1));
        population[indexToReplace] = initFunction();
    }
    howManyTimesInsertedRandoms++;
}

void HybridGaSa::optimizePartOfPopulationWithSa()
{
    auto start = std::chrono::steady_clock::now();
    auto& random = utils::rnd::Random::getInstance();
    for(auto i = 0u; i < numOfIndividualsToOptimizeWithSa; i++)
    {
        const uint32_t selectedIndividual = random.getRandomUint(0u, static_cast<uint32_t>(population.size() - 1));
        auto& individual = *population[selectedIndividual];
        auto& individualAsSaSolution = dynamic_cast<sa::SaSolutionInterface&>(individual);
        auto initFunction = [&individualAsSaSolution]() {return individualAsSaSolution.cloneSaSolution();};
        sa::SimulatedAnnealing saAlgo{hybridGaSaCfg.saCfg, initFunction, dumbLoggerForSa};
        auto optimizedSolution = saAlgo.run();
        saLogger.log("selected=%u, before opt=%.3f, after opt=%.3f", selectedIndividual,
                     individual.getCurrentFitness(), optimizedSolution->cost());
        if(optimizedSolution->cost() < individual.getCurrentFitness())
        {
            population[selectedIndividual] = saSolutionToIndividualConverter(std::move(optimizedSolution));
        }
    }
    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    saLogger.log("optimization has taken %.3f seconds", elapsed.count() / 1000.0);
}

void HybridGaSa::additionalLocalOptimizations()
{
    auto start = std::chrono::steady_clock::now();
    optimizeBestWith2Opt();
    optimizeBestWith3Opt();
    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    using namespace std::chrono_literals;
    const std::chrono::minutes maxAdditionalOptimizationsDuration = 5min;
    const uint32_t howManyIndividualsCouldBeOptimized = static_cast<uint32_t>(maxAdditionalOptimizationsDuration / elapsed);
    saLogger.log("best individual 2-opt and 3-opt have taken: %.3f seconds, how many individuals could be optimized: %u",
                 elapsed.count() / 1000.0, howManyIndividualsCouldBeOptimized);
    const uint32_t numOfIndividualsToOptimize = std::min(howManyIndividualsCouldBeOptimized, static_cast<uint32_t>(population.size()));
    auto startAdditionalOpt = std::chrono::steady_clock::now();
    for(auto i = 0u; i < numOfIndividualsToOptimize; i++)
    {
        optimizeRandomWith2OptAnd3Opt();
    }
    auto endAdditionalOpt = std::chrono::steady_clock::now();
    auto elapsedAdditionalOpt = std::chrono::duration_cast<std::chrono::milliseconds>(endAdditionalOpt - startAdditionalOpt);
    saLogger.log("additional optimalizations have taken: %.3f seconds, optimized individuals num: %u",
                 elapsedAdditionalOpt.count() / 1000.0, numOfIndividualsToOptimize);
    updateStatisticValues();
    updateBestIndividualSoFar();
}

void HybridGaSa::optimizeBestWith2Opt()
{
    auto start = std::chrono::steady_clock::now();
    auto optimized = twoOptOptimizer(*bestSoFar);
    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    saLogger.log("best before 2-opt=%.3f, best after 2-opt=%.3f, elapsed=%.3f seconds",
                 bestSoFar->getCurrentFitness(), optimized->getCurrentFitness(), elapsed.count() / 1000.0);
    if(optimized->getCurrentFitness() < bestSoFar->getCurrentFitness())
    {
        bestSoFar = std::move(optimized);
        bestFitnessStatistic = bestSoFar->getCurrentFitness();
    }
}

void HybridGaSa::optimizeBestWith3Opt()
{
    auto start = std::chrono::steady_clock::now();
    auto optimized = threeOptOptimizer(*bestSoFar);
    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    saLogger.log("best before 3-opt=%.3f, best after 3-opt=%.3f, elapsed=%.3f seconds",
                 bestSoFar->getCurrentFitness(), optimized->getCurrentFitness(), elapsed.count() / 1000.0);
    if(optimized->getCurrentFitness() < bestSoFar->getCurrentFitness())
    {
        bestSoFar = std::move(optimized);
        bestFitnessStatistic = bestSoFar->getCurrentFitness();
    }
}

void HybridGaSa::optimizeRandomWith2OptAnd3Opt()
{
    auto& random = utils::rnd::Random::getInstance();
    const uint32_t rndIndividualIndex = random.getRandomUint(0u, static_cast<uint32_t>(population.size() - 1));
    auto& randomIndividual = population[rndIndividualIndex];
    auto optimizedBy2Opt = twoOptOptimizer(*randomIndividual);
    auto optimizedBy3Opt = threeOptOptimizer(*optimizedBy2Opt);
    if(optimizedBy3Opt->getCurrentFitness() < randomIndividual->getCurrentFitness())
    {
        population[rndIndividualIndex] = std::move(optimizedBy3Opt);
    }
}

} // namespace hybrid
} // namespace algo
