#pragma once
#include <cstdint>
#include <vector>
#include <tsp/TspSolution.hpp>

namespace algo {

class TwoOpt
{
public:
    tsp::TspSolution run2Opt(const tsp::TspSolution& inputSolution) const;

private:
    double calcDistanceDiff(const int32_t firstCityIndex, const int32_t secondCityIndex,
                            const tsp::CitySequence& citySeq, const configuration::tsp::TspInstance& tspCfg) const;
    tsp::CitySequence twoOptSwap(const uint32_t firstCityIndex, const uint32_t secondCityIndex,
                                 const tsp::CitySequence& solution) const;
};

} // namespace algo
