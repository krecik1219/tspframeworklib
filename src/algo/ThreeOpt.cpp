#include "ThreeOpt.hpp"
#include <algorithm>
#include <iterator>

namespace algo {

tsp::TspSolution ThreeOpt::run3Opt(const tsp::TspSolution& inputSolution)
{
    bool improved = true;
    tspCfg = &inputSolution.getTspCfg();
    tsp::CitySequence currentSolution = inputSolution.getCitySequence();
    const uint32_t citiesNum = currentSolution.size();
    while(improved)
    {
        improved = false;
        int32_t delta = 0;
        for(auto i = 0u; i < citiesNum - 3; i++)
        {
            for(auto j = i + 2; j < citiesNum - 1; j++)
            {
                const uint32_t kLimit = i > 0 ? citiesNum + 1 : citiesNum;
                for(auto k = j + 2; k < kLimit; k++)
                {
                    delta += reverseTourIfBetter(static_cast<int32_t>(i),
                                                 static_cast<int32_t>(j),
                                                 static_cast<int32_t>(k),
                                                 currentSolution);
                }
            }
        }
        if(delta < 0)
            improved = true;
    }
    return tsp::TspSolution{inputSolution.getTspCfg(), std::move(currentSolution)};
}

double ThreeOpt::reverseTourIfBetter(const int32_t i, const int32_t j, const int32_t k,
                                     tsp::CitySequence& solution) const
{
    const uint32_t cityA = i - 1 >= 0 ? solution[static_cast<uint32_t>(i - 1)] : solution.back();
    const uint32_t cityB = solution[static_cast<uint32_t>(i)];
    const uint32_t cityC = solution[static_cast<uint32_t>(j - 1)];
    const uint32_t cityD = solution[static_cast<uint32_t>(j)];
    const uint32_t cityE = solution[static_cast<uint32_t>(k - 1)];
    const uint32_t cityF = static_cast<uint32_t>(k) < solution.size()
                               ? solution[static_cast<uint32_t>(k)] : solution[0];

    const double d0 = distance(cityA, cityB) + distance(cityC, cityD) + distance(cityE, cityF);
    const double d1 = distance(cityA, cityC) + distance(cityB, cityD) + distance(cityE, cityF);
    const double d2 = distance(cityA, cityB) + distance(cityC, cityE) + distance(cityD, cityF);
    const double d3 = distance(cityA, cityD) + distance(cityE, cityB) + distance(cityC, cityF);
    const double d4 = distance(cityF, cityB) + distance(cityC, cityD) + distance(cityE, cityA);

    uint32_t whichMin = 0;
    if(d1 < d2 && d1 < d3 && d1 < d4)
        whichMin = 1;
    if(d2 < d1 && d2 < d3 && d2 < d4)
        whichMin = 2;
    if(d3 < d2 && d3 < d1 && d3 < d4)
        whichMin = 3;
    if(d4 < d2 && d4 < d3 && d4 < d1)
        whichMin = 4;

    if(d0 > d1 && whichMin == 1)
    {
        auto iIt = std::next(solution.begin(), i);
        auto jIt = std::next(solution.begin(), j);
        std::reverse(iIt, jIt);
        return -d0 + d1;
    }
    else if(d0 > d2 && whichMin == 2)
    {
        auto jIt = std::next(solution.begin(), j);
        auto kIt = std::next(solution.begin(), k);
        std::reverse(jIt, kIt);
        return -d0 + d2;
    }
    else if(d0 > d4 && whichMin == 4)
    {
        auto iIt = std::next(solution.begin(), i);
        auto kIt = std::next(solution.begin(), k);
        std::reverse(iIt, kIt);
        return -d0 + d4;
    }
    else if(d0 > d3 && whichMin == 3)
    {
        auto iIt = std::next(solution.begin(), i);
        auto jIt = std::next(solution.begin(), j);
        auto kIt = std::next(solution.begin(), k);
        std::vector<uint32_t> tmp(jIt, kIt);
        tmp.reserve(tmp.size() + static_cast<uint32_t>(j - i));
        jIt = std::next(solution.begin(), j);
        tmp.insert(tmp.end(), iIt, jIt);
        iIt = std::next(solution.begin(), i);
        kIt = std::next(solution.begin(), k);
        std::copy(tmp.cbegin(), tmp.cend(), iIt);
        // tmp = tour[j:k] + tour[i:j];
        // tour[i:k] = tmp;
        return -d0 + d3;
    }
    return 0.0;
}

double ThreeOpt::distance(const uint32_t city1, const uint32_t city2) const
{
    return tspCfg->distanceMatrix[city1][city2];
}

} // namespace algo
