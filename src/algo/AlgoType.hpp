#pragma once

namespace algo {

enum class AlgoType
{
    GA,
    TS,
    SA,
    HYBRID_GA_SA,
    HYBRID_GA_TS,
    HYBRID_GA_TS_2,
    GREEDY,
    RANDOM,
    UNKNOWN,
};

} // namespace algo
