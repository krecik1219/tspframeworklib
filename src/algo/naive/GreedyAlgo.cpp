#include "GreedyAlgo.hpp"
#include <algorithm>
#include <numeric>

namespace algo {
namespace naive {

GreedyAlgo::GreedyAlgo(const configuration::tsp::TspInstance& tspCfg)
    : tspCfg{tspCfg}
    , statistics{}
{
}

GreedyAlgo::~GreedyAlgo() = default;

Statistics GreedyAlgo::getLastRunStatistics() const
{
    return statistics;
}

std::unique_ptr<tsp::TspSolution> GreedyAlgo::getGreedyBest()
{
    std::unique_ptr<tsp::TspSolution> bestSolution;
    double bestFitness = std::numeric_limits<double>::infinity();
    std::vector<double> fitnessList(tspCfg.dimension);
    for (auto i = 0u; i < tspCfg.dimension; i++)
    {
        tsp::CitySequence citySequence;
        citySequence.reserve(tspCfg.dimension);
        const uint32_t startingCity = i;
        citySequence.push_back(startingCity);
        AlreadyVisitedCitiesLookup alreadyVisited(tspCfg.dimension, false);
        alreadyVisited[startingCity] = true;
        for(auto j = 0u; j < tspCfg.dimension - 1; j++)
        {
            const uint32_t nearestCityId = findNearestCity(citySequence[j], alreadyVisited);
            citySequence.push_back(nearestCityId);
            alreadyVisited[nearestCityId] = true;
        }
        auto solution = std::make_unique<tsp::TspSolution>(tspCfg, std::move(citySequence));
        fitnessList[i] = solution->computeTotalDistance();
        if(fitnessList[i] < bestFitness)
        {
            bestFitness = fitnessList[i];
            bestSolution = std::move(solution);
        }
    }
    statistics.computeStatistics(fitnessList);
    return bestSolution;
}

uint32_t GreedyAlgo::findNearestCity(const uint32_t cityId, const AlreadyVisitedCitiesLookup& alreadyVisited)
{
    auto minDistance = std::numeric_limits<double>::infinity();
    uint32_t nearestCityId = 0u;
    for (auto i = 0u; i < tspCfg.dimension; i++)
    {
        if (i == cityId)
            continue;
        if (alreadyVisited[i])
            continue;
        double distance = tspCfg.distanceMatrix[cityId][i];
        if (distance < minDistance)
        {
            minDistance = distance;
            nearestCityId = i;
        }
    }
    return nearestCityId;
}

} // namespace naive
} // namespace algo
