#include "RandomSearch.hpp"

namespace algo {
namespace naive {

RandomSearch::RandomSearch(const configuration::RandomCfg& randomCfg,
                           const configuration::tsp::TspInstance& tspCfg, tools::Logger& logger)
    : randomCfg{randomCfg}
    , tspCfg{tspCfg}
    , statistics{}
    , logger{logger}
{
}

RandomSearch::~RandomSearch() = default;

Statistics RandomSearch::getLastRunStatistics() const
{
    return statistics;
}

void RandomSearch::logSolution(const uint32_t iterNum, const double fitness)
{
    logger.log("%u, %.3f", iterNum, fitness);
}

} // namespace naive
} // namespace algo
