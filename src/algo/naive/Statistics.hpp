#pragma once

#include <cstdint>
#include <vector>

namespace algo {
namespace naive {

struct Statistics
{
    uint32_t iterationsNum;
    double bestFitness;
    double avgFitness;
    double stdFitness;
    double worstFitness;

    void computeStatistics(const std::vector<double>& samples);
};

} // namespace naive
} // namespace algo
