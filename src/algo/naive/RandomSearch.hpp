#pragma once

#include <configuration/TspInstance.hpp>
#include <random>
#include <memory>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <tsp/TspSolution.hpp>
#include "Statistics.hpp"
#include <configuration/RandomCfg.hpp>
#include <tools/logger/Logger.hpp>

namespace algo {
namespace naive {

class RandomSearch
{
public:
    RandomSearch(const configuration::RandomCfg& randomCfg,
                 const configuration::tsp::TspInstance& tspCfg, tools::Logger& loggers);
    ~RandomSearch();

    RandomSearch() = delete;
    RandomSearch(const RandomSearch&) = delete;
    RandomSearch& operator=(const RandomSearch&) = delete;
    RandomSearch(RandomSearch&&) = delete;
    RandomSearch& operator=(RandomSearch&&) = delete;

    template<class RandomGenerator>
    std::unique_ptr<tsp::TspSolution> getRandomSolution(RandomGenerator&& rndGen);

    Statistics getLastRunStatistics() const;

private:
    template<class RandomGenerator>
    std::unique_ptr<tsp::TspSolution> createRandomSolution(RandomGenerator&& rndGen);

    void logSolution(const uint32_t iterNum, const double fitness);

    const configuration::RandomCfg randomCfg;
    const configuration::tsp::TspInstance& tspCfg;
    Statistics statistics;
    tools::Logger& logger;
};

template<class RandomGenerator>
std::unique_ptr<tsp::TspSolution> RandomSearch::getRandomSolution(RandomGenerator&& rndGen)
{
    statistics = Statistics{};
    statistics.iterationsNum = randomCfg.iterations;
    if(randomCfg.iterations == 0u)
        return nullptr;

    std::vector<double> fitnessList(randomCfg.iterations);
    double bestFitness = std::numeric_limits<double>::infinity();
    std::unique_ptr<tsp::TspSolution> bestSolution;
    for(auto i = 0u; i < randomCfg.iterations; i++)
    {
        auto solution = createRandomSolution(std::forward<RandomGenerator>(rndGen));
        fitnessList[i] = solution->computeTotalDistance();
        if(fitnessList[i] < bestFitness)
        {
            bestFitness = fitnessList[i];
            bestSolution = std::move(solution);
        }
        logSolution(i, fitnessList[i]);
    }
    statistics.computeStatistics(fitnessList);
    return bestSolution;
}

template<class RandomGenerator>
std::unique_ptr<tsp::TspSolution> RandomSearch::createRandomSolution(RandomGenerator&& rndGen)
{
    tsp::CitySequence citySequence(tspCfg.citiesData.size());
    std::iota(citySequence.begin(), citySequence.end(), 0u);
    std::shuffle(citySequence.begin(), citySequence.end(), std::forward<RandomGenerator>(rndGen));
    return std::make_unique<tsp::TspSolution>(tspCfg, std::move(citySequence));
}

} // namespace naive
} // namespace algo
