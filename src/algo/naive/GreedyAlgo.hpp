#pragma once

#include <configuration/TspInstance.hpp>
#include <memory>
#include <cstdint>
#include <tsp/TspSolution.hpp>
#include "Statistics.hpp"

namespace algo {
namespace naive {

using AlreadyVisitedCitiesLookup = std::vector<bool>;

class GreedyAlgo
{
public:
    GreedyAlgo(const configuration::tsp::TspInstance& tspCfg);
    ~GreedyAlgo();

    GreedyAlgo() = delete;
    GreedyAlgo(const GreedyAlgo&) = delete;
    GreedyAlgo& operator=(const GreedyAlgo&) = delete;
    GreedyAlgo(GreedyAlgo&&) = delete;
    GreedyAlgo& operator=(GreedyAlgo&&) = delete;

    std::unique_ptr<tsp::TspSolution> getGreedyBest();
    Statistics getLastRunStatistics() const;

private:
    uint32_t findNearestCity(const uint32_t cityId, const AlreadyVisitedCitiesLookup& alreadyVisited);

    const configuration::tsp::TspInstance& tspCfg;
    Statistics statistics;
};


} // namespace naive
} // namespace algo
