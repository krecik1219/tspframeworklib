#include "Statistics.hpp"
#include <algorithm>
#include <numeric>
#include <cmath>

namespace algo {
namespace naive {

void Statistics::computeStatistics(const std::vector<double>& samples)
{
    iterationsNum = samples.size();
    avgFitness =
        std::accumulate(samples.cbegin(), samples.cend(), 0.0) / static_cast<double>(samples.size());
    double sumForStd = 0.0;
    for(auto sample : samples)
        sumForStd += ((sample - avgFitness) * (sample - avgFitness));
    stdFitness = std::sqrt(sumForStd / samples.size());
    auto minMaxIt = std::minmax_element(samples.cbegin(), samples.cend());
    bestFitness = *minMaxIt.first;
    worstFitness = *minMaxIt.second;
}

} // namespace naive
} // namespace algo
