#include "TspIndividual.hpp"
#include <utils/RandomUtils.hpp>
#include <cstdint>
#include <set>
#include <iterator>
#include <list>

namespace algo {
namespace ga {

TspIndividual::TspIndividual(const cfgtsp::TspInstance& tspCfg,
                             const uint32_t problemSize,
                             const CrossoverType crossoverType)
    : IndividualInterface()
    , tsp::TspSolution(tspCfg, problemSize)
    , crossoverType{crossoverType}
{
}

TspIndividual::TspIndividual(const cfgtsp::TspInstance& tspCfg,
                             tsp::CitySequence&& citySequence,
                             const CrossoverType crossoverType)
    : IndividualInterface()
    , tsp::TspSolution(tspCfg, std::move(citySequence))
    , crossoverType{crossoverType}
{
}

TspIndividual::~TspIndividual() = default;
TspIndividual::TspIndividual(const TspIndividual&) = default;

IndividualPtr TspIndividual::cloneIndividual() const
{
    return std::make_unique<TspIndividual>(*this);
}

void TspIndividual::evaluate()
{
    if(!isCurrentFitnessValid)
    {
        currentFitness = computeTotalDistance();
        isCurrentFitnessValid = true;
    }
}

void TspIndividual::mutation()
{
    mutationIrgibnnm();
    // mutationByInversion();
    isCurrentFitnessValid = false;
}

void TspIndividual::mutationIrgibnnm()
{
    const int32_t neighbourhoodThreshold = 5;
    auto& random = utils::rnd::Random::getInstance();
    auto lastIndexInChain = static_cast<int32_t>(citySequence.size() - 1);
    auto first = random.getRandomInt(0, lastIndexInChain);
    auto second = random.getRandomInt(0, lastIndexInChain);
    auto firstIt = std::next(citySequence.begin(), std::min(first, second));
    auto secondIt = std::next(citySequence.begin(), std::max(first, second));
    std::reverse(firstIt, secondIt);
    auto randomGeneIndex = random.getRandomUint(0, static_cast<uint32_t>(lastIndexInChain));
    auto randomGene = citySequence[randomGeneIndex];
    auto nearestId = tspCfg.nearestDistanceLookup.at(randomGene).first;
    auto nearestCityIndex = static_cast<int32_t>(getIndexOfCityInSequence(nearestId));
    auto leftBound = nearestCityIndex - neighbourhoodThreshold >= 0 ? nearestCityIndex - neighbourhoodThreshold : 0;
    auto rightBound =
        nearestCityIndex + neighbourhoodThreshold <= lastIndexInChain ?
                                                                      nearestCityIndex + neighbourhoodThreshold
                                                                      : lastIndexInChain;
    auto randomNeighbourIndex = random.getRandomUint(static_cast<uint32_t>(leftBound),
                                                     static_cast<uint32_t>(rightBound));
    std::swap(citySequence[randomGeneIndex], citySequence[randomNeighbourIndex]);
}

void TspIndividual::mutationByInversion()
{
    auto& random = utils::rnd::Random::getInstance();
    auto lastIndexInChain = static_cast<int32_t>(citySequence.size() - 1);
    auto first = random.getRandomInt(0, lastIndexInChain);
    auto second = random.getRandomInt(0, lastIndexInChain);
    auto firstIt = std::next(citySequence.begin(), std::min(first, second));
    auto secondIt = std::next(citySequence.begin(), std::max(first, second));
    std::reverse(firstIt, secondIt);
}

uint32_t TspIndividual::getIndexOfCityInSequence(const uint32_t cityId) const
{
    return static_cast<uint32_t>(
        std::distance(citySequence.cbegin(),
                      std::find_if(citySequence.cbegin(), citySequence.cend(),
                                   [cityId](const auto& city) {return city == cityId; }))
        );
}

IndividualPtr TspIndividual::crossover(const IndividualInterface& other) const
{
    const auto& otherIndividual = dynamic_cast<const TspIndividual&>(other);
    if(crossoverType == CrossoverType::OX)
        return crossoverOx(otherIndividual);
    else
        return crossoverNrx(otherIndividual);
}

IndividualPtr TspIndividual::crossoverNrx(const TspIndividual& otherIndividual) const
{
    std::vector<double> stepsSum(citySequence.size() + 1);
    auto& random = utils::rnd::Random::getInstance();
    auto referenceCityPos = random.getRandomUint(0, static_cast<uint32_t>(citySequence.size() - 1));
    for (auto i = 0u; i < citySequence.size(); i++)
        stepsSum[i] = (getStepsNumTo(referenceCityPos, i) * currentFitness
                       + otherIndividual.getStepsNumTo(referenceCityPos, i) * otherIndividual.currentFitness);

    std::vector<uint32_t> offspringCitySequence(citySequence.size());
    std::iota(offspringCitySequence.begin(), offspringCitySequence.end(), 0u);
    for (auto i = 0u; i < citySequence.size(); i++)
    {
        for (auto j = i + 1; j < citySequence.size(); j++)
        {
            if (stepsSum[j] < stepsSum[i])
                std::swap(offspringCitySequence[i], offspringCitySequence[j]);
        }
    }
    return makeIndividual(std::move(offspringCitySequence));
}

IndividualPtr TspIndividual::crossoverOx(const TspIndividual& otherIndividual) const
{
    auto& random = utils::rnd::Random::getInstance();
    const uint32_t lastIndexInChain = static_cast<uint32_t>(citySequence.size() - 1);
    auto first = random.getRandomUint(0, lastIndexInChain);
    auto second = random.getRandomUint(0, lastIndexInChain);
    auto firstItParent = std::next(citySequence.begin(), std::min(first, second));
    auto secondItParent = std::next(citySequence.begin(), std::max(first, second));
    tsp::CitySequence parent1SubSequence(firstItParent, secondItParent);
    std::list<uint32_t> orderedLeftCitiesFromParent2;
    std::copy_if(otherIndividual.citySequence.cbegin(), otherIndividual.citySequence.cend(),
                 std::back_inserter(orderedLeftCitiesFromParent2), [&parent1SubSequence](const auto cityId){
                     return std::find(parent1SubSequence.cbegin(), parent1SubSequence.cend(), cityId)
                            == parent1SubSequence.cend();
                 });
    uint32_t iteratorOffset = std::min(first, second);
    for(auto i = 0u; i < parent1SubSequence.size(); i++)
    {
        auto it = orderedLeftCitiesFromParent2.begin();
        std::advance(it, iteratorOffset);
        orderedLeftCitiesFromParent2.insert(it, parent1SubSequence[i]);
        iteratorOffset++;
    }

    return makeIndividual(tsp::CitySequence(orderedLeftCitiesFromParent2.cbegin(),
                                            orderedLeftCitiesFromParent2.cend()));
}

IndividualPtr TspIndividual::makeIndividual(tsp::CitySequence&& citySequence) const
{
    return std::make_unique<TspIndividual>(tspCfg, std::move(citySequence), crossoverType);
}

uint32_t TspIndividual::getStepsNumTo(const uint32_t refCityPos, const uint32_t destCityId) const
{
    auto stepsNum = 0u;
    for (auto i = refCityPos; i < citySequence.size(); i++)
    {
        if (citySequence[i] == destCityId)
            return stepsNum;
        stepsNum++;
    }
    for (auto i = 0u; i < refCityPos; i++)
    {
        if (citySequence[i] == destCityId)
            return stepsNum;
        stepsNum++;
    }
    return 0u;
}


bool TspIndividual::validate()
{
    if(citySequence.size() != tspCfg.dimension)
        return false;

    std::set<uint32_t> citiesAsSet(citySequence.begin(), citySequence.end());
    if(citiesAsSet.size() != tspCfg.dimension)
        return false;

    std::set<uint32_t> allCities;
    for(auto i = 0u; i < tspCfg.dimension; i++)
        allCities.insert(i);

    if(citiesAsSet != allCities)
        return false;

    return true;
}

std::string TspIndividual::toString() const
{
    std::string repr = "fitness=" + std::to_string(currentFitness) + ", " + tsp::TspSolution::toString();
    return repr;
}

} // namespace ga
} // namespace algo
