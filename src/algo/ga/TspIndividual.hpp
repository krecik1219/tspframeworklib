#pragma once

#include <algorithm>
#include <tsp/TspSolution.hpp>
#include <configuration/TspInstance.hpp>
#include "IndividualInterface.hpp"
#include <numeric>
#include <memory>
#include <iostream>

namespace algo {
namespace ga {

namespace cfgtsp = configuration::tsp;

enum class CrossoverType
{
    OX,
    NRX
};

class TspIndividual : public IndividualInterface, public tsp::TspSolution
{
public:
    TspIndividual(const cfgtsp::TspInstance& tspCfg, const uint32_t problemSize, const CrossoverType crossoverType);
    TspIndividual(const cfgtsp::TspInstance& tspCfg, tsp::CitySequence&& citySequence, const CrossoverType crossoverType);

    TspIndividual() = delete;

    virtual ~TspIndividual() override;
    TspIndividual(const TspIndividual&);
    TspIndividual& operator=(const TspIndividual&) = delete;
    TspIndividual(TspIndividual&&) = delete;
    TspIndividual& operator=(TspIndividual&&) = delete;

    virtual IndividualPtr cloneIndividual() const override;
    virtual void evaluate() override;
    virtual void mutation() override;
    virtual IndividualPtr crossover(const IndividualInterface& other) const override;

    virtual bool validate() override;
    virtual std::string toString() const override;
    virtual IndividualPtr makeIndividual(tsp::CitySequence&& citySequence) const;

    template <class RandomGenerator>
    static std::unique_ptr<TspIndividual> createRandom(const cfgtsp::TspInstance& tspCfg,
                                                       RandomGenerator&& g,
                                                       const CrossoverType crossoverType);

protected:
    void mutationIrgibnnm();
    void mutationByInversion();
    IndividualPtr crossoverNrx(const TspIndividual& otherIndividual) const;
    IndividualPtr crossoverOx(const TspIndividual& otherIndividual) const;

    uint32_t getIndexOfCityInSequence(const uint32_t cityId) const;
    uint32_t getStepsNumTo(const uint32_t refCityPos, const uint32_t destCityId) const;

    const CrossoverType crossoverType;
};

template<class RandomGenerator>
std::unique_ptr<TspIndividual> TspIndividual::createRandom(const configuration::tsp::TspInstance& tspCfg,
                                                           RandomGenerator&& g,
                                                           const CrossoverType crossoverType)
{
    tsp::CitySequence citySequence(tspCfg.citiesData.size());
    std::iota(citySequence.begin(), citySequence.end(), 0u);
    std::shuffle(citySequence.begin(), citySequence.end(), g);
    return std::make_unique<TspIndividual>(tspCfg, std::move(citySequence), crossoverType);
}

} // namespace ga
} // namespace algo
