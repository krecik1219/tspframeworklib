#include "IndividualInterface.hpp"
#include <limits>

namespace algo {
namespace ga {

IndividualInterface::IndividualInterface()
    : currentFitness{std::numeric_limits<double>::infinity()}
    , isCurrentFitnessValid{false}
{
}

IndividualInterface::~IndividualInterface() = default;
IndividualInterface::IndividualInterface(const IndividualInterface &) = default;
IndividualInterface &IndividualInterface::operator=(const IndividualInterface &) = default;
IndividualInterface::IndividualInterface(IndividualInterface &&) = default;
IndividualInterface &IndividualInterface::operator=(IndividualInterface &&) = default;

double IndividualInterface::getCurrentFitness()
{
    if(!isCurrentFitnessValid)
        evaluate();
    return currentFitness;
}

} // namespace ga
} // namespace algo
