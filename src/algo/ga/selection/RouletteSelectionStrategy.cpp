#include "RouletteSelectionStrategy.hpp"
#include <utils/RandomUtils.hpp>
#include <algorithm>
#include <numeric>

namespace algo {
namespace ga {

const IndividualInterface& RouletteSelectionStrategy::selection(const std::vector<IndividualPtr>& population)
{
    std::vector<double> scaledFitnesses(population.size());
    const double scalingFactor = 10e10;
    std::generate(scaledFitnesses.begin(), scaledFitnesses.end(),
                  [i = 0u, &population, &scalingFactor]() mutable
                  {return 1.0 / population[i++]->getCurrentFitness() * scalingFactor; });
    std::vector<double> partialFitnessSums(population.size());
    std::partial_sum(scaledFitnesses.cbegin(), scaledFitnesses.cend(), partialFitnessSums.begin());
    auto& random = utils::rnd::Random::getInstance();
    auto randomVal = random.getRandomDouble(0.0, partialFitnessSums.back());
    auto winnerIt = std::lower_bound(partialFitnessSums.cbegin(), partialFitnessSums.cend(), randomVal);
    uint32_t index = static_cast<uint32_t>(std::distance(partialFitnessSums.cbegin(), winnerIt));
    return *population[index];
}

} // namespace ga
} // namespace algo
