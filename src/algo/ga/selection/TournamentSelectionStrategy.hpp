#pragma once

#include "SelectionStrategy.hpp"
#include <cstdint>

namespace algo {
namespace ga {

class TournamentSelectionStrategy : public SelectionStrategy
{
public:
    explicit TournamentSelectionStrategy(const uint32_t tournamentSize);

    virtual const IndividualInterface& selection(const std::vector<IndividualPtr>& population) override;

private:
    const uint32_t tournamentSize;
};

} // namespace ga
} // namespace algo
