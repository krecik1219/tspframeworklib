#include "SelectionStrategyFactory.hpp"
#include "TournamentSelectionStrategy.hpp"
#include "RouletteSelectionStrategy.hpp"

namespace algo {
namespace ga {

std::unique_ptr<SelectionStrategy> SelectionStrategyFactory::make(const configuration::GaCfg& gaCfg) const
{
    if(gaCfg.selectionStrategy == "tournament")
        return std::make_unique<TournamentSelectionStrategy>(gaCfg.tournamentSize);
    else if(gaCfg.selectionStrategy == "roulette")
        return std::make_unique<RouletteSelectionStrategy>();
    else
        throw std::runtime_error("Invalid selection strategy: " + gaCfg.selectionStrategy);
}

} // namespace ga
} // namespace algo
