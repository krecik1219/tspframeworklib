#pragma once

#include "SelectionStrategy.hpp"
#include <cstdint>

namespace algo {
namespace ga {

class RouletteSelectionStrategy : public SelectionStrategy
{
public:
    virtual const IndividualInterface& selection(const std::vector<IndividualPtr>& population) override;
};

} // namespace ga
} // namespace algo
