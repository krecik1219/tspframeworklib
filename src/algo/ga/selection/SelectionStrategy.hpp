#pragma once

#include <algo/ga/IndividualInterface.hpp>
#include <vector>

namespace algo {
namespace ga {

class SelectionStrategy
{
public:
    SelectionStrategy();
    virtual ~SelectionStrategy();

    SelectionStrategy(const SelectionStrategy&);
    SelectionStrategy& operator=(const SelectionStrategy&);
    SelectionStrategy(SelectionStrategy&&);
    SelectionStrategy& operator=(SelectionStrategy&&);

    virtual const IndividualInterface& selection(const std::vector<IndividualPtr>& population) = 0;
};

} // namespace ga
} // namespace algo
