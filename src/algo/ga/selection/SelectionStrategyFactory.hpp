#pragma once

#include "SelectionStrategy.hpp"
#include <configuration/GaCfg.hpp>
#include <memory>

namespace algo {
namespace ga {

class SelectionStrategyFactory
{
public:
    std::unique_ptr<SelectionStrategy> make(const configuration::GaCfg& gaCfg) const;
};

} // namespace ga
} // namespace algo
