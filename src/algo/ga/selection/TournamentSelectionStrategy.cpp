#include "TournamentSelectionStrategy.hpp"
#include <utils/RandomUtils.hpp>

namespace algo {
namespace ga {

TournamentSelectionStrategy::TournamentSelectionStrategy(const uint32_t tournamentSize)
    : tournamentSize{tournamentSize}
{
}

const IndividualInterface& TournamentSelectionStrategy::selection(const std::vector<IndividualPtr>& population)
{
    auto& random = utils::rnd::Random::getInstance();
    const uint32_t maxPossibleIndex = static_cast<uint32_t>(population.size() - 1);
    uint32_t rndIndex = random.getRandomUint(0, maxPossibleIndex);
    IndividualInterface* winner = population[rndIndex].get();
    for(auto i = 1u; i < tournamentSize; i++)
    {
        rndIndex = random.getRandomUint(0, maxPossibleIndex);
        IndividualInterface* rndIndividual = population[rndIndex].get();
        if(rndIndividual->getCurrentFitness() < winner->getCurrentFitness())
            winner = rndIndividual;
    }
    return *winner;
}

} // namespace ga
} // namespace algo
