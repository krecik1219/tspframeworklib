#include "SelectionStrategy.hpp"

namespace algo {
namespace ga {

SelectionStrategy::SelectionStrategy() = default;
SelectionStrategy::~SelectionStrategy() = default;
SelectionStrategy::SelectionStrategy(const SelectionStrategy &) = default;
SelectionStrategy &SelectionStrategy::operator=(const SelectionStrategy &) = default;
SelectionStrategy::SelectionStrategy(SelectionStrategy &&) = default;
SelectionStrategy &SelectionStrategy::operator=(SelectionStrategy &&) = default;

} // namespace ga
} // namespace algo
