#include "GeneticAlgorithm.hpp"
#include <algorithm>
#include <limits>
#include <utils/RandomUtils.hpp>
#include <numeric>

namespace algo {
namespace ga {

GeneticAlgorithm::GeneticAlgorithm(const configuration::GaCfg& gaCfg, const InitFunction& initFunction,
                                   std::unique_ptr<SelectionStrategy>&& selectionStrategy,
                                   tools::Logger& logger)
    : gaCfg{gaCfg}
    , initFunction{initFunction}
    , selectionStrategy{std::move(selectionStrategy)}
    , logger{logger}
    , bestFitnessStatistic{std::numeric_limits<double>::infinity()}
    , avgFitnessStatistic{std::numeric_limits<double>::infinity()}
    , worstFitnessStatistic{std::numeric_limits<double>::infinity()}
    , generationNum{0u}
{
}

GeneticAlgorithm::~GeneticAlgorithm() = default;

void GeneticAlgorithm::run()
{
    initialize();
    evaluate();
    // validatePopulation();
    updateStatisticValues();
    updateBestIndividualSoFar();
    logState();
    gaLoop();
}

IndividualPtr GeneticAlgorithm::getBestEverIndividual() const
{
    return bestSoFar->cloneIndividual();
}

void GeneticAlgorithm::initialize()
{
    for(auto i = 0u; i < gaCfg.populationSize; i++)
    {
        population.push_back(initFunction());
    }
}

void GeneticAlgorithm::evaluate()
{
    for(auto& individual : population)
    {
        individual->evaluate();
    }
}

void GeneticAlgorithm::gaLoop()
{
    while(!stopConditionReached())
    {
        selection();
        evaluate();
        generationNum++;
        updateStatisticValues();
        updateBestIndividualSoFar();
        logState();
        // validatePopulation();
    }
}

void GeneticAlgorithm::selection()
{
    Population nextPopulation;
    nextPopulation.reserve(population.size());
    while(nextPopulation.size() < population.size())
    {
        const IndividualInterface& parent1 = selectionStrategy->selection(population);
        const IndividualInterface& parent2 = selectionStrategy->selection(population);
        insertToNextPopulation(parent1, parent2, nextPopulation);
    }
    population = std::move(nextPopulation);
}

void GeneticAlgorithm::insertToNextPopulation(const IndividualInterface& parent1,
                                              const IndividualInterface& parent2,
                                              Population& nextPopulation)
{
    auto& random = utils::rnd::Random::getInstance();
    double rndCrossover = random.getRandomDouble(0.0, 1.0);
    if(rndCrossover < gaCfg.crossoverProb)
    {
        IndividualPtr offspring = parent1.crossover(parent2);
        followWithMutation(std::move(offspring), nextPopulation);
    }
    else if(nextPopulation.size() + 2 <= population.size())
    {
        IndividualPtr nextPopulationIndividual1 = parent1.cloneIndividual();
        IndividualPtr nextPopulationIndividual2 = parent2.cloneIndividual();
        followWithMutation(std::move(nextPopulationIndividual1), nextPopulation);
        followWithMutation(std::move(nextPopulationIndividual2), nextPopulation);
    }
    else
    {
        double rndWhichParent = random.getRandomDouble(0.0, 1.0);
        IndividualPtr nextPopulationIndividual;
        if(rndWhichParent < 0.5)
            nextPopulationIndividual = parent1.cloneIndividual();
        else
            nextPopulationIndividual = parent2.cloneIndividual();
        followWithMutation(std::move(nextPopulationIndividual), nextPopulation);
    }
}

void GeneticAlgorithm::followWithMutation(IndividualPtr&& individual, Population& nextPopulation)
{
    auto& random = utils::rnd::Random::getInstance();
    double rndMutation = random.getRandomDouble(0.0, 1.0);
    if(rndMutation < gaCfg.mutationProb)
    {
        individual->mutation();
    }
    nextPopulation.push_back(std::move(individual));
}

bool GeneticAlgorithm::stopConditionReached() const
{
    return generationNum >= gaCfg.maxGenerationsNum;
}

void GeneticAlgorithm::updateStatisticValues()
{
    auto minMaxIndividualsIt = std::minmax_element(population.cbegin(), population.cend(),
                                                   [](const auto& lhs, const auto& rhs)
                                                   {
                                                       return lhs->getCurrentFitness() < rhs->getCurrentFitness();
                                                   });
    bestFitnessStatistic = (*minMaxIndividualsIt.first)->getCurrentFitness();
    worstFitnessStatistic = (*minMaxIndividualsIt.second)->getCurrentFitness();
    double fitnessSum = std::accumulate(population.cbegin(), population.cend(), 0.0,
                                        [](auto acc, const auto& individual)
                                        {
                                            return acc + individual->getCurrentFitness();
                                        });
    avgFitnessStatistic = fitnessSum / population.size();
}

void GeneticAlgorithm::updateBestIndividualSoFar()
{
    auto compareFn = [](const auto& lhs, const auto& rhs)
                     {
                         return lhs->getCurrentFitness() < rhs->getCurrentFitness();
                     };
    auto bestIndividualIt = std::min_element(population.cbegin(), population.cend(), compareFn);
    if(bestSoFar == nullptr)
    {
        bestSoFar = (*bestIndividualIt)->cloneIndividual();
    }
    else
    {
        const auto best = std::min((*bestIndividualIt).get(), bestSoFar.get(), compareFn);
        if(best != bestSoFar.get())
            bestSoFar = best->cloneIndividual();
    }
}

void GeneticAlgorithm::logState()
{
    logger.log("%d, %.4f, %.4f, %.4f", generationNum, bestFitnessStatistic, avgFitnessStatistic, worstFitnessStatistic);
}

void GeneticAlgorithm::validatePopulation()
{
    for(auto& individual : population)
    {
        if(!individual->validate())
            throw std::runtime_error("Validation failed");
    }
}

} // namespace ga
} // namespace algo
