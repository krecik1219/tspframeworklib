#pragma once

#include <memory>
#include <utility>

namespace algo {
namespace ga {

class IndividualInterface;
using IndividualPtr = std::unique_ptr<IndividualInterface>;

class IndividualInterface
{
public:
    IndividualInterface();
    virtual ~IndividualInterface();

    IndividualInterface(const IndividualInterface&);
    IndividualInterface& operator=(const IndividualInterface&);
    IndividualInterface(IndividualInterface&&);
    IndividualInterface& operator=(IndividualInterface&&);

    virtual void evaluate() = 0;
    virtual void mutation() = 0;
    virtual IndividualPtr crossover(const IndividualInterface& other) const = 0;
    double getCurrentFitness();
    virtual bool validate() = 0;
    virtual std::string toString() const = 0;
    virtual IndividualPtr cloneIndividual() const = 0;

protected:
    double currentFitness;
    bool isCurrentFitnessValid;
};

} // namespace ga
} // namespace algo
