#pragma once

#include <configuration/GaCfg.hpp>
#include "IndividualInterface.hpp"
#include <vector>
#include <memory>
#include <functional>
#include <tools/logger/Logger.hpp>
#include <algo/ga/selection/SelectionStrategy.hpp>

namespace algo {
namespace ga {

using Population = std::vector<IndividualPtr>;
using InitFunction = std::function<IndividualPtr()>;

class GeneticAlgorithm
{
public:
    GeneticAlgorithm(const configuration::GaCfg& gaCfg, const InitFunction& initFunction,
                     std::unique_ptr<SelectionStrategy>&& selectionStrategy,
                     tools::Logger& logger);
    virtual ~GeneticAlgorithm();

    GeneticAlgorithm(const GeneticAlgorithm&) = delete;
    GeneticAlgorithm& operator=(const GeneticAlgorithm&) = delete;
    GeneticAlgorithm(GeneticAlgorithm&&) = delete;
    GeneticAlgorithm& operator=(GeneticAlgorithm&&) = delete;

    virtual void run();
    IndividualPtr getBestEverIndividual() const;

protected:
    virtual void initialize();
    void evaluate();
    virtual void gaLoop();
    void selection();
    void insertToNextPopulation(const IndividualInterface& parent1, const IndividualInterface& parent2,
                                Population& nextPopulation);
    void followWithMutation(IndividualPtr&& individual, Population& nextPopulation);
    bool stopConditionReached() const;
    void updateStatisticValues();
    void updateBestIndividualSoFar();
    void logState();

    void validatePopulation();

    const configuration::GaCfg gaCfg;
    const InitFunction initFunction;
    std::unique_ptr<SelectionStrategy> selectionStrategy;
    tools::Logger& logger;

    Population population;
    double bestFitnessStatistic;
    double avgFitnessStatistic;
    double worstFitnessStatistic;
    IndividualPtr bestSoFar;
    uint32_t generationNum;
};

} // namespace ga
} // namespace algo
