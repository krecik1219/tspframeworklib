#include "TwoOpt.hpp"
#include <algorithm>

namespace algo {

tsp::TspSolution TwoOpt::run2Opt(const tsp::TspSolution& inputSolution) const
{
    const auto& tspCfg = inputSolution.getTspCfg();
    tsp::CitySequence bestSolution = inputSolution.getCitySequence();
    double bestDistance = tsp::computeTotalDistance(bestSolution, tspCfg);
    bool improved = true;
    while(improved)
    {
        improved = false;
        for(auto i = 0u; i < inputSolution.getCitySequence().size() - 1 && !improved; i++)
        {
            for(auto j = i + 1; j < inputSolution.getCitySequence().size() && !improved; j++)
            {
                if(calcDistanceDiff(static_cast<int32_t>(i), static_cast<int32_t>(j), bestSolution, tspCfg) > 0)
                {
                    tsp::CitySequence newSolution = twoOptSwap(i, j, bestSolution);
                    double newDistance = tsp::computeTotalDistance(newSolution, tspCfg);
                    if(newDistance < bestDistance)
                    {
                        bestSolution = std::move(newSolution);
                        bestDistance = newDistance;
                        improved = true;
                    }
                }
            }
        }
    }
    return tsp::TspSolution{tspCfg, std::move(bestSolution)};
}

double TwoOpt::calcDistanceDiff(const int32_t firstCityIndex, const int32_t secondCityIndex,
                                const tsp::CitySequence& citySeq, const configuration::tsp::TspInstance& tspCfg) const
{
    const uint32_t beforeFirstCityId = citySeq[firstCityIndex - 1 >= 0 ? static_cast<uint32_t>(firstCityIndex - 1) : citySeq.size() - 1];
//    const uint32_t afterFitstCityId = solution.getCityIdAtIndex(
//        static_cast<uint32_t>(firstCityIndex + 1) < citySeq.size() ? static_cast<uint32_t>(firstCityIndex) + 1 : 0u);
    const uint32_t firstCityId = citySeq[static_cast<uint32_t>(firstCityIndex)];
    const uint32_t secondCityId = citySeq[static_cast<uint32_t>(secondCityIndex)];
//    const uint32_t beforeSecondCityId = solution.getCityIdAtIndex(
//        secondCityIndex - 1 >= 0 ? static_cast<uint32_t>(secondCityIndex - 1) : citySeq.size() - 1);
    const uint32_t afterSecondCityId = citySeq[static_cast<uint32_t>(secondCityIndex + 1) < citySeq.size() ? static_cast<uint32_t>(secondCityIndex) + 1 : 0u];
    return tspCfg.distanceMatrix[beforeFirstCityId][firstCityId]
           + tspCfg.distanceMatrix[secondCityId][afterSecondCityId]
           - tspCfg.distanceMatrix[firstCityId][afterSecondCityId]
           - tspCfg.distanceMatrix[beforeFirstCityId][secondCityId];
}

tsp::CitySequence TwoOpt::twoOptSwap(const uint32_t firstCityIndex, const uint32_t secondCityIndex,
                                     const tsp::CitySequence& solution) const
{
    tsp::CitySequence newSolution = solution;
    auto firstCityIt = std::next(newSolution.begin(), static_cast<int32_t>(firstCityIndex));
    auto secondCityIt = std::next(newSolution.begin(), static_cast<int32_t>(secondCityIndex) + 1);
    std::reverse(firstCityIt, secondCityIt);
    return newSolution;
}

} // namespace algo
